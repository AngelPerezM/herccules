Googletest notes
================

Here is a cheat sheet from the most used/important gtest (Google Test framework) methods, in addition to some guideline learned from Internet tutorials.

Test attributes
---------------

Tests should be:
1. **independent and repeatable**. gtest runs tests in isolation.
2. **well organized**. gtest offers `test suites` to share data & subroutines between related tests.
3. **portable and reusable**.
4. **verbose**.
5. **automated and easy to use**.
6. **fast**. gtest offers the reuse of shared resources across tests.

Nomenclature
------------

- **Test** ISTQB test case. Tests exercise a particular program path with specific input values and verify the results. In gtest it is represented with the `TEST()` macro. These tests use *assertions* to test the code's behavior.
- **Test Suite**. Contains one or many tests. You should group your tests into test suites that reflect the structure of the tested code. When multiple tests in a test suite need to share common objects and subroutines, you can put them into a test fixture class
- **Test program** Contains multiple test suites.

So, the hierarchy of tests is depicted graphically bellow:
```
                                                 |-- Assertion 1
                                  |-- TEST() 1 --+
                                  |              |-- Assertion 2
               |-- Test suite 1 --+
               |                  |-- TEST() 2
Test program --+
               |-- Test suite 2

```
Assertions
----------

- `ASSERT_*` exit the function if the condition fails. We should only use this if it does not make sense to continue when the assertion fails.
- `EXPECT_*` does not exit the function if failure. This is preferred since we can report more than one failure in the same function.
- ` << str` operator. This allow us to print custom messages.

See https://google.github.io/googletest/reference/assertions.html for a complete list of ASSERT and EXPECT types