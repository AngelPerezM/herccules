CMake Configuration
===================

The Herccules OBSW follows a hierarchical decomposition of various software modules.
These modules need to communicate to implement the system logic, which results in dependecny and usage relationships:

```
.----------.      .----------.
| Module A | ---> | Module B |
'----------'      '----------'
```
*Dependency relationship*


This recipe contains a few notes learned while creating the CMake project based on different tutorials and guidelines found on the Internet.

Note 1: include dependencies
----------------------------

- `target_include_directories(A PRIVATE   B_Include_Dirs)`: A uses B headers in .cpp or private .h files.
- `target_include_directories(A INTERFACE B_Include_Dirs)`: A exports B headers to modules that depend on A.
  E.g.: Header only libraries.
- `target_include_directories(A PUBLIC    B_Include_Dirs)`: A uses B headers in public .h files, which are included in
  both A's source files and might be included in any client of A's.

Note 2: Link dependencies
-------------------------

- `target_link_libraries(A PRIVATE   B)`: All the objects following PRIVATE will only be used for linking to the
current target. I.E.: B's objects can be used *only* by A
- `target_link_libraries(A INTERFACE B)`: All the objects following INTERFACE will only be used for providing the
interface to the other targets that have dependencies on the current target. I.E.: B's objects can be used *only* by A's
clients. E.G.: Header only libraries.
- `target_link_libraries(A PUBLIC    B)`: All the objects following PUBLIC will be used for linking to the current
target and providing the interface to the other targets that have dependencies on the current target. I.E.: B's objects
can be used by *both* A and A's clients.

Evil, anti-patterns
-------------------

1. Do not use `include_directories`, `add_definitions`, `link_libraries` because it affects to every module.
2. Do not use `target_include_directories` with a path outside your module.
3. Always specify if the linked library is public, private or interface.
4. Do not use `target_compile_options` to set flags that affect the ABI.

References
----------
- [GitHub Article](https://leimao.github.io/blog/CMake-Public-Private-Interface/)
- [YouTube, modern CMake](https://www.youtube.com/watch?v=eC9-iRN2b04)
- [Organizing Larger Projects in C++](https://db.in.tum.de/teaching/ss20/c++praktikum/slides/lecture-11.pdf)
