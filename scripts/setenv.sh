#!/bin/bash
# Purpose:
#   This script shall be executed before editing/compiling ANY TASTE
#   project. It works similarly to ssh-agent, i.e: it prints a
#   commands that shall be executed by the user in order to set
#   some environment variables.
# Usage:
#   source ./scripts/setenv.sh

# Root folder for the HERCCULES-OBSW project:

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

export OBSW_SRCD=${HERE}/../src

# Environmental variables used across the project config:

# For TASTE:
export OBSW_DATA_TYPES="${OBSW_SRCD}/tmtc-interface/Data_Types/src"
export TASTE_SHARED_TYPES="${OBSW_SRCD}/Library/Shared_Types_Library"

# For HAL:
export HAL_LIB="${OBSW_SRCD}/build/rpi/release/lib"
export HAL_INC="${OBSW_SRCD}/build/rpi/release/include"

# For scripts:
alias upload="${HERE}/upload.sh"
alias taste-create-gitignore="${HERE}/taste/scripts/taste-create-gitignore"
