<img align="right" width="220em" src="docs/fig/herccules-logo-hori.png"><br/>

# HERCCULES

The REXUS/BEXUS programme allows students from universities and higher education colleges across Europe to carry out scientific and technological experiments on research rockets and balloons. Each year, two rockets and two balloons are launched, carrying up to 20 experiments designed and built by student teams.

HERCCULES is a balloon project selected for the ESA BEXUS programme. This project is designed and built by university students from the "Ignacio Da Riva" Institute (IDR) and the STRAST research group, both from the Technical University of Madrid (UPM), in collaboration with industry partners.

# On-Board Software (OBSW)

This repository contains the HERCCULES OBSW source code.

The HERCCULES OBSW shall implement a set of requirements, which can be summarized in the following high-level features:
- Control and manage the status from all the hardware components.
- Data acquisition and control the following experiments and subsystems:
  - Attitude Determination Subsystem (ADS).
  - Attitude Laboratory (Att. Lab.).
  - Environmental Laboratory (Env. Lab.).
  - Heat Transfer Laboratory (HTL).
  - Power Control Unit (PCU).
- Manage the system and subsystem operational modes and its transitions.
- OBDH operations: process and perform TCs from the GS and handle the TM data to be downlinked to the GS.
- Onboard storage of the data acquired from all the experiments and additional HK data.
- Context Management, i.e.: save the relative time, the system and subsystem operational modes. So that, in case of failure these data can be restored.
- FDIR functions for the OBSW dependability.

## Software architecture

The OBSW has been developed following the componet and model-based software engineering approaches. The basic building block of the architecture are hierarchical software components. In actual fact, these components are static libraries.

The high level components are implemented using TASTE (The ASSERT Set of Tools for Engineering)
since it provides the required Real Time abstractions to implement the HERCCULES embedded software. While the low-level ones, like the HAL, are implemented as CMakeProjects.

<img src="docs/software/images/high_level_sw_arch-v2.0.png" width="450px">

_Figure 1: UML Package diagram that represents the static architecture of the HERCCULES OBSW._

## License

> Copyright (C) 2023 Universidad Politécnica de Madrid

The UPMSat-3 OBSW was developed by the [Real-Time Systems Group](https://www.dit.upm.es/~str/) at the [Universidad Politécnica de Madrid](https://www.upm.es/) under the [**GNU General Public License v3**](LICENSE).