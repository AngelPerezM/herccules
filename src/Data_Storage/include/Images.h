/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                               DATA STORAGE                                 --
--                                                                            --
--                              Images  Header                                --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef DATA_STORAGE_IMAGE_H
#define DATA_STORAGE_IMAGE_H

#include "String.h"

#include <cstdio>

#include <cfloat>
#include <cstdint>
#include <cinttypes>

/**
 * @brief This module provides `image` functions for integers and float types
 * The image function is similar to Ada's Image, returns an String 
 */
namespace data_storage::basic_images {

    // Format for int16_t
    // - - - - - - - - - -
    //
    // R(value) = [-32768 .. +32767]
    // Size for str = 1 [sign] + 5 [digits] + 1 [\0] = 7    
    inline auto image(std::int16_t value) {
        static thread_local String <7U> value_str;
        std::snprintf (value_str, value_str.size(), "%" PRIi16, value);
        return value_str;
    }

    // Format for uint16_t
    // - - - - - - - - - -
    //
    // R(value) = [0 .. 65535]
    // Size for str = 5 [digits] + 1 [\0] = 6
    inline auto image(std::uint16_t value) {
        static thread_local String <6U> value_str;
        std::snprintf (value_str, value_str.size(), "%" PRIu16, value);
        return value_str;
    }

    // Format for int32_t
    // - - - - - - - - - -
    //
    // R(value) = [-2'147'483'648 .. +2'147'483'647]
    // Size for str = 1 [sign] + 10 [digits] + 1 [\0] = 12
    inline auto image(std::int32_t value) {
        static thread_local String <12U> value_str;
        std::snprintf (value_str, value_str.size(), "%" PRIi32, value);
        return value_str;
    }
    
    // Format for uint32_t
    // - - - - - - - - - -
    //
    // R(value) = [0 .. 4'294'967'295]
    // Size for str = 10 [digits] + 1 [\0] = 11
    inline auto image(std::uint32_t value) {
        static thread_local String <11U> value_str;
        std::snprintf (value_str, value_str.size(), "%" PRIu32, value);
        return value_str;
    }
    
    // Format for float
    // - - - - - - - - -
    //
    // + 7 [Max accuracy of 7 digits after point]
    //   3 [8 bits of exponent]
    //   2 [Sign for Mantisa and Number]
    //   1 [E/e symbol]
    //   1 [The decimal point]
    //   1 [null character '\0']
    // -----
    //  15 [digits], + 1 since the %l is the same as %lf for the formatter
    inline auto image(float value) {
        static thread_local String <16U> value_str;
        std::snprintf (value_str, value_str.size(), "%0.7ge", value);
        return value_str;
    }
    
    // Format for double
    // - - - - - - - - -
    //
    // + 17 [Max accuracy of 17 digits after point]
    //    4 [11 bits of exponent]
    //    2 [Sign for Mantisa and Number]
    //    1 [E/e symbol]
    //    1 [The decimal point]
    //    1 [null character '\0']
    // ------
    //   26 [digits]
    inline auto image(double value) {
        static thread_local String <26U> value_str;
        std::snprintf (value_str, value_str.size(), "%0.17ge", value);
        return value_str;
    }
}

// Refs:
// [1] Template specialization:  https://www.modernescpp.com/index.php/c-core-guidelines-template-interfaces
// [2] Max digits for representation: https://stackoverflow.com/questions/1701055/what-is-the-maximum-length-in-chars-needed-to-represent-any-double-value

#endif // DATA_STORAGE_IMAGE_H
