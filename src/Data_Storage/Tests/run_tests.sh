#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
TESTOBJECT="${SCRIPTPATH}/../../build/pc/debug/Data_Storage/Tests/DataLoggerTests"
TEST_CASES="new empty with_contents_noln with_contents_ln 100_lines 1000_lines 1000_lines_with_image"
RESULT=0

result_fname() {
    fname=$1
    echo "$SCRIPTPATH/results_data/$fname.results.csv"
}

in_fname() {
    fname=$1
    echo "$SCRIPTPATH/in_data/$fname.in.csv"
}

expected_fname() {
    fname=$1
    echo "$SCRIPTPATH/expected_data/$fname.expected.csv"
}

################################################################################
echo "-- Preparing CSVs"

for test_name in $TEST_CASES
do
    rm $(result_fname $test_name)
    if [[ -f $(in_fname $test_name) ]]; then
        cp $(in_fname $test_name) $(result_fname $test_name)
    fi
done


################################################################################
echo "-- Running tests"
$TESTOBJECT || RESULT=1


################################################################################
echo "-- Comparing expected results"

for test_name in ${TEST_CASES}
do
    if diff $(result_fname $test_name) $(expected_fname $test_name) &> /dev/null; then
        echo "[PASSED] $test_name test case"
    else
        echo "[FAILED] $test_name test case"
        RESULT=1
    fi
done

exit ${RESULT};
