#include "CsvWriter.h"
#include "Images.h"
#include "String.h"

#include <iostream>
#include <assert.h>

#define RESULTS_DIR "./results_data"

// -- New wrapper -- //

void* operator new(size_t size) {
    std::cout << "Allocating " << size << " bytes\n";
    return malloc(size);
}

// -- Tests -- //

void check_non_existing_file() {
    constexpr uint8_t n_cols = 3U;

    std::array <std::string_view, n_cols> header {
        "mission_time", "operating_mode", "measurements"
    };

    data_storage::CsvWriter<n_cols>
        csv_writer (RESULTS_DIR"/new.results.csv", ';', header);

    assert(csv_writer.status().was_empty == 1);
    assert(csv_writer.status().opened    == 1);
    assert(csv_writer.status().add_ln    == 0);

    std::array <std::string_view, n_cols> dummy_contents {
        data_storage::EMPTY_FIELD, "OFF", "20"
    };
    csv_writer.add_line(dummy_contents);
}

void check_empty_file() {
    constexpr uint8_t n_cols = 3U;

    std::array <std::string_view, n_cols> header {
        "mission_time", "operating_mode", "measurements"
    };

    data_storage::CsvWriter<n_cols>
        csv_writer (RESULTS_DIR"/empty.results.csv", ',', header);

    assert(csv_writer.status().was_empty == 1);
    assert(csv_writer.status().opened    == 1);
    assert(csv_writer.status().add_ln    == 0);

    std::array <std::string_view, n_cols> dummy_contents {
        "0", "OFF", "20"
    };
    csv_writer.add_line(dummy_contents);
}

void check_file_with_contents_noln() {
    constexpr uint8_t n_cols = 3U;

    std::array <std::string_view, n_cols> header {
        "mission_time", "operating_mode", "measurements"
    };

    data_storage::CsvWriter<n_cols>
        csv_writer (RESULTS_DIR"/with_contents_noln.results.csv", ',', header);

    assert(csv_writer.status().was_empty == 0);
    assert(csv_writer.status().opened    == 1);
    assert(csv_writer.status().add_ln    == 1);

    std::array <std::string_view, n_cols> dummy_contents {
        "0", "OFF", "20"
    };
    csv_writer.add_line(dummy_contents);
}

void check_file_with_contents_ln() {
    constexpr uint8_t n_cols = 3U;

    std::array <std::string_view, n_cols> header {
        "mission_time", "operating_mode", "measurements"
    };

    data_storage::CsvWriter<n_cols>
        csv_writer (RESULTS_DIR"/with_contents_ln.results.csv", ',', header);

    assert(csv_writer.status().was_empty == 0);
    assert(csv_writer.status().opened    == 1);
    assert(csv_writer.status().add_ln    == 0);

    std::array <std::string_view, n_cols> dummy_contents {
        "0", "PRE-LAUNCH", "20"
    };
    csv_writer.add_line(dummy_contents);
}

void check_file_with_100_lines() {
    constexpr uint8_t n_cols = 5U;

    std::array <std::string_view, n_cols> header {
        "mission_time", "operating_mode", "experiment_mode", "measurement-1", "measurement-2"
    };

    data_storage::CsvWriter<n_cols>
        csv_writer (RESULTS_DIR"/100_lines.results.csv", ';', header);

    assert(csv_writer.status().was_empty == 1);
    assert(csv_writer.status().opened    == 1);
    assert(csv_writer.status().add_ln    == 0);

    std::array <std::string_view, n_cols> the_contents {
        "0", "PRE-LAUNCH", "ON", "100", "100"
    };

    for (int i = 0; i < 100; ++i) {
        csv_writer.add_line(the_contents);
    }
}

void check_file_with_1000_lines() {
    constexpr uint8_t n_cols = 5U;

    std::array <std::string_view, n_cols> header {
        "mission_time", "operating_mode", "experiment_mode", "measurement-1", "measurement-2"
    };

    data_storage::CsvWriter<n_cols>
        csv_writer (RESULTS_DIR"/1000_lines.results.csv", ';', header);

    assert(csv_writer.status().was_empty == 1);
    assert(csv_writer.status().opened    == 1);
    assert(csv_writer.status().add_ln    == 0);

    std::array <std::string_view, n_cols> the_contents {
        "0", "PRE-LAUNCH", "ON", "1000", "1000"
    };

    for (int i = 0; i < 1000; ++i) {
        csv_writer.add_line(the_contents);
    }
}

enum DayOfWeek {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday
};

auto image(DayOfWeek dow) {
    switch (dow) {
        case (DayOfWeek::Monday):
            return "Monday";
        case (DayOfWeek::Tuesday):
            return "Tuesday";
        case (DayOfWeek::Wednesday):
            return "Wendsday";
        case (DayOfWeek::Thursday):
            return "Thursday";
        case (DayOfWeek::Friday):
            return "Friday";
        case (DayOfWeek::Saturday):
            return "Saturday";
        case (DayOfWeek::Sunday):
            return "Sunday";
        default:
            return "DayOfWeek Unknown";
    }
    return "";
}

void check_file_with_1000_lines_with_image() {

    struct data {
        std::int16_t   i16 {};
        std::uint16_t  u16 {};
        std::int32_t   i32 {};
        std::uint32_t  u32 {};
        float     f32 {};
        double    f64 {};
        DayOfWeek dow = Monday;
    } structured {};

    data_storage::CsvWriter<7U>
    csv_writer (RESULTS_DIR"/1000_lines_with_image.results.csv",
                ';',
                { "i16", "u16", "i32", "u32", "f32", "f64", "day-of-week" });

    assert(csv_writer.status().was_empty == 1);
    assert(csv_writer.status().opened    == 1);
    assert(csv_writer.status().add_ln    == 0);
    

    using data_storage::basic_images::image; // ADL
    for (int i = 0; i < 1000; ++i) {
        structured.i16++; structured.u16++;
        structured.i32++; structured.u32++;
        structured.f32++; structured.f64++;

        structured.dow = structured.dow == DayOfWeek::Sunday ?
                         DayOfWeek::Monday :
                         DayOfWeek(structured.dow + 1);

        csv_writer.add_line( {
            image(structured.i16),
            image(structured.u16),
            image(structured.i32),
            image(structured.u32),
            image(structured.f32),
            image(structured.f64),
            image(structured.dow)
        } );
    }

}

// -- Main -- //

int main() {
    check_non_existing_file();
    check_empty_file();
    check_file_with_contents_noln();
    check_file_with_contents_ln();
    check_file_with_100_lines();
    check_file_with_1000_lines();
    check_file_with_1000_lines_with_image();
}
