/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                             NADS_Manager Source                            --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "nads_manager.h"
#include "nads_manager_state.h"
#include "Context-nads-manager.h" // Context Parameters defined in the Interface View

#include "CsvWriter.h"
#include "String.h"
#include "Images.h"

#include "Time_Management.h"

// --  Local variables  --------------------------------------------------------

namespace {
    nads_manager_state ctxt;
}

// --  Internal operations  ----------------------------------------------------

namespace {

    // --------------------------
    // -- Process balloon mode --
    // --------------------------

    void nads_process_balloon_mode()
    {
        asn1SccBalloon_Mode mode;
        nads_manager_RI_Get_Mode(&mode);

        switch (mode) {
            case asn1SccBalloon_Mode_ground_await:
                if (ctxt.nads_data.mode == asn1SccNADS_Mode_on)
                    puts("NADS: AWAIT processed!");
                ctxt.nads_data.mode = asn1SccNADS_Mode_off;
                break;
            case asn1SccBalloon_Mode_ground_pre_launch :
            case asn1SccBalloon_Mode_flight_ascent     :
            case asn1SccBalloon_Mode_flight_float      :
                if (ctxt.nads_data.mode == asn1SccNADS_Mode_off)
                    puts("NADS: ON processed!");
                ctxt.nads_data.mode = asn1SccNADS_Mode_on;
                break;
            case asn1SccBalloon_Mode_off:
                puts("NADS: Shutdown processed!");
                ctxt.nads_data.mode = asn1SccNADS_Mode_off;
                ctxt.end_of_mission = true;
                break;
        }
    }

    // -------------------------------------------
    // -- Handle data & its auxiliary functions --
    // -------------------------------------------

    // -- Store data:

    /**
     * Stores the 100 measurments each second.
     */
    void nads_manager_store_data() {
        const auto & imu = ctxt.nads_data.payload.imu;

        using data_storage::basic_images::image;

        ctxt.logfile.add_line({
            image(static_cast<int32_t>(ctxt.nads_data.snapshot_time.secs)),
            image(static_cast<int32_t>(ctxt.nads_data.snapshot_time.usecs)),
            image(static_cast<float>  (ctxt.nads_data.mission_time)),
            nads_manager_state::
            image(static_cast<asn1SccNADS_Mode>(ctxt.nads_data.mode)),

            image(static_cast<float>(imu.sensors_data.acceleration.x)),
            image(static_cast<float>(imu.sensors_data.acceleration.y)),
            image(static_cast<float>(imu.sensors_data.acceleration.z)),
            image(static_cast<float>(imu.sensors_data.angular_velocity.x)),
            image(static_cast<float>(imu.sensors_data.angular_velocity.y)),
            image(static_cast<float>(imu.sensors_data.angular_velocity.z)),
            image(static_cast<float>(imu.sensors_data.mag_field.x)),
            image(static_cast<float>(imu.sensors_data.mag_field.y)),
            image(static_cast<float>(imu.sensors_data.mag_field.z)),

            image(static_cast<float>(imu.fusion_data.euler_orientation.x)),
            image(static_cast<float>(imu.fusion_data.euler_orientation.y)),
            image(static_cast<float>(imu.fusion_data.euler_orientation.z)),
            image(static_cast<float>(imu.fusion_data.liner_acceleration.x)),
            image(static_cast<float>(imu.fusion_data.liner_acceleration.y)),
            image(static_cast<float>(imu.fusion_data.liner_acceleration.z)),
            image(static_cast<float>(imu.fusion_data.gravity.x)),
            image(static_cast<float>(imu.fusion_data.gravity.y)),
            image(static_cast<float>(imu.fusion_data.gravity.z)),
            image(static_cast<float>(imu.fusion_data.quaternion_orientation.w)),
            image(static_cast<float>(imu.fusion_data.quaternion_orientation.x)),
            image(static_cast<float>(imu.fusion_data.quaternion_orientation.y)),
            image(static_cast<float>(imu.fusion_data.quaternion_orientation.z)),

            image(static_cast<std::int8_t>(imu.temperatures.temperature_accel)),
            image(static_cast<std::int8_t>(imu.temperatures.temperature_gyro)),
        });

        if (ctxt.cycle == 50U) {
            ctxt.logfile.save_file();
        }
    }

    // -- Publish data

    /**
     * @brief Updates data pool at 1 Hz frequency
     */
    void nads_publish_data() {
        if (ctxt.cycle == 100U) {
            nads_manager_RI_Update_NADS_Data(&ctxt.nads_data);
        }
    }

    // -- Handle data

    /**
     * @brief Updates the cycle, each increment represents a 10ms lapse
     */
    constexpr void update_cycle(uint8_t &cycle) {
        cycle = cycle < 100U
              ? cycle + 1
              : 1U;
    }

    /**
     * Acquires and stores data regarding the NADS mode.
     */
    void nads_handle_data() {
        // Get time:
        auto && [secs, usecs] = time_management::absolute_time();
        ctxt.nads_data.snapshot_time = {secs, usecs};
        ctxt.nads_data.mission_time  = time_management::mission_time();

        // Read & store IMU measurements:
        if (ctxt.nads_data.mode == asn1SccNADS_Mode_on) {
            auto cycle {static_cast<asn1SccUINT8_Type>(ctxt.cycle)};
            nads_manager_RI_Read_IMU_Data(&cycle, &ctxt.nads_data.payload.imu);
            nads_manager_store_data();
        }

        nads_publish_data();

        update_cycle(ctxt.cycle);
    }
}

// --  Provided Interface  -----------------------------------------------------

// ------------------------
// -- Startup & Shutdown --
// ------------------------

/**
 * @note This startup routine is invoked by the TASTE runtime,
 * which guarantees that it's executed only once, even if it was invoked multiple times.
 */
void nads_manager_startup(void)
{
    ctxt.nads_data.mode = asn1SccNADS_Mode_off;
}

/**
 * This is the shutdown routine that shall be invoked when "end of mission" is reached.
 */
static
void nads_shutdown(void)
{
    puts("NADS: Shutdown!");
    ctxt.logfile.save_file();

    // Update Data Pool with latest info:
    nads_manager_RI_Update_NADS_Data(&ctxt.nads_data);

    // Notify the Manager that this subsystem has finished:
    constexpr asn1SccBalloon_Events finish_event {asn1SccBalloon_Events_nads_finished};
    nads_manager_RI_Notify_Event(&finish_event);
}

// -------------------------------
// -- Measure And Data Handling --
// -------------------------------

void nads_manager_PI_Measure_And_Handle(void)
{
    if ( ! ctxt.end_of_mission ) {
        // 1.  Nominal activity:
        nads_process_balloon_mode();
        nads_handle_data();

        // 2. Shutdown if end of mission:
        if (ctxt.end_of_mission) {
            nads_shutdown();
        }
    }
}

// ----------------------
// -- Reset I2C Device --
// ----------------------

void nads_manager_PI_Reset_I2C_Device
    (const asn1SccRestartable_Device_ID * IN_device_id)
{
    switch (*IN_device_id) {
    case asn1SccRestartable_Device_ID_nads_imu:
        nads_manager_RI_Reset_IMU( );
        break;
    case asn1SccRestartable_Device_ID_nads_gps:
        // TODO: restart GPS
        break;
    default:
        break;
    }
}
