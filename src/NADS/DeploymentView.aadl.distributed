--  This file was generated automatically - do not modify it manually
--  If you wish to modify the template used to create this file, it is located
--  in ~/tool-src/misc/space-creator/dv2aadl/deploymentview.tmplt
--  After modifications, install it by running ~/tool-src/install/90_misc
--  Template written by Maxime Perrotin (maxime.perrotin@esa.int) 2021-09
package deploymentview::DV::Node_1
public

 with Deployment;
 with TASTE;
 with TASTE_DV_Properties;

    with ocarina_buses; 
    with ocarina_drivers; 

 --  Declare partition(s) of node "Node_1" (aka "x86_Linux_POHIC_1").
 process Partition_2
 end Partition_2;

 process implementation Partition_2.others
 end Partition_2.others;

 device ethernet3
    extends ocarina_drivers::generic_sockets_ip 
 features
    link : refined to requires bus access ocarina_buses::ip.i;
 properties 
    Deployment::Configuration => "{ devname  &quot;enp0s3&quot;, address  &quot;172.20.10.4&quot;, version  ipv4, port  5488 }"; 
    Deployment::Config => "/home/taste/tool-inst/include/ocarina/runtime/polyorb-hi-c/src/drivers/configuration/ip.asn";
 end ethernet3;

 device implementation ethernet3.others
    extends ocarina_drivers::generic_sockets_ip.pohic 
 end ethernet3.others;

end deploymentview::DV::Node_1;

--  This file was generated automatically - do not modify it manually
--  If you wish to modify the template used to create this file, it is located
--  in ~/tool-src/misc/space-creator/dv2aadl/deploymentview.tmplt
--  After modifications, install it by running ~/tool-src/install/90_misc
--  Template written by Maxime Perrotin (maxime.perrotin@esa.int) 2021-09
package deploymentview::DV::Node_2
public

 with Deployment;
 with TASTE;
 with TASTE_DV_Properties;

    with ocarina_buses; 
    with ocarina_drivers; 

 --  Declare partition(s) of node "Node_2" (aka "Raspberry PI Linux POHIC_1").
 process Partition_1
 end Partition_1;

 process implementation Partition_1.others
 end Partition_1.others;

 device wlan0
    extends ocarina_drivers::generic_sockets_ip 
 features
    link : refined to requires bus access ocarina_buses::ip.i;
 properties 
    Deployment::Configuration => "{ devname  &quot;wlan0&quot;, address  &quot;172.20.10.3&quot;, version  ipv4, port  5489 }"; 
    Deployment::Config => "/home/taste/tool-inst/include/ocarina/runtime/polyorb-hi-c/src/drivers/configuration/ip.asn";
 end wlan0;

 device implementation wlan0.others
    extends ocarina_drivers::generic_sockets_ip.pohic 
 end wlan0.others;

end deploymentview::DV::Node_2;

package deploymentview::DV
public

 with TASTE;
 with Deployment;
 with Interfaceview::IV;
 with TASTE_DV_Properties;

 --  Dependencies of node Node_1
 with interfaceview::IV::Tester_Interface;
    with ocarina_buses; 
 with deploymentview::DV::Node_1;
 with ocarina_processors_x86;
 --  Dependencies of node Node_2
 with interfaceview::IV::NADS::NADS_Manager;
 with interfaceview::IV::NADS::NADS_System_Mode;
 -- with interfaceview::IV::NADS;
 with interfaceview::IV::Tester;
 with interfaceview::IV::NADS::IMU_Proxy;
    with ocarina_buses; 
 with deploymentview::DV::Node_2;
 with ocarina_processors_arm;

 --  Node Node_1
 system Node_1
 features
    ethernet3_ethernet3 : requires bus access ocarina_buses::ip.i;
 end Node_1;

 system implementation Node_1.others
 subcomponents
    IV_Tester_Interface : system Interfaceview::IV::Tester_Interface::Tester_Interface.others {
       Taste::ImplementationName => "";
    };
    Partition_2 : process deploymentview::DV::Node_1::Partition_2.others { 
       TASTE_DV_Properties::CoverageEnabled => false;
    };
    p1 : processor ocarina_processors_x86::x86.linux;
    ethernet3 : device deploymentview::DV::Node_1::ethernet3.others;
 connections
    ethernet3_ethernet3_link : bus access ethernet3_ethernet3 -> ethernet3.link;
 properties
    TASTE::APLC_Binding => (reference (Partition_2)) applies to IV_Tester_Interface;
    Actual_Processor_Binding => (reference (p1)) applies to Partition_2;
    Actual_Processor_Binding => (reference (p1)) applies to ethernet3;
 end Node_1.others;

 --  Node Node_2
 system Node_2
 features
    wlan0_ethernet3 : requires bus access ocarina_buses::ip.i;
 end Node_2;

 system implementation Node_2.others
 subcomponents
    --IV_NADS : system Interfaceview::IV::NADS::NADS.others {
    --   Taste::ImplementationName => "";
    --};
    IV_Tester : system Interfaceview::IV::Tester::Tester.others {
       Taste::ImplementationName => "";
    };
    IV_NADS_System_Mode : system Interfaceview::IV::NADS::NADS_System_Mode::NADS_System_Mode.others {
       Taste::ImplementationName => "";
    };
    IV_NADS_Manager : system Interfaceview::IV::NADS::NADS_Manager::NADS_Manager.others {
       Taste::ImplementationName => "";
    };
    Partition_1 : process deploymentview::DV::Node_2::Partition_1.others { 
       TASTE_DV_Properties::CoverageEnabled => false;
    };
    p1 : processor ocarina_processors_arm::rpi.posix;
    wlan0 : device deploymentview::DV::Node_2::wlan0.others;
 connections
    wlan0_ethernet3_link : bus access wlan0_ethernet3 -> wlan0.link;
 properties
    --TASTE::APLC_Binding => (reference (Partition_1)) applies to IV_NADS;
    TASTE::APLC_Binding => (reference (Partition_1)) applies to IV_NADS_Manager;
    TASTE::APLC_Binding => (reference (Partition_1)) applies to IV_NADS_System_Mode;
    TASTE::APLC_Binding => (reference (Partition_1)) applies to IV_Tester;
    TASTE::APLC_Binding => (reference (Partition_1)) applies to IV_IMU_Proxy;
    Actual_Processor_Binding => (reference (p1)) applies to Partition_1;
    Actual_Processor_Binding => (reference (p1)) applies to wlan0;
 end Node_2.others;


system deploymentview
end deploymentview;

system implementation deploymentview.others
subcomponents
   interfaceview : system interfaceview::IV::interfaceview.others;
   Node_1 : system Node_1.others;
   Node_2 : system Node_2.others;
   bus_1 : bus ocarina_buses::ip.i;
 connections
   Node_1_bus_1 : bus access bus_1 -> Node_1.ethernet3_ethernet3;
   Node_2_bus_1 : bus access bus_1 -> Node_2.wlan0_ethernet3;
 properties
   Actual_Connection_Binding => (reference (bus_1)) applies to interfaceview.Tester_Interface_RI_Notify_Mode_Change;
   Actual_Connection_Binding => (reference (bus_1)) applies to interfaceview.Tester_RI_Update_NADS_Data_Async;
   Actual_Connection_Binding => (reference (bus_1)) applies to interfaceview.NADS_RI_Notify_Event;
   -- Actual_Connection_Binding => (reference (bus_1)) applies to interfaceview.NADS_Manager_RI_Notify_Event;
end deploymentview.others;

end deploymentview::DV;
