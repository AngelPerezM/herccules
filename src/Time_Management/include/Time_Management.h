/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                             TIME MANAGEMENT                                --
--                                                                            --
--                                  Header                                    --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef TIME_MANAGEMENT_H
#define TIME_MANAGEMENT_H

#include <cstdint> // int32_t

namespace time_management {

    void initialize();

    // --  Mutators  -----------------------------------------------------------

    /**
     * Sets the absolute number of seconds and microseconds since the Unix Epoch
     */
    bool configure_absolute_time(int32_t seconds, int32_t microseconds);

    // --  Accessors  ----------------------------------------------------------

    // -----------------------
    // -- Get absolute time --
    // -----------------------

    struct AbsoluteTime {
        int32_t seconds;
        int32_t microseconds;
    };

    /**
     * \return the number of seconds and microseconds since the Unix Epoch.
     */
    AbsoluteTime absolute_time();

    /**
     * \return the number of seconds since the Unix Epoch.
     */
    int32_t absolute_time_secs();

    // ------------------
    // -- mission time --
    // ------------------

    /**
     * \return the number of seconds, with nanoseconds precision,
     *         since the first program execution as a floating point number.
     */
    float mission_time();

}

#endif // TIME_MANAGEMENT_H
