/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                             TIME MANAGEMENT                                --
--                                                                            --
--                                  Source                                    --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "Time_Management.h"
#include <time.h>     // clock_gettime
#include <sys/time.h> // {get|set}_timeofday

namespace {
    auto program_start_time = []() -> struct timespec {
        struct timespec t_now;
        (void) clock_gettime(CLOCK_MONOTONIC, &t_now);
        return t_now;
    }();

    float previous_execution_relative_time = 0.0F;
}

namespace time_management {

    void initialize() {
        // nothing
    }

    // --  Mutators  -----------------------------------------------------------

    bool configure_absolute_time(int32_t seconds, int32_t microseconds) {
        const struct timeval tv {seconds, microseconds};
        return settimeofday(&tv, NULL) == 0;
    }

    // --  Accessors  ----------------------------------------------------------

    int32_t absolute_time_secs() {
        return time(NULL);
    }

    AbsoluteTime absolute_time() {
        struct timeval tv;
        (void) gettimeofday(&tv, NULL);
        return {
            static_cast<int32_t>(tv.tv_sec),
            static_cast<int32_t>(tv.tv_usec)
        };
    }

    float mission_time() {
        struct timespec now;
        (void) clock_gettime(CLOCK_MONOTONIC, &now);

        const float program_elapsed_time =
              static_cast<float>(now.tv_sec  - program_start_time.tv_sec )
           + (static_cast<float>(now.tv_nsec - program_start_time.tv_nsec) / 1.0e9);

        return program_elapsed_time + previous_execution_relative_time;
    }

}
