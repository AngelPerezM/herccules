// C+++ body file for function EL_Data

#include "el_data.h"
#include "el_data_state.h"

namespace {
    el_data_state ctxt_el_data;
}

void el_data_startup(void)
{
    asn1SccEnvLab_Data_Initialize(&ctxt_el_data.el_data);
}

void el_data_PI_Get
      (asn1SccEnvLab_Data *OUT_el_data)
{
   *OUT_el_data = ctxt_el_data.el_data;
}


void el_data_PI_Put
      (const asn1SccEnvLab_Data *IN_el_data)
{
   ctxt_el_data.el_data = *IN_el_data;
}


