################################################################################
#                              H E R C C U L E S                               #
#            Copyright (C) 2022 Universidad Politécnica de Madrid              #
#                                                                              #
# This file have been developed by the real time systems group from UPM        #
################################################################################

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_C_COMPILER "arm-linux-gnueabihf-gcc")
set(CMAKE_C_COMPILER_TARGET ${arch})
set(CMAKE_CXX_COMPILER "arm-linux-gnueabihf-g++")
set(CMAKE_CXX_COMPILER_TARGET ${arch})

message(STATUS "[RPi.cmake] Using RPI cross compilers!")
