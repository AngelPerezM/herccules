// Fill in this class with your context data (internal state):
// list all the variables you want global (per function instance)
#include "dataview-uniq.h"

class env_lab_heaters_state {
public:
    asn1SccEnv_Lab_Heaters_Mode mode {asn1SccActuator_Control_Mode_autonomous_control};

    asn1SccHeater_Power_Type heaters_power_uel {0.0};
    asn1SccHeater_Power_Type heaters_power_del {0.0};
};
