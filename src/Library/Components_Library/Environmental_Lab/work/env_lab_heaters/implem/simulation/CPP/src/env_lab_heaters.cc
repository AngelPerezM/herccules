/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                           Env_Lab_Heaters Source                           --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "env_lab_heaters.h"
#include "env_lab_heaters_state.h"

/*******************************************************************************
 * Private variables and constants:
 ******************************************************************************/

namespace {
    env_lab_heaters_state ctxt;
}

/*******************************************************************************
 * Private functions:
 ******************************************************************************/

namespace {

}

/*******************************************************************************
 * Public functions:
 ******************************************************************************/

void env_lab_heaters_startup(void)
{

}

void env_lab_heaters_PI_Control_Heater_Auto
    (const asn1SccEnvLab_Heater_ID *IN_heater_id,
     const asn1SccHeater_Power_Type *IN_power)
{
    if (ctxt.mode == asn1SccActuator_Control_Mode_autonomous_control) {
        if (*IN_heater_id == asn1SccEnvLab_Heater_ID_upwards_heater) {
            ctxt.heaters_power_uel = *IN_power;
        } else {
            ctxt.heaters_power_del = *IN_power;
        }
    }
}

void env_lab_heaters_PI_Control_Heater_Manual
    (const asn1SccEnvLab_Heater_ID *IN_heater_id,
     const asn1SccHeater_Power_Type *IN_power)
{
    if (ctxt.mode == asn1SccActuator_Control_Mode_manual_control) {
        if (*IN_heater_id == asn1SccEnvLab_Heater_ID_upwards_heater) {
            ctxt.heaters_power_uel = *IN_power;
        } else {
            ctxt.heaters_power_del = *IN_power;
        }
    }
}

void env_lab_heaters_PI_Get_Heaters_Status
    (asn1SccEnv_Lab_Heaters_Mode *OUT_heaters_mode,
     asn1SccHeater_Power_Type    *OUT_heater_uel,
     asn1SccHeater_Power_Type    *OUT_heater_del)
{
    *OUT_heaters_mode = ctxt.mode;
    *OUT_heater_uel = ctxt.heaters_power_uel;
    *OUT_heater_del = ctxt.heaters_power_del;
}


void env_lab_heaters_PI_Set_Heaters_Mode
    (const asn1SccEnv_Lab_Heaters_Mode *IN_new_heaters_mode)
{
   ctxt.mode = *IN_new_heaters_mode;
}
