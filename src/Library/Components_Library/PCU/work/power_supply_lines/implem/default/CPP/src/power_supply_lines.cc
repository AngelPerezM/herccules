/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                       PCU_Power_Supply_Lines  Header                       --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "power_supply_lines.h"
#include "power_supply_lines_state.h"

#include "PCU.h"

namespace bs = board_support;

// --  Local variables  --------------------------------------------------------

namespace {
    power_supply_lines_state ctxt;
}

// --  Internal operations  ----------------------------------------------------

namespace {

    void set_ps_line(asn1SccPower_Supply_Line_ID  ps_line,
                     asn1SccSwitch_Status         status)
    {
        // Nested function to activate or deactivate on the real hardware:
        auto set_ps_line_hw = [status](bs::pcu::PowerSupplyLines line)
        {
            return (status == asn1SccSwitch_Status_on)
                 ? bs::pcu::activatePowerSupplyFor(line)
                 : bs::pcu::deactivatePowerSupplyFor(line);
        };


        // I miss enums as index, key feature in Ada!
        switch (ps_line) {
            case asn1SccPower_Supply_Line_ID_atl:
                if (set_ps_line_hw(bs::pcu::PowerSupplyLines::AL_LINE)) {
                    ctxt.ps_lines_status.al_line = status;
                }
                break;
            case asn1SccPower_Supply_Line_ID_tmu:
                if (set_ps_line_hw(bs::pcu::PowerSupplyLines::TMU_LINE)) {
                    ctxt.ps_lines_status.tmu_line = status;
                }
                break;
            case asn1SccPower_Supply_Line_ID_sdpu:
                if (set_ps_line_hw(bs::pcu::PowerSupplyLines::SDPU_LINE)) {
                    ctxt.ps_lines_status.sdpu_line = status;
                }
                break;
        }
    }

}


// --  Provided Interface  -----------------------------------------------------


// -------
// Startup
// -------

void power_supply_lines_startup(void)
{

}

// --------------------------
// Activate power supply line
// --------------------------

void power_supply_lines_PI_Activate_Power_Supply_Line
    (const asn1SccPower_Supply_Line_ID *IN_line)
{
    set_ps_line(*IN_line, asn1SccSwitch_Status_on);
}

// ----------------------------
// Deactivate power supply line
// ----------------------------

void power_supply_lines_PI_Deactivate_Power_Supply_Line
    (const asn1SccPower_Supply_Line_ID *IN_line)
{
    set_ps_line(*IN_line, asn1SccSwitch_Status_off);
}

// ----------------------------------
// Get power supply (PS) lines status
// ----------------------------------

void power_supply_lines_PI_Get_PS_Lines_Status
    (asn1SccPCU_PS_Lines_Status *OUT_line_status)
{
    *OUT_line_status = ctxt.ps_lines_status;
}


//             Controlled     |        IIC Buses usage        |
//                PCBS        | IIC-0 | IIC-1 | IIC-3 | IIC-4 |
//
// +-----+     +------+
// |     |---->| TMU  |-----> |   X   |       |       |       |
// |  P  |     +======+
// |     |---->| ATL  |-----> |       |       |       |       |
// |  C  |     +======+
// |     |---->| SDPU |-----> |       |   X   |   X   |       |
// |  U  |     +------+
// |     |------------------> |       |       |       |   X   |
// +-----+
