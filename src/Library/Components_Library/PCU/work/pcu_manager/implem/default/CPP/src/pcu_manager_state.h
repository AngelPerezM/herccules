/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                              PCU_Manager State                             --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

// Fill in this class with your context data (internal state):
// list all the variables you want global (per function instance)

#include <cstdio>
#include "dataview-uniq.h"
#include "Context-pcu-manager.h"
#include "CsvWriter.h"

class pcu_manager_state {
public:

    // --  Context data  -------------------------------------------------------

    bool end_of_mission {false};

    asn1SccPCU_Data pcu_data;

    using Logfile_type = data_storage::CsvWriter<12U>;

    const Logfile_type::Line header {
        // == Timestamps & modes ==
        "Snapshot time [secs]",
        "Snapshot time [msecs]",
        "Mission time [secs]",
        "PCU Mode",

        // == INA Data ==
        "Power [W]",
        "Current [A]",
        "Bus Voltage [V]",
        "Shunt Voltage [V]",

        // == TC74 Data ==
        "Temperature [C]",

        // == MOSFET switches status ==
        "ATL [ON/OFF]",
        "TMU [ON/OFF]",
        "SDPU [ON/OFF]"
    };

    Logfile_type logfile {pcu_manager_ctxt.logfile_name, ';', header};

    // --  Context operations  -------------------------------------------------

    static const char * image(const asn1SccSwitch_Status& x) {
        switch (x) {
            case asn1SccSwitch_Status_on:
                return "ON";
            case asn1SccSwitch_Status_off:
                return "OFF";
        }
        return "ERROR";
    }

    static const char * image(const asn1SccPCU_Mode& x) {
        switch (x) {
            case asn1SccPCU_Mode_on:
                return "Mode-ON";
            case asn1SccPCU_Mode_off:
                return "Mode-OFF";
        }
        return "Mode-ERROR";
    }
};
