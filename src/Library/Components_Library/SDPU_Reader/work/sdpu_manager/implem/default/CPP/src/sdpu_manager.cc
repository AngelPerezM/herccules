/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                            SDPU_Manager Source                             --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

// C++ body file for function SDPU_Manager

#include "sdpu_manager.h"
#include "sdpu_manager_state.h"

#include "SDPU.h" // Hardware I/F for SDPU board
#include "PCU.h"  // Hardware I/F for PCU board (turn on SDPU)

#include <iostream>

using namespace board_support;

/*******************************************************************************
 * Private variables and constants:
 ******************************************************************************/

namespace {
    sdpu_manager_state ctxt;
}

/*******************************************************************************
 * Private functions:
 ******************************************************************************/

namespace {

using namespace board_support::sdpu;

    inline void read_abs_baroms
        (PressureSensor IN_ps_ID,
         asn1SccAbsolute_Barometer &OUT_pressure)
    {
        if (BarometerReading br {}; getPressureFrom(IN_ps_ID, br)) {
            OUT_pressure.pressure_raw = br.pressure_raw;
            OUT_pressure.pressure_mbar = br.pressure_milliBar;
            OUT_pressure.temperature_raw = br.temperature_raw;
            OUT_pressure.temperature_celsius = br.temperature_celsius;
        }
    }

    inline void distribute_to_ATL_and_EL()
    {
        static asn1SccAtt_Lab_Data_Measurements atl_data;
        static asn1SccEnvLab_Experiment_Data_Sensors uel;
        static asn1SccEnvLab_Experiment_Data_Sensors del;
        static asn1SccEnvLab_Pressure_Data pressure;

        /**
         * Reads all analogue lines from all channels
         * where valid signals are present
         *
         * @{
         */
        auto reading {readAllADCChannels(MUXChannel::CH0)};
        uel.pyranometer_reading        = reading[0];
        uel.pyranometer_temperature    = reading[1];
        pressure.dif_barometers.arr[0] = reading[2];
        atl_data.photodiodes.arr[0]    = reading[3];

        reading = readAllADCChannels(MUXChannel::CH1);
        del.pyranometer_reading        = reading[0];
        del.pyranometer_temperature    = reading[1];
        pressure.dif_barometers.arr[1] = reading[2];
        atl_data.photodiodes.arr[1]    = reading[3];

        reading = readAllADCChannels(MUXChannel::CH2);
        uel.pyrgeometer_reading        = reading[0];
        uel.pyrgeometer_temperature    = reading[1];
        pressure.dif_barometers.arr[2] = reading[2];
        atl_data.photodiodes.arr[2]    = reading[3];

        reading = readAllADCChannels(MUXChannel::CH3);
        del.pyrgeometer_reading        = reading[0];
        del.pyrgeometer_temperature    = reading[1];
        pressure.dif_barometers.arr[3] = reading[2];
        atl_data.photodiodes.arr[3]    = reading[3];

        atl_data.thermistors.arr[0] = readRawFrom(PT1000_1);
        atl_data.thermistors.arr[1] = readRawFrom(PT1000_2);
        /** }@ */

        /**
         * Reads all absolute barometers:
         * @{
         */
        read_abs_baroms(PS1, pressure.abs_barometers.arr[0]);
        read_abs_baroms(PS2, pressure.abs_barometers.arr[1]);
        /** }@ */

        sdpu_manager_RI_Send_Env_Lab_Measurements(&uel, &del, &pressure);
        sdpu_manager_RI_Send_ATL_Measurements(&atl_data);
    }

    inline void distribute_only_to_EL()
    {
        static asn1SccEnvLab_Experiment_Data_Sensors uel;
        static asn1SccEnvLab_Experiment_Data_Sensors del;
        static asn1SccEnvLab_Pressure_Data pressure;

        /**
         * Reads all analogue lines from all channels
         * where valid signals FROM the EL are present
         *
         * @{
         */
        uel.pyranometer_reading = readRawFrom(UP_PYRANOMETER);
        uel.pyranometer_temperature = readRawFrom(THERM_1);
        pressure.dif_barometers.arr[0] = readRawFrom(DIFF_BAROM_1);

        del.pyranometer_reading = readRawFrom(DOWN_PYRANOMETER);
        del.pyranometer_temperature = readRawFrom(THERM_2);
        pressure.dif_barometers.arr[1] = readRawFrom(DIFF_BAROM_2);

        uel.pyrgeometer_reading = readRawFrom(UP_PYRGEOMETER);
        uel.pyrgeometer_temperature = readRawFrom(THERM_3);
        pressure.dif_barometers.arr[2] = readRawFrom(DIFF_BAROM_3);

        del.pyrgeometer_reading = readRawFrom(DOWN_PYRGEOMETER);
        del.pyrgeometer_temperature = readRawFrom(THERM_4);
        pressure.dif_barometers.arr[3] = readRawFrom(DIFF_BAROM_4);
        /** }@ */

        /**
         * Reads all absolute barometers:
         * @{
         */
        read_abs_baroms(PS1, pressure.abs_barometers.arr[0]);
        read_abs_baroms(PS2, pressure.abs_barometers.arr[1]);
        /** }@ */

        sdpu_manager_RI_Send_Env_Lab_Measurements(&uel, &del, &pressure);
    }

    inline void distribute_only_to_ATL()
    {
        static asn1SccAtt_Lab_Data_Measurements atl_data {};

        /**
         * Reads all analogue lines from all channels
         * where valid signals FROM the ATL are present
         *
         * @{
         */
        atl_data.photodiodes.arr[0] = readRawFrom(PHOTODIODE_1);
        atl_data.photodiodes.arr[1] = readRawFrom(PHOTODIODE_2);
        atl_data.photodiodes.arr[2] = readRawFrom(PHOTODIODE_3);
        atl_data.photodiodes.arr[3] = readRawFrom(PHOTODIODE_4);

        atl_data.thermistors.arr[0] = readRawFrom(PT1000_1);
        atl_data.thermistors.arr[1] = readRawFrom(PT1000_2);
        /** }@ */

        sdpu_manager_RI_Send_ATL_Measurements(&atl_data);
    }

}

/*******************************************************************************
 * Public Operations:
 ******************************************************************************/

/**
 * Order matters, first turn on SDPU before initialize it
 */
void sdpu_manager_startup(void) {
    pcu::initialize_switches();
    pcu::activatePowerSupplyFor(pcu::PowerSupplyLines::SDPU_LINE);

    sdpu::initialize();
}

void sdpu_manager_PI_Read_SDPU_Measurements(void) {
    asn1SccBalloon_Mode balloon_mode;
    sdpu_manager_RI_Get_Mode(&balloon_mode);

    bool EL_needs_measurements {false};
    sdpu_manager_RI_EL_Needs_Measurements(&balloon_mode, &EL_needs_measurements);
    bool ATL_needs_measurements {false};
    sdpu_manager_RI_ATL_Needs_Measurements(&balloon_mode, &ATL_needs_measurements);

    if (EL_needs_measurements && ATL_needs_measurements) {
        distribute_to_ATL_and_EL();
    } else if (EL_needs_measurements) {
        distribute_only_to_EL();
    } else if (ATL_needs_measurements) {
        distribute_only_to_ATL();
    }
}
