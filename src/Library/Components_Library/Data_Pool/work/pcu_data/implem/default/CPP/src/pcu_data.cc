// C+++ body file for function PCU_Data

#include "pcu_data.h"
#include "pcu_data_state.h"

namespace {
    pcu_data_state ctxt_pcu_data;
}

void pcu_data_startup(void)
{
    asn1SccPCU_Data_Initialize(&ctxt_pcu_data.pcu_data);
}

void pcu_data_PI_Get
      (asn1SccPCU_Data *OUT_pcu_data)

{
   *OUT_pcu_data = ctxt_pcu_data.pcu_data;
}


void pcu_data_PI_Put
      (const asn1SccPCU_Data *IN_pcu_data)
{
   ctxt_pcu_data.pcu_data = *IN_pcu_data;
}


