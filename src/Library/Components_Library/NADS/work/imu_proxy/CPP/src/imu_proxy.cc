/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                            NADS_IMU_Proxy Source                           --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "imu_proxy.h"
#include "imu_proxy_state.h"
#include "BNO055_IMU.h"

#include <cstdint>

using namespace equipment_handlers;

// --  Local variables  --------------------------------------------------------

namespace {
    imu_proxy_state ctxt;

    using bus_handlers::data::I2CBusID;

    // --  BNO055 IMU Config  --------------------------------------------------
    /**
     * @brief Configuration parameters for the IMU inside the NADS experiment
     * @{
     */
    constexpr auto imuI2CBusID   {I2CBusID::BUS1};
    constexpr auto imuI2CAddress {BNO055_IMU::BNO055_Primary_Address};
    constexpr auto opMode        {BNO055_IMU::NDOF};
    constexpr auto powerMode     {BNO055_IMU::NORMAL};

    constexpr auto accelerationUnits {BNO055_IMU::METERS_SECOND_SQUARED};
    constexpr auto angularRateUnits  {BNO055_IMU::RADIANS_SECOND};
    constexpr auto eulerAngleUnits   {BNO055_IMU::RADIANS};
    constexpr auto temperatureUnits  {BNO055_IMU::CELSIUS};
    constexpr auto pitchRotationConvention {BNO055_IMU::CLOCKWISE_DECREASING};
    /*@}*/
}

// --  Local operations  -------------------------------------------------------

namespace  {
    bool imu_initialize(void)
    {
        bool imuSuccess {
            ctxt.imu.initialize(imuI2CAddress, imuI2CBusID, opMode, powerMode)
        };

        imuSuccess = imuSuccess && ctxt.imu.setAccelerationUnits(accelerationUnits);
        imuSuccess = imuSuccess && ctxt.imu.setAngularRateUnits(angularRateUnits);
        imuSuccess = imuSuccess && ctxt.imu.setEulerAngleUnits(eulerAngleUnits);
        imuSuccess = imuSuccess && ctxt.imu.setTemperatureUnits(temperatureUnits);
        imuSuccess = imuSuccess && ctxt.imu.setPitchRotation(pitchRotationConvention);

        return imuSuccess;
    }

    asn1SccIMU_OperatingMode to_local_mode(BNO055_IMU::OperatingModes mode) {
        switch (mode) {
        case BNO055_IMU::CONFIG:
            return asn1SccIMU_OperatingMode_config;
        case BNO055_IMU::ACC_ONLY:
            return asn1SccIMU_OperatingMode_acc_only;
        case BNO055_IMU::MAG_ONLY:
            return asn1SccIMU_OperatingMode_mag_only;
        case BNO055_IMU::GYRO_ONLY:
            return asn1SccIMU_OperatingMode_gyro_only;
        case BNO055_IMU::ACC_MAG:
            return asn1SccIMU_OperatingMode_acc_mag;
        case BNO055_IMU::ACC_GYRO:
            return asn1SccIMU_OperatingMode_acc_gyro;
        case BNO055_IMU::MAG_GYRO:
            return asn1SccIMU_OperatingMode_mag_gyro;
        case BNO055_IMU::AMG:
            return asn1SccIMU_OperatingMode_amg;
        case BNO055_IMU::IMU:
            return asn1SccIMU_OperatingMode_imu;
        case BNO055_IMU::COMPASS:
            return asn1SccIMU_OperatingMode_compass;
        case BNO055_IMU::M4G:
            return asn1SccIMU_OperatingMode_m4g;
        case BNO055_IMU::NDOF_FMS_OFF:
            return asn1SccIMU_OperatingMode_ndof_fmc_off;
        case BNO055_IMU::NDOF:
            return asn1SccIMU_OperatingMode_ndof;
        }

        return asn1SccIMU_OperatingMode_config;
    }

    BNO055_IMU::OperatingModes to_imu_mode(asn1SccIMU_OperatingMode mode) {
        switch (mode) {
        case asn1SccIMU_OperatingMode_config:
            return BNO055_IMU::CONFIG;
        case asn1SccIMU_OperatingMode_acc_only:
            return BNO055_IMU::ACC_ONLY;
        case asn1SccIMU_OperatingMode_mag_only:
            return BNO055_IMU::MAG_ONLY;
        case asn1SccIMU_OperatingMode_gyro_only:
            return BNO055_IMU::GYRO_ONLY;
        case asn1SccIMU_OperatingMode_acc_mag:
            return BNO055_IMU::ACC_MAG;
        case asn1SccIMU_OperatingMode_acc_gyro:
            return BNO055_IMU::ACC_GYRO;
        case asn1SccIMU_OperatingMode_mag_gyro:
            return BNO055_IMU::MAG_GYRO;
        case asn1SccIMU_OperatingMode_amg:
            return BNO055_IMU::AMG;
        case asn1SccIMU_OperatingMode_imu:
            return BNO055_IMU::IMU;
        case asn1SccIMU_OperatingMode_compass:
            return BNO055_IMU::COMPASS;
        case asn1SccIMU_OperatingMode_m4g:
            return BNO055_IMU::M4G;
        case asn1SccIMU_OperatingMode_ndof_fmc_off:
            return BNO055_IMU::NDOF_FMS_OFF;
        case asn1SccIMU_OperatingMode_ndof:
            return BNO055_IMU::NDOF;
        }

        return BNO055_IMU::CONFIG;
    }
}

// --  Provided Interface  -----------------------------------------------------

// --------------
// -- Start up --
// --------------

void imu_proxy_startup(void)
{
    imu_initialize();
}

// -------------------
// -- Read IMU Data --
// -------------------

void imu_proxy_PI_Read_IMU_Data
    (const asn1SccUINT8_Type *IN_cycle,
           asn1SccIMU_Data   *OUT_imu_data)
{
    using equipment_handlers::BNO055_IMU;

    // --  Read magnetometer  --------------------------------------------------
    if (*IN_cycle == 1U) {
        BNO055_IMU::MagField     mgm {};
        BNO055_IMU::Temperatures ts  {};
        ctxt.imu.readSensorData(mgm);
        ctxt.imu.readTemperatures(ts);

        OUT_imu_data->sensors_data.mag_field = {mgm.X, mgm.Y, mgm.Z};
        OUT_imu_data->temperatures = {ts.from_accelerometer, ts.from_gyroscope};
    }

    // --  Read accelerometer and gyroscope  -----------------------------------
    BNO055_IMU::Acceleration    accel {};
    BNO055_IMU::AngularVelocity gyro  {};
    ctxt.imu.readSensorData(accel);
    ctxt.imu.readSensorData(gyro);

    OUT_imu_data->sensors_data.acceleration     = {accel.X , accel.Y , accel.Z};
    OUT_imu_data->sensors_data.angular_velocity = {gyro.X  , gyro.Y  , gyro.Z};

    // --  Read fused data  ----------------------------------------------------
    BNO055_IMU::FusedData fused;
    ctxt.imu.readAllFusedData(fused);

    OUT_imu_data->fusion_data = {
        .euler_orientation = {
            fused.eulerOrientation.X,
            fused.eulerOrientation.Y,
            fused.eulerOrientation.Z
        },
        .liner_acceleration = {
            fused.linearAcceleration.X,
            fused.linearAcceleration.Y,
            fused.linearAcceleration.Z
        },
        .gravity = {
            fused.gravity.X,
            fused.gravity.Y,
            fused.gravity.Z
        },
        .quaternion_orientation = {
            fused.quat.W,
            fused.quat.X,
            fused.quat.Y,
            fused.quat.Z
        }
    };
}

// ---------------
// -- Reset IMU --
// ---------------

void imu_proxy_PI_Reset_IMU(void) {
    ctxt.imu.reset() && imu_initialize();
}

// -----------------------------
// -- Set Calibration Offsets --
// -----------------------------

void imu_proxy_PI_Set_IMU_Calibration_Offsets
    (const asn1SccIMU_CalibrationOffsets * IN_calibration_offsets)
{
    float lsb_accel {ctxt.imu.getAccelerationUnits() == BNO055_IMU::MILLIGRAVITY   ? 1.00F : 100.0F};
    float lsb_gyro  {ctxt.imu.getAngularRateUnits()  == BNO055_IMU::DEGREES_SECOND ? 16.0F : 900.0F};
    float lsb_mgm   {16.0F};
    const BNO055_IMU::SensorOffsetValues offsets {
        .accel_x = IN_calibration_offsets->accel_offset_x * lsb_accel,
        .accel_y = IN_calibration_offsets->accel_offset_y * lsb_accel,
        .accel_z = IN_calibration_offsets->accel_offset_z * lsb_accel,

        .gyro_x = IN_calibration_offsets->gyro_offset_x * lsb_gyro,
        .gyro_y = IN_calibration_offsets->gyro_offset_y * lsb_gyro,
        .gyro_z = IN_calibration_offsets->gyro_offset_z * lsb_gyro,

        .mgm_x = IN_calibration_offsets->mgm_offset_x * lsb_mgm,
        .mgm_y = IN_calibration_offsets->mgm_offset_y * lsb_mgm,
        .mgm_z = IN_calibration_offsets->mgm_offset_z * lsb_mgm,

        .accel_radius = IN_calibration_offsets->accel_radius,
        .mgm_radius   = IN_calibration_offsets->mgm_radius
    };
    ctxt.imu.SetSensorsOffsets(offsets);
}

// ------------------------
// -- Get IMU Parameters --
// ------------------------

void imu_proxy_PI_Get_IMU_Parameters(asn1SccIMU_Parameters * OUT_imu_parameters) {
    // - Op mode:
    OUT_imu_parameters->operating_mode = to_local_mode(ctxt.imu.getCurrentMode());

    // - Axes configuration:
    auto axes_conf {ctxt.imu.getAxesConfiguration()};
    OUT_imu_parameters->axes_configuration.x = {
        .sign  = static_cast<asn1SccIMU_AxisSelection_sign>(axes_conf.x.sign),
        .remap = static_cast<asn1SccIMU_AxisSelection_remap>(axes_conf.x.remapping),
    };
    OUT_imu_parameters->axes_configuration.y = {
        .sign  = static_cast<asn1SccIMU_AxisSelection_sign>(axes_conf.y.sign),
        .remap = static_cast<asn1SccIMU_AxisSelection_remap>(axes_conf.y.remapping)
    };
    OUT_imu_parameters->axes_configuration.z = {
        .sign  = static_cast<asn1SccIMU_AxisSelection_sign>(axes_conf.z.sign),
        .remap = static_cast<asn1SccIMU_AxisSelection_remap>(axes_conf.z.remapping)
    };

    // - Calibration complete:
    OUT_imu_parameters->calibration_parameters.calibration_complete = ctxt.imu.calibrationComplete();

    // - Calibration status:
    auto calibs {ctxt.imu.getSensorsCalibration()};
    OUT_imu_parameters->calibration_parameters.calibration_states = {
        .platform      = static_cast<asn1SccIMU_CalibrationLevel>(calibs.Platform),
        .gyroscope     = static_cast<asn1SccIMU_CalibrationLevel>(calibs.Gyroscope),
        .accelerometer = static_cast<asn1SccIMU_CalibrationLevel>(calibs.Accelerometer),
        .magnetometer  = static_cast<asn1SccIMU_CalibrationLevel>(calibs.Magnetometer)
    };

    // - Axes offsets:
    float lsb_accel {ctxt.imu.getAccelerationUnits() == BNO055_IMU::MILLIGRAVITY   ? 1.00F : 100.0F};
    float lsb_gyro  {ctxt.imu.getAngularRateUnits()  == BNO055_IMU::DEGREES_SECOND ? 16.0F : 900.0F};
    float lsb_mgm   {16.0F};
    auto offsets {ctxt.imu.getSensorOffsets()};
    OUT_imu_parameters->calibration_parameters.offsets = {
        .accel_offset_x = offsets.accel_x / lsb_accel,
        .accel_offset_y = offsets.accel_y / lsb_accel,
        .accel_offset_z = offsets.accel_z / lsb_accel,

        .gyro_offset_x = offsets.gyro_x / lsb_gyro,
        .gyro_offset_y = offsets.gyro_y / lsb_gyro,
        .gyro_offset_z = offsets.gyro_z / lsb_gyro,

        .mgm_offset_x = offsets.mgm_x / lsb_mgm,
        .mgm_offset_y = offsets.mgm_y / lsb_mgm,
        .mgm_offset_z = offsets.mgm_z / lsb_mgm,

        .accel_radius = offsets.accel_radius,
        .mgm_radius   = offsets.mgm_radius
    };
}

// --------------------
// -- Configure Axes --
// --------------------

void imu_proxy_PI_IMU_Configure_Axes(const asn1SccIMU_AxesConfiguration * IN_axes_config) {
    BNO055_IMU::AxesRemapping axes {
        .x = static_cast<BNO055_IMU::AxisRemappingSelection>(IN_axes_config->x.remap),
        .y = static_cast<BNO055_IMU::AxisRemappingSelection>(IN_axes_config->y.remap),
        .z = static_cast<BNO055_IMU::AxisRemappingSelection>(IN_axes_config->z.remap)
    };
    ctxt.imu.RemapAxes(axes);

    BNO055_IMU::AxesSigns signs {
        .x = static_cast<BNO055_IMU::AxisSignSelection>(IN_axes_config->x.sign),
        .y = static_cast<BNO055_IMU::AxisSignSelection>(IN_axes_config->y.sign),
        .z = static_cast<BNO055_IMU::AxisSignSelection>(IN_axes_config->z.sign)
    };
    ctxt.imu.RemapSigns(signs);
}

// ----------------------------
// -- Set IMU Operating Mode --
// ----------------------------

void imu_proxy_PI_Set_IMU_Operating_Mode(const asn1SccIMU_OperatingMode * IN_new_mode) {
    ctxt.imu.setMode(to_imu_mode(*IN_new_mode));
}

