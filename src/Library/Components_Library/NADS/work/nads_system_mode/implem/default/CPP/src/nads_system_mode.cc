/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                           NADS_System_Mode Source                          --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "nads_system_mode.h"
#include "nads_system_mode_state.h"

// Define and use function state inside this context structure
// avoid defining global/static variable elsewhere
namespace {
    nads_system_mode_state ctxt;
}

void nads_system_mode_startup(void)
{
    ctxt.current_mode = asn1SccBalloon_Mode_ground_await;
}

void nads_system_mode_PI_Get_Mode
    (asn1SccBalloon_Mode *OUT_system_mode)
{
    *OUT_system_mode = ctxt.current_mode;
}


void nads_system_mode_PI_Notify_Mode_Change_NADS
    (const asn1SccBalloon_Mode *IN_new_mode)
{
    ctxt.current_mode = *IN_new_mode;
}
