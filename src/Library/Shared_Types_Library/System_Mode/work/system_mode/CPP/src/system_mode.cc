/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                             COMPONENTS LIBRARY                             --
--                                                                            --
--                             System_Mode Source                             --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "system_mode.h"

void system_mode::startup()
{
    ctxt.current_mode = asn1SccBalloon_Mode_ground_await;
}

void system_mode::Set_Mode
    (const asn1SccBalloon_Mode *IN_new_mode)
{
    ctxt.current_mode = *IN_new_mode;
}

void system_mode::Get_Mode
    (asn1SccBalloon_Mode *OUT_current_mode)
{
    *OUT_current_mode = ctxt.current_mode;
}
