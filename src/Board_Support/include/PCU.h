/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              BOARD  SUPPORT                                --
--                                                                            --
--                                PCU Header                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef HAL_PCU_H
#define HAL_PCU_H

#include <cstdint>

namespace board_support::pcu {

    enum PowerSupplyLines : uint8_t {
        SDPU_LINE = 0U,
        TMU_LINE  = 1U,
        AL_LINE   = 2U
    };

    enum EnvLabPWMHeaters : uint8_t {
        UpwardsELHeater   = 0U,
        DownwardsELHeater = 1U
    };

    enum HTLPWMHeater : uint8_t {
        Heater1 = 0U,
        Heater2 = 1U,
        Heater3 = 2U,
        Heater4 = 3U
    };

    bool initialize_switches();

    bool initialize_sensors();

    bool activatePowerSupplyFor(PowerSupplyLines line);

    bool deactivatePowerSupplyFor(PowerSupplyLines line);

    bool setEnvLabPWMDutyCycle(EnvLabPWMHeaters heater, float pwmPercentage);

    void getEnvLabPWMDutyCycle(float &pwm_uel, float &pwm_del);

    bool setHTLPWMDutyCycle(HTLPWMHeater heater, float pwmPercentage);

    void getHTLPWMDutyCycle(float &pwm_htl1, float &pwm_htl2,
                            float &pwm_htl3, float &pwm_htl4);

    /**
     * Configures the TC74 in Normal mode, in which the TC74 consumes 200uA.
     */
    bool activateTC74();

    bool readTC74(int8_t &temperature);

    /**
     * Configures the TC74 in Stand-By mode, in which the TC74 consumes 5uA.
     */
    bool deactivateTC74();

    struct PowerAndCurrentData {
        float busVoltage_V;
        float shuntVoltage_V;
        float power_W;
        float current_A;
    };

    bool readPowerAndCurrent(PowerAndCurrentData & data);

    bool resetPowerAndCurrentSensor();

}

#endif //HAL_PCU_H
