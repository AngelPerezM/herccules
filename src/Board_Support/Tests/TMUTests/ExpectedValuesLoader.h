// =============================================================================
// --                                                                         --
// --                   HERCCULES Software Components                         --
// --                                                                         --
// --               Equipment_Handlers - Expected Values Loader                --
// --                                                                         --
// --         Copyright (C) 2022 Universidad Politécnica de Madrid            --
// =============================================================================

#ifndef OBSW_EXPECTEDVALUESLOADER_H
#define OBSW_EXPECTEDVALUESLOADER_H

#include "TMU.h"
#include "cstdint"
#include <array>
#include <string>

namespace expected_values_loader {

    bool initialize(std::string fileName);

    std::array <int16_t, 4U> expectedValuesFrom(board_support::tmu::MUXChannel muxChannel);

    int16_t expectedValueFrom(board_support::tmu::PT1000_ID pt1000Id);
}

#endif //OBSW_EXPECTEDVALUESLOADER_H
