// =============================================================================
// --                                                                         --
// --                   HERCCULES Software Components                         --
// --                                                                         --
// --               Equipment_Handlers - Expected Values Loader                --
// --                                                                         --
// --         Copyright (C) 2022 Universidad Politécnica de Madrid            --
// =============================================================================

#include "ExpectedValuesLoader.h"
#include <fstream>
#include <iostream>
#include <sstream>

using std::cout;

namespace {
    std::ifstream csvFile;

    constexpr uint nOfADCChannels = 4U;
    constexpr uint nOfMUXChannels = 7U;

    using ADCReadings = std::array<int16_t, nOfADCChannels>;

    using MUXReadings = std::array<ADCReadings, nOfMUXChannels>;

    MUXReadings expectedValues;

    void parseLine (const std::string &line, ADCReadings & mux) {
        std::string id;
        std::stringstream ss (line);

        std::string substr;
        (void) getline(ss, substr, ','); // get first string delimited by comma
        std::cout << "Reading row from " << substr << ": ";

        unsigned adcChannel = 0U;
        while (ss.good() &&
               adcChannel < nOfADCChannels)
        {
            (void) getline(ss, substr, ',');
            int value = std::stoi(substr);
            mux.at(adcChannel) = static_cast<uint16_t>(value);
            std::cout << " | " << value;

            adcChannel++;
        }
        std::cout << std::endl;
    }

    // Format at row 0:
    // MUX_Channel_0-ADC_Channel-0 | MUX_Channel_0-ADC_Channel-1 | MUX_Channel_0-ADC_Channel-2 |  ...
    bool loadValues() {
        // skip header:
        std::string header;
        (void) std::getline(csvFile, header);
        std::cout << header << std::endl;

        // read contents:
        unsigned int muxChannel = 0U;
        std::string line;
        while (getline(csvFile, line) &&
               muxChannel < nOfMUXChannels)
        {
            std::string ID;
            auto & mux = expectedValues.at(muxChannel);
            parseLine(line, mux);
            muxChannel++;
        }

        bool readAllChannels = muxChannel > nOfMUXChannels;
        return readAllChannels;
    }

    std::pair<unsigned int, unsigned int> toMuxChannelPair(const board_support::tmu::PT1000_ID pt1000ID) {
        using namespace board_support::tmu;

        unsigned int muxChannel = 0U;
        unsigned int adcChannel = 0U;

        if (pt1000ID <= PT1000_7) {
            // 1 to 7
            muxChannel = pt1000ID - PT1000_1;
            adcChannel = 0U;
        } else if (pt1000ID <= PT1000_14) {
            // 8 to 14
            muxChannel = pt1000ID - PT1000_8;
            adcChannel = 1U;
        } else if (pt1000ID <= PT1000_21) {
            // 15 to 21
            muxChannel = pt1000ID - PT1000_15;
            adcChannel = 2U;
        } else if (pt1000ID <= PT1000_28) {
            // 22 to 28
            muxChannel = pt1000ID - PT1000_22;
            adcChannel = 3U;
        } else {
            std::cerr << "PT1000 identifier out of range, GT 28" << std::endl;
        }

        return std::make_pair(muxChannel, adcChannel);
    }
}

namespace expected_values_loader {

    bool initialize(std::string fileName) {
        csvFile.open(fileName);
        if (csvFile.fail()) {
            return false;
        }
        if (!loadValues()) {
            return false;
        }
        return true;
    }

    std::array <int16_t, 4U> expectedValuesFrom(board_support::tmu::MUXChannel muxChannel) {
        return expectedValues.at(static_cast<uint>(muxChannel));
    }

    int16_t expectedValueFrom(board_support::tmu::PT1000_ID pt1000Id) {
        auto [muxChannel, adcChannel] = toMuxChannelPair(pt1000Id);
        return expectedValuesFrom(static_cast<board_support::tmu::MUXChannel>(muxChannel))
                    .at(adcChannel);
    }

}
