// =============================================================================
// --                                                                         --
// --                   HERCCULES Software Components                         --
// --                                                                         --
// --               Equipment_Handlers - Expected Values Loader                --
// --                                                                         --
// --         Copyright (C) 2022 Universidad Politécnica de Madrid            --
// =============================================================================

#include "ExpectedValuesLoader.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>

using std::cout;

namespace {
    std::ifstream csvFile;

    constexpr uint nOfADCChannels = 4U;
    constexpr uint nOfMUXChannels = 7U;

    using ADCReadings = std::array<int16_t, nOfADCChannels>;

    using MUXReadings = std::array<ADCReadings, nOfMUXChannels>;

    MUXReadings expectedValues;

    void parseLine (const std::string &line, ADCReadings & mux) {
        std::string id;
        std::stringstream ss (line);

        std::string substr;
        (void) getline(ss, substr, ','); // get first string delimited by comma
        std::cout << "Reading row from " << substr << ": ";

        unsigned adcChannel = 0U;
        while (ss.good() &&
               adcChannel < nOfADCChannels)
        {
            (void) getline(ss, substr, ',');
            int value = std::stoi(substr);
            mux.at(adcChannel) = static_cast<uint16_t>(value);
            std::cout << value << " | ";

            adcChannel++;
        }
        std::cout << std::endl;
    }

    // Format at row 0:
    // MUX_Channel_0-ADC_Channel-0 | MUX_Channel_0-ADC_Channel-1 | MUX_Channel_0-ADC_Channel-2 |  ...
    bool loadValues() {
        // skip header:
        std::string header;
        (void) std::getline(csvFile, header);
        std::cout << header << std::endl;

        // read contents:
        unsigned int muxChannel = 0U;
        std::string line;
        while (getline(csvFile, line) &&
               muxChannel < nOfMUXChannels)
        {
            std::string ID;
            auto & mux = expectedValues.at(muxChannel);
            parseLine(line, mux);
            muxChannel++;
        }

        bool readAllChannels = muxChannel > nOfMUXChannels;
        return readAllChannels;
    }

    std::pair<unsigned int, unsigned int> toMuxChannelPair(const board_support::sdpu::AnalogDevice analogDevice) {
        using namespace board_support::sdpu;
        static const auto lut =
                std::map <AnalogDevice, std::pair<unsigned int, unsigned int>>
        {
            {UP_PYRANOMETER,  {0, 0}},
            {DOWN_PYRANOMETER,  {1, 0}},
            {UP_PYRGEOMETER, {2, 0}},
            {DOWN_PYRGEOMETER, {3, 0}},
            {THERM_1, {0, 1}},
            {THERM_2, {1, 1}},
            {THERM_3, {2, 1}},
            {THERM_4, {3, 1}},
            {PT1000_1, {4, 1}},
            {PT1000_2, {5, 1}},
            {DIFF_BAROM_1, {0, 2}},
            {DIFF_BAROM_2, {1, 2}},
            {DIFF_BAROM_3, {2, 2}},
            {DIFF_BAROM_4, {3, 2}},
            {PHOTODIODE_1, {0, 3}},
            {PHOTODIODE_2, {1, 3}},
            {PHOTODIODE_3, {2, 3}},
            {PHOTODIODE_4, {3, 3}}
        };

        return lut.at(analogDevice);
    }
}

namespace expected_values_loader {

    bool initialize(std::string fileName) {
        csvFile.open(fileName);
        if (csvFile.fail()) {
            return false;
        }
        if (!loadValues()) {
            return false;
        }
        return true;
    }

    std::array <int16_t, 4U> expectedValuesFrom(board_support::sdpu::MUXChannel muxChannel) {
        return expectedValues.at(static_cast<uint>(muxChannel));
    }

    int16_t expectedValueFrom(board_support::sdpu::AnalogDevice analogDevice) {
        auto [muxChannel, adcChannel] = toMuxChannelPair(analogDevice);
        return expectedValuesFrom(static_cast<board_support::sdpu::MUXChannel>(muxChannel))
                    .at(adcChannel);
    }

}
