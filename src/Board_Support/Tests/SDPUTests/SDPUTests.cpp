/*****************************************************************************
**                                                                          **
**                    HERCCULES Software Components                         **
**                                                                          **
**                         Equipment_Handlers                                **
**                                                                          **
**                            SDPU Tests                                    **
**                                                                          **
**          Copyright (C) 2022 Universidad Politécnica de Madrid            **
**                                                                          **
** This is free software;  you can redistribute it  and/or modify it  under **
** terms of the  GNU General Public License as published  by the Free Soft- **
** ware  Foundation;  either version 3,  or (at your option) any later ver- **
** sion.  This software is distributed in the hope  that it will be useful, **
** but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- **
** TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public **
** License for  more details.  You should have  received  a copy of the GNU **
** General  Public  License  distributed  with  this  software;   see  file **
** COPYING3.  If not, go to http://www.gnu.org/licenses for a complete copy **
** of the license.                                                          **
**                                                                          **
*****************************************************************************/

#include "SDPU.h"
#include <gtest/gtest.h>
#include <cmath>    // fabs
#include "ExpectedValuesLoader.h"

namespace bs_sdpu = board_support::sdpu;

// ===============
// SDPUTests class
// ===============

class SDPUTests : public ::testing::Test {
protected:

    static constexpr float maximumDeviationRaw = 0.01 * 1E6 / 125; // Means 0.01 volts

    float expectedPressure          = 954.0F;
    float maxPressureDeviation_mBar = 5.0F;

    static void SetUpTestCase() {
        (void) bs_sdpu::initialize();
        (void) expected_values_loader::initialize("./ExpectedValues.csv");
    }

    // ------------------------------------
    // Auxiliary methods for the unit tests
    // ------------------------------------

    static void ExpectedReadingFromOneLine(bs_sdpu::AnalogDevice analog) {
        int16_t reading  = bs_sdpu::readRawFrom(analog);
        int16_t expected = expected_values_loader::expectedValueFrom(analog);

        EXPECT_NEAR(reading, expected, maximumDeviationRaw);
    }

    static void ExpectedReadingFromVariousLines(const std::vector<bs_sdpu::AnalogDevice> &analogLines) {
        for (const auto &line : analogLines) {
            SCOPED_TRACE("Test for analogue line = " + std::to_string(line));
            ExpectedReadingFromOneLine(line);
            static constexpr auto periodNSecs = static_cast<int>((1.0F / 8.0F) * 1E+9);
            waitNSecs(periodNSecs);
        }
    }

    static void ExpectReadingsFromAllADCChannels(bs_sdpu::MUXChannel muxChannel) {
        auto readings = bs_sdpu::readAllADCChannels(static_cast<bs_sdpu::MUXChannel>(muxChannel));
        int adcChannel = 0;
        auto expectedValues = expected_values_loader::expectedValuesFrom(muxChannel);
        for (auto reading : readings) {
            SCOPED_TRACE("Test for ADC Channel [0 --> 3] = " + std::to_string(adcChannel));
            EXPECT_NEAR(reading, expectedValues.at(adcChannel), maximumDeviationRaw);
            adcChannel++;
        }
    }

    static void CompareIndividualsWithCompletesFrom(std::vector<bs_sdpu::AnalogDevice> lines,
                                                    bs_sdpu::MUXChannel channel)
    {
        auto completeReadings =
           bs_sdpu::readAllADCChannels(static_cast<bs_sdpu::MUXChannel>(channel));

        // std::cout << "MUX CHANNEL: "<< channel << std::endl;
        uint idx = 0U;
        for (const auto &line : lines) {
            auto individualReading = bs_sdpu::readRawFrom(line);
            // std::cout << "  - individual: " << individualReading << " line: " << completeReadings[idx] << std::endl;
            EXPECT_NEAR(individualReading, completeReadings[idx], maximumDeviationRaw);
            idx++;
        }
    }

    /// Violates MISRA 18-0-4, but `clock_nanosleep` is recommended in the Burns & Wellings RTS&PL book.
    static void waitNSecs(int transitionTimeNSecs) {
        struct timespec wait {
                .tv_sec = 0,
                .tv_nsec = transitionTimeNSecs
        };

        if(clock_nanosleep(CLOCK_MONOTONIC, 0, &wait, nullptr) < 0) {
            perror("clock_nanosleep");
        }
    }
};

// --------------
// All unit tests
// --------------

TEST_F(SDPUTests, InitializationSuccess) {
    ASSERT_TRUE(bs_sdpu::initialize()) <<
        "----------------ooOOoo-----------------\n"
        "Could NOT initialize the SDPU subsystem\n"
        "----------------ooOOoo-----------------";
}

TEST_F(SDPUTests, MultipleInitializations) {
    ASSERT_TRUE(bs_sdpu::initialize() &&
                bs_sdpu::initialize()) <<
        "------------------ooOOoo------------------\n"
        "Multiple initialization the SDPU subsystem\n"
        "------------------ooOOoo------------------";
}

TEST_F(SDPUTests, CheckAllADCChannelsReverse) {
    auto firstChannel = static_cast<int>(bs_sdpu::MUXChannel::CH0);
    auto lastChannel  = static_cast<int>(bs_sdpu::MUXChannel::CH4);
    for (auto channel = lastChannel; channel >= firstChannel; --channel) {
        SCOPED_TRACE("Test for MUX channel [0 --> 4] = " + std::to_string(channel));
        ExpectReadingsFromAllADCChannels(static_cast<bs_sdpu::MUXChannel>(channel));
    }
}

TEST_F(SDPUTests, CheckAllADCChannels) {
    auto firstChannel = static_cast<int>(bs_sdpu::MUXChannel::CH0);
    auto lastChannel  = static_cast<int>(bs_sdpu::MUXChannel::CH4);
    for (auto channel = firstChannel; channel >= lastChannel; ++channel) {
        SCOPED_TRACE("Test for MUX channel [0 --> 4] = " + std::to_string(channel));
        ExpectReadingsFromAllADCChannels(static_cast<bs_sdpu::MUXChannel>(channel));
    }
}

TEST_F(SDPUTests, CheckPyranometerReadings) {
    std::vector<bs_sdpu::AnalogDevice> analogLines {
            bs_sdpu::AnalogDevice::UP_PYRANOMETER,
            bs_sdpu::AnalogDevice::DOWN_PYRANOMETER
    };

    ExpectedReadingFromVariousLines(analogLines);
}

TEST_F(SDPUTests, CheckPyrgeometerReadings) {
    std::vector<bs_sdpu::AnalogDevice> analogLines {
            bs_sdpu::AnalogDevice::UP_PYRGEOMETER,
            bs_sdpu::AnalogDevice::DOWN_PYRGEOMETER
    };

    ExpectedReadingFromVariousLines(analogLines);
}

TEST_F(SDPUTests, CheckPyranometersAndPyrgeometersTemperatures) {
    std::vector<bs_sdpu::AnalogDevice> analogLines {
            bs_sdpu::AnalogDevice::THERM_1,
            bs_sdpu::AnalogDevice::THERM_2,
            bs_sdpu::AnalogDevice::THERM_3,
            bs_sdpu::AnalogDevice::THERM_4
    };

    ExpectedReadingFromVariousLines(analogLines);
}

TEST_F(SDPUTests, CheckNadirTemperature) {
    std::vector<bs_sdpu::AnalogDevice> analogLines {
            bs_sdpu::AnalogDevice::PT1000_1,
            bs_sdpu::AnalogDevice::PT1000_2
    };

    ExpectedReadingFromVariousLines(analogLines);
}

TEST_F(SDPUTests, CheckNadirPhotodiodes) {
    std::vector<bs_sdpu::AnalogDevice> analogLines {
            bs_sdpu::AnalogDevice::PHOTODIODE_1,
            bs_sdpu::AnalogDevice::PHOTODIODE_2,
            bs_sdpu::AnalogDevice::PHOTODIODE_3,
            bs_sdpu::AnalogDevice::PHOTODIODE_4
    };

    ExpectedReadingFromVariousLines(analogLines);
}

TEST_F(SDPUTests, CheckDifferentialBarometers) {
    std::vector<bs_sdpu::AnalogDevice> analogLines {
            bs_sdpu::AnalogDevice::DIFF_BAROM_1,
            bs_sdpu::AnalogDevice::DIFF_BAROM_2,
            bs_sdpu::AnalogDevice::DIFF_BAROM_3,
            bs_sdpu::AnalogDevice::DIFF_BAROM_4
    };

    ExpectedReadingFromVariousLines(analogLines);
}

TEST_F(SDPUTests, CheckAbsoluteBarometers) {
    bs_sdpu::BarometerReading reading;

    bs_sdpu::getPressureFrom(bs_sdpu::PS1, reading);
    SCOPED_TRACE("Test for Absolute Barometer 1");
    EXPECT_NEAR(reading.pressure_milliBar, expectedPressure, maxPressureDeviation_mBar);

    bs_sdpu::getPressureFrom(bs_sdpu::PS2, reading);
    SCOPED_TRACE("Test for Absolute Barometer 2");
    EXPECT_NEAR(reading.pressure_milliBar, expectedPressure, maxPressureDeviation_mBar);
}

TEST_F(SDPUTests, AbsoluteBarometersAreSimilar) {
    bs_sdpu::BarometerReading fromPS1;
    bs_sdpu::BarometerReading fromPS2;

    bs_sdpu::getPressureFrom(bs_sdpu::PS1, fromPS1);
    bs_sdpu::getPressureFrom(bs_sdpu::PS2, fromPS2);

    SCOPED_TRACE("Similar pressure");
    EXPECT_NEAR(fromPS1.pressure_milliBar, fromPS2.pressure_milliBar, 1.0F);

    SCOPED_TRACE("Similar Temperature");
    EXPECT_NEAR(fromPS1.temperature_celsius, fromPS2.temperature_celsius, 5.0F);
}

TEST_F(SDPUTests, CompareIndividialReadingsWithCompleteReadingsFromChannel0) {
    std::vector<bs_sdpu::AnalogDevice> lines {
        bs_sdpu::UP_PYRANOMETER,
        bs_sdpu::THERM_1,
        bs_sdpu::DIFF_BAROM_1,
        bs_sdpu::PHOTODIODE_1
    };
    CompareIndividualsWithCompletesFrom(lines, bs_sdpu::MUXChannel::CH0);
}

TEST_F(SDPUTests, CompareIndividialReadingsWithCompleteReadingsFromChannel1) {
    std::vector<bs_sdpu::AnalogDevice> lines {
            bs_sdpu::DOWN_PYRANOMETER,
            bs_sdpu::THERM_2,
            bs_sdpu::DIFF_BAROM_2,
            bs_sdpu::PHOTODIODE_2
    };
    CompareIndividualsWithCompletesFrom(lines, bs_sdpu::MUXChannel::CH1);
}

TEST_F(SDPUTests, CompareIndividialReadingsWithCompleteReadingsFromChannel2) {
    std::vector<bs_sdpu::AnalogDevice> lines {
            bs_sdpu::UP_PYRGEOMETER,
            bs_sdpu::THERM_3,
            bs_sdpu::DIFF_BAROM_3,
            bs_sdpu::PHOTODIODE_3
    };
    CompareIndividualsWithCompletesFrom(lines, bs_sdpu::MUXChannel::CH2);
}

TEST_F(SDPUTests, CompareIndividialReadingsWithCompleteReadingsFromChannel3) {
    std::vector<bs_sdpu::AnalogDevice> lines {
            bs_sdpu::DOWN_PYRGEOMETER,
            bs_sdpu::THERM_4,
            bs_sdpu::DIFF_BAROM_4,
            bs_sdpu::PHOTODIODE_4
    };
    CompareIndividualsWithCompletesFrom(lines, bs_sdpu::MUXChannel::CH3);
}
