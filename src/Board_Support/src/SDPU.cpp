/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              BOARD  SUPPORT                                --
--                                                                            --
--                               SDPU Source                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "SDPU.h"
#include "SDPU_Conf.h"

#include "MUX.h"
#include "ADS1115_ADC.h"
#include "BusHandlers.h"

#include <iostream>
#include <cstdio>  // perror
#include <cstdlib> // atexit

namespace bs = equipment_handlers;
namespace bh = bus_handlers;

namespace {

    using namespace board_support::sdpu;

    // --------------------
    // Private data members
    // --------------------

    bool initialized = false;

    bs::ADS1115_ADC adc;

    bs::MUX mux;

    std::array<conf::AnalogDeviceConf, conf::nAnalogDevices> analogDevices;

    std::array<bs::MS5611_01BA03_Barometer, conf::nOfBarometers> barometers;

    // -------------------
    // Auxiliary functions
    // -------------------

    int16_t readOneChannel(int muxChannel, bs::ADS1115_ADC::Channel adcChannel, uint8_t n_retries) {
        mux.selectInput(muxChannel);
        return adc.readOneChannel(adcChannel, n_retries);
    }

    void setUpAnalogDevices() {
        analogDevices[AnalogDevice::UP_PYRGEOMETER]   = conf::UP_PYRGEOMETER;
        analogDevices[AnalogDevice::UP_PYRANOMETER]   = conf::UP_PYRANOMETER;
        analogDevices[AnalogDevice::DOWN_PYRANOMETER] = conf::DOWN_PYRANOMETER;
        analogDevices[AnalogDevice::DOWN_PYRGEOMETER] = conf::DOWN_PYRGEOMETER;

        analogDevices[AnalogDevice::THERM_1]  = conf::THERM_1;
        analogDevices[AnalogDevice::THERM_2]  = conf::THERM_2;
        analogDevices[AnalogDevice::THERM_3]  = conf::THERM_3;
        analogDevices[AnalogDevice::THERM_4]  = conf::THERM_4;
        analogDevices[AnalogDevice::PT1000_1] = conf::PT1000_1;
        analogDevices[AnalogDevice::PT1000_2] = conf::PT1000_2;

        analogDevices[AnalogDevice::DIFF_BAROM_1] = conf::DIFF_BAROM_1;
        analogDevices[AnalogDevice::DIFF_BAROM_2] = conf::DIFF_BAROM_2;
        analogDevices[AnalogDevice::DIFF_BAROM_3] = conf::DIFF_BAROM_3;
        analogDevices[AnalogDevice::DIFF_BAROM_4] = conf::DIFF_BAROM_4;

        analogDevices[AnalogDevice::PHOTODIODE_1] = conf::PHOTODIODE_1;
        analogDevices[AnalogDevice::PHOTODIODE_2] = conf::PHOTODIODE_2;
        analogDevices[AnalogDevice::PHOTODIODE_3] = conf::PHOTODIODE_3;
        analogDevices[AnalogDevice::PHOTODIODE_4] = conf::PHOTODIODE_4;
    }

    float transferFunction(float m, float b, int16_t raw) {
        float volts = static_cast<float>(raw) * 125.0F;
        return m * volts + b;
    }
}

namespace board_support::sdpu {

    bool initialize() {
        static bool atexit_success {atexit(finalize) == 0};
        if (!atexit_success) {
            perror("[SDPU] Could not register Finalize atexit");
            atexit_success = atexit(finalize) == 0;
        }

        if (!bh::initialize()) {
            std::cerr << "[BH] Could not initialize bus handlers!!!" << std::endl;
            return false;
        }

        setUpAnalogDevices();

        if (!initialized) {
            bool muxInitialized = mux.initialize(conf::muxPin0,
                                                 conf::muxPin1,
                                                 conf::muxPin2);

            bool adcInitialized = adc.initialize(conf::adc_i2cBusId,
                                                 conf::adc_i2cAddress,
                                                 conf::adcMode);

            bool barom1Initialized = barometers[PS1].initialize(conf::ps1_i2cBusId,
                                                                conf::ps1_i2cAddress,
                                                                conf::ps1_OSR,
                                                                conf::ps1_compensationEnabled);
            bool barom2Initialized = barometers[PS2].initialize(conf::ps2_i2cBusId,
                                                                conf::ps2_i2cAddress,
                                                                conf::ps2_OSR,
                                                                conf::ps2_compensationEnabled);

            initialized = adcInitialized
                       && muxInitialized
                       && barom1Initialized
                       && barom2Initialized;
        }

        return initialized;
    }

    void finalize() {
        mux.finalize();
        initialized = false;
    }

    int16_t readRawFrom(AnalogDevice analogDevice, uint8_t n_retries) {
        auto [muxChannel, adcChannel] = analogDevices.at(analogDevice);
        return readOneChannel(muxChannel, adcChannel, n_retries);
    }

    std::array<int16_t, 4U> readAllADCChannels(MUXChannel muxChannel, uint8_t n_retries) {
        mux.selectInput(muxChannel);
        return adc.readAllChannels(n_retries);
    }

    bool getPressureFrom(PressureSensor ps, BarometerReading &reading) {
        bs::MS5611_01BA03_Barometer::Reading result {};
        bool success = barometers.at(ps).get(result);

        reading.temperature_celsius = result.temperature_celsius;
        reading.pressure_milliBar   = result.pressure_milliBar;
        reading.temperature_raw     = result.temperature_raw;
        reading.pressure_raw        = result.pressure_raw;

        return success;
    }

    bool resetPressureSensor(PressureSensor ps) {
        if (barometers.at(ps).reset()) {
            return barometers[ps].initialize(conf::ps1_i2cBusId,
                                             conf::ps1_i2cAddress,
                                             conf::ps1_OSR,
                                             conf::ps1_compensationEnabled);
        }
        return false;
    }

    bool resetADC() {
        if (adc.reset()) {
            return adc.initialize(conf::adc_i2cBusId,
                                  conf::adc_i2cAddress,
                                  conf::adcMode);
        }
        return false;
    }

    float pt1000ToCelsius(int16_t raw) {
        return transferFunction(34.18F, -80.24F, raw);
    }

    float thermistorToCelsius(int16_t raw) {
        return transferFunction(13.85F, 28.42F, raw);
    }

    float pyranometersTomV(int16_t raw) {
        return transferFunction(4.84F, -4.84F, raw);
    }

    float pyrgeometersTomV(int16_t raw) {
        return transferFunction(3.66F, -7.5F, raw);
    }

    float diffBarometersToPa(int16_t raw) {
        return transferFunction(24.41F, -50.0F, raw);
    }

}
