/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              BOARD  SUPPORT                                --
--                                                                            --
--                                PCU Source                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "PCU.h"
#include "PCU_Conf.h"
#include "Switch.h"
#include "TC74.h"
#include "PWM_GPIO.h"
#include "INA226.h"
#include "BusHandlers.h"

#include <array>
#include <iostream>

namespace {
    using namespace equipment_handlers;
    using namespace board_support::pcu;

    TC74 tc74;

    INA226 ina226;

    std::array <PWM_GPIO, conf::NUMBER_OF_PWM_HEATERS> pwmHeaters;

    std::array <PWM_GPIO, conf::NUMBER_OF_ENV_LAB_HEATERS> envLabHeaters;

    std::array <Switch, conf::NUMBER_OF_POWER_SUPPLY_LINES> powerSupplyLines;

    // -----------------
    // Private functions
    // -----------------

    bool setupPwmHeaters() {
        bool pwm1Success = pwmHeaters.at(HTLPWMHeater::Heater1).initialize(conf::HEATER_1_PIN);
        bool pwm2Success = pwmHeaters.at(HTLPWMHeater::Heater2).initialize(conf::HEATER_2_PIN);
        bool pwm3Success = pwmHeaters.at(HTLPWMHeater::Heater3).initialize(conf::HEATER_3_PIN);
        bool pwm4Success = pwmHeaters.at(HTLPWMHeater::Heater4).initialize(conf::HEATER_4_PIN);

        bool uelSuccess = envLabHeaters
                            .at(EnvLabPWMHeaters::UpwardsELHeater)
                            .initialize(conf::UP_ENV_LAB_HEATER_PIN);
        bool delSuccess = envLabHeaters
                            .at(EnvLabPWMHeaters::DownwardsELHeater)
                            .initialize(conf::DOWN_ENV_LAB_HEATER_PIN);

        bool pwmConfigSuccess = pwm1Success && pwm2Success && pwm3Success && pwm4Success &&
                                uelSuccess && delSuccess;

        for (auto & htlPwm : pwmHeaters) {
            if (!htlPwm.setPWMFrequency(conf::pwmFrequency)) {
                pwmConfigSuccess = false;
            }
            if (!htlPwm.setDutyCycleRange(conf::pwmDutyCycleRange)) {
                pwmConfigSuccess = false;
            }
            if (!htlPwm.setDutyCycle(0.0F)) {
                pwmConfigSuccess = false;
            }
        }

        for (auto & elPwm : envLabHeaters) {
            if (!elPwm.setPWMFrequency(conf::pwmFrequency)) {
                pwmConfigSuccess = false;
            }
            if (!elPwm.setDutyCycleRange(conf::pwmDutyCycleRange)) {
                pwmConfigSuccess = false;
            }
            if (!elPwm.setDutyCycle(0.0F)) {
                pwmConfigSuccess = false;
            }
        }

        return pwmConfigSuccess;
    }

    bool initializeINA226() {
        bool inaSuccess  = ina226.initialize(conf::inaI2CBusId, conf::inaI2CAddress);
        if (inaSuccess) {
            ina226.setMode(conf::inaMode);
            ina226.setAverage(conf::average_samples);
            ina226.setBusVoltageConversionTime(conf::conversionTime);
            ina226.setShuntVoltageConversionTime(conf::conversionTime);
            ina226.setMaxCurrentShunt(conf::maxCurrent_A, conf::shunt_OHMs);
        }
        return inaSuccess;
    }
}

namespace board_support::pcu {

    bool initialize_switches() {
        static bool switches_initialized = false;

        if (!switches_initialized) {
            bool power1Success = powerSupplyLines.at(AL_LINE).initialize(conf::AL_GPIO_PIN);
            bool power2Success = powerSupplyLines.at(TMU_LINE).initialize(conf::TMU_GPIO_PIN);
            bool power3Success = powerSupplyLines.at(SDPU_LINE).initialize(conf::SDPU_GPIO_PIN);

            bool pwmSuccess = setupPwmHeaters();

            switches_initialized = power1Success && power2Success && power3Success &&
                                   pwmSuccess;
        }

        return switches_initialized;
    }

    bool initialize_sensors() {
        if (!bus_handlers::initialize()) {
            return false;
        }

        static bool sensors_initialized = false;

        if (!sensors_initialized) {
            bool tc74Success = tc74.initialize(conf::tc74I2CBusId,
                                               conf::tc74I2CAddress,
                                               conf::tc74Mode);
            bool inaSuccess  = initializeINA226();

            sensors_initialized = tc74Success && inaSuccess;
        }

        return sensors_initialized;
    }

    bool activatePowerSupplyFor(PowerSupplyLines line) {
        return powerSupplyLines.at(line).switchOn();
    }

    bool deactivatePowerSupplyFor(PowerSupplyLines line) {
        return powerSupplyLines.at(line).switchOff();
    }

    bool setEnvLabPWMDutyCycle(EnvLabPWMHeaters heater, float pwmPercentage) {
        return envLabHeaters.at(heater).setDutyCycle(pwmPercentage);
    }

    void getEnvLabPWMDutyCycle(float &pwm_uel, float &pwm_del) {
        pwm_uel = envLabHeaters.at(EnvLabPWMHeaters::UpwardsELHeater).dutyCycle();
        pwm_del = envLabHeaters.at(EnvLabPWMHeaters::DownwardsELHeater).dutyCycle();
    }

    bool setHTLPWMDutyCycle(HTLPWMHeater heater, float pwmPercentage) {
        return pwmHeaters.at(heater).setDutyCycle(pwmPercentage);
    }

    void getHTLPWMDutyCycle(float &pwm_htl1, float &pwm_htl2,
                            float &pwm_htl3, float &pwm_htl4)
    {
        pwm_htl1 = pwmHeaters.at(HTLPWMHeater::Heater1).dutyCycle();
        pwm_htl2 = pwmHeaters.at(HTLPWMHeater::Heater2).dutyCycle();
        pwm_htl3 = pwmHeaters.at(HTLPWMHeater::Heater3).dutyCycle();
        pwm_htl4 = pwmHeaters.at(HTLPWMHeater::Heater4).dutyCycle();
    }

    bool activateTC74() {
        return tc74.setMode(TC74::Mode::Normal);
    }

    bool readTC74(int8_t &temperature) {
        return tc74.readTemperature(temperature);
    }

    bool deactivateTC74() {
        return tc74.setMode(TC74::Mode::Standby);
    }

    /**
     * Note that requestData() clears the "conversion ready" register.
     * Then, if we invoke requestData() before conversionReady(),
     * and the elapsed time between these calls is lower than the conversion time,
     * we will definitely get "false".
     */
    bool readPowerAndCurrent(PowerAndCurrentData & data) {
        bool isReady = ina226.conversionsReady();        

        if (isReady) {
            data.shuntVoltage_V = ina226.getShunt();
            data.busVoltage_V   = ina226.getBusVoltage();
            data.current_A      = ina226.getCurrent();
            data.power_W        = ina226.getPower();

            ina226.requestData();
        }

        return isReady;
    }

    bool resetPowerAndCurrentSensor() {
        ina226.reset();
        bool inaSuccess = initializeINA226();
        return inaSuccess;
    }

} // board_support::pcu