/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              BOARD  SUPPORT                                --
--                                                                            --
--                                PCU Config                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef HAL_PCU_CONF_H
#define HAL_PCU_CONF_H

#include "BusHandlers_Data.h"
#include "TC74.h"
#include "INA226.h"
#include <cstdint>

namespace board_support::pcu::conf {

    using bus_handlers::data::I2CBusID;
    using equipment_handlers::INA226;
    using equipment_handlers::TC74;

    // --------------------------------------------
    // Config for switches that power up subsystems
    // --------------------------------------------
    constexpr uint8_t NUMBER_OF_POWER_SUPPLY_LINES {3U};

    constexpr unsigned int AL_GPIO_PIN   {16U};
    constexpr unsigned int TMU_GPIO_PIN  {20U};
    constexpr unsigned int SDPU_GPIO_PIN {26U};

    // ------------------------------------
    // Config for Env Lab Heaters' switches
    // ------------------------------------
    constexpr uint8_t NUMBER_OF_ENV_LAB_HEATERS {2U};

    constexpr unsigned int UP_ENV_LAB_HEATER_PIN   {21U};
    constexpr unsigned int DOWN_ENV_LAB_HEATER_PIN {23U};

    // ----------------------
    // Config for PWM Heaters
    // ----------------------
    constexpr uint8_t NUMBER_OF_PWM_HEATERS (4U);

    constexpr unsigned int HEATER_1_PIN {9U};
    constexpr unsigned int HEATER_2_PIN {11U};
    constexpr unsigned int HEATER_3_PIN {6U};
    constexpr unsigned int HEATER_4_PIN {13U};

    constexpr unsigned int pwmFrequency      {1000U};
    constexpr unsigned int pwmDutyCycleRange {200U};

    // ---------------------------------
    // Config for Power & Current sensor
    // ---------------------------------
    constexpr uint8_t  inaI2CAddress  {0x40U};
    constexpr I2CBusID inaI2CBusId    {I2CBusID::BUS4};

    constexpr auto inaMode         {INA226::OperatingMode::ShuntBusTrigger};
    constexpr auto average_samples {INA226::AVERAGE_512};
    constexpr auto conversionTime  {INA226::CONV_TIME_4156};

    constexpr float maxCurrent_A {2.048F};
    constexpr float shunt_OHMs   {0.0399F};

    // ---------------------
    // Config for Thermistor
    // ---------------------
    constexpr uint8_t tc74I2CAddress {0x49U};
    constexpr auto    tc74I2CBusId   {I2CBusID::BUS4};
    constexpr auto    tc74Mode       {TC74::Standby};

}

#endif //HAL_PCU_CONF_H
