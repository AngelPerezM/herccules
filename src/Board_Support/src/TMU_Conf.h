/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              BOARD  SUPPORT                                --
--                                                                            --
--                                TMU Config                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef HAL_TMU_CONF_H
#define HAL_TMU_CONF_H

#include "BusHandlers_Data.h"
#include "ADS1115_ADC.h"

/// @brief Configuration parameters for the Thermal Measurement Unit (TMU)
namespace board_support::tmu::conf {

    using equipment_handlers::ADS1115_ADC;

    constexpr auto    i2cBusId   {bus_handlers::data::I2CBusID::BUS0};
    constexpr uint8_t i2cAddress {ADS1115_ADC::primaryI2CAddress};
    constexpr ADS1115_ADC::Mode adcMode {
        .pga      = ADS1115_ADC::PGAMode::FSR_4V096,
        .opMode   = ADS1115_ADC::OperatingMode::Single_Shot_Or_Power_Down,
        .dataRate = ADS1115_ADC::DataRate::_64SPS,
        .compMode = ADS1115_ADC::ComparatorMode::Traditional,
        .polarity = ADS1115_ADC::ComparatorPolarity::ActiveLow,
        .latch    = ADS1115_ADC::LatchingComparator::NonLatching,
        .queue    = ADS1115_ADC::ComparatorQueueConfig::Disabled
    };

    constexpr unsigned int muxPin0 {27U};
    constexpr unsigned int muxPin1 {22U};
    constexpr unsigned int muxPin2 {10U};
}

#endif //HAL_TMU_CONF_H
