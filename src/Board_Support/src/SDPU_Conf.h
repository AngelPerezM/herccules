/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              BOARD  SUPPORT                                --
--                                                                            --
--                               SDPU Config                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef HAL_SDPU_CONF_H
#define HAL_SDPU_CONF_H

#include "BusHandlers_Data.h"

#include "ADS1115_ADC.h"
#include "MS5611_01BA03_Barometer.h"

#include <utility>      // std::pair

/// @brief Configuration parameters for the Sensor Data Processing Unit (SDPU)
namespace board_support::sdpu::conf {

    using bus_handlers::data::I2CBusID;
    using equipment_handlers::ADS1115_ADC;
    using equipment_handlers::MS5611_01BA03_Barometer;

    /****************************
     * Configuration of the ADC *
     ****************************/
    constexpr auto    adc_i2cBusId   {I2CBusID::BUS3};
    constexpr uint8_t adc_i2cAddress {0x48U};
    constexpr ADS1115_ADC::Mode adcMode {
        .pga      = ADS1115_ADC::PGAMode::FSR_4V096,
        .opMode   = ADS1115_ADC::OperatingMode::Single_Shot_Or_Power_Down,
        .dataRate = ADS1115_ADC::DataRate::_32SPS,
        .compMode = ADS1115_ADC::ComparatorMode::Traditional,
        .polarity = ADS1115_ADC::ComparatorPolarity::ActiveLow,
        .latch    = ADS1115_ADC::LatchingComparator::NonLatching,
        .queue    = ADS1115_ADC::ComparatorQueueConfig::Disabled
    };

    /****************************
     * Configuration of the MUX *
     ****************************/
    constexpr unsigned int muxPin0 {24U};
    constexpr unsigned int muxPin1 {25U};
    constexpr unsigned int muxPin2 { 8U};

    /***************************************
     * Configuration of the analogue lines *
     ***************************************/

    constexpr int nAnalogDevices = 18;
    struct AnalogDeviceConf {
        int                  muxChannel;
        ADS1115_ADC::Channel adcChannel;
    };

    constexpr AnalogDeviceConf DOWN_PYRANOMETER {0, ADS1115_ADC::Channel::Channel0};
    constexpr AnalogDeviceConf UP_PYRANOMETER   {1, ADS1115_ADC::Channel::Channel0};
    constexpr AnalogDeviceConf DOWN_PYRGEOMETER {2, ADS1115_ADC::Channel::Channel0};
    constexpr AnalogDeviceConf UP_PYRGEOMETER   {3, ADS1115_ADC::Channel::Channel0};

    constexpr AnalogDeviceConf THERM_1  {0, ADS1115_ADC::Channel::Channel1};
    constexpr AnalogDeviceConf THERM_2  {1, ADS1115_ADC::Channel::Channel1};
    constexpr AnalogDeviceConf THERM_3  {2, ADS1115_ADC::Channel::Channel1};
    constexpr AnalogDeviceConf THERM_4  {3, ADS1115_ADC::Channel::Channel1};
    constexpr AnalogDeviceConf PT1000_1 {4, ADS1115_ADC::Channel::Channel1};
    constexpr AnalogDeviceConf PT1000_2 {5, ADS1115_ADC::Channel::Channel1};

    constexpr AnalogDeviceConf DIFF_BAROM_1 {0, ADS1115_ADC::Channel::Channel2};
    constexpr AnalogDeviceConf DIFF_BAROM_2 {1, ADS1115_ADC::Channel::Channel2};
    constexpr AnalogDeviceConf DIFF_BAROM_3 {2, ADS1115_ADC::Channel::Channel2};
    constexpr AnalogDeviceConf DIFF_BAROM_4 {3, ADS1115_ADC::Channel::Channel2};

    constexpr AnalogDeviceConf PHOTODIODE_1 {0, ADS1115_ADC::Channel::Channel3};
    constexpr AnalogDeviceConf PHOTODIODE_2 {1, ADS1115_ADC::Channel::Channel3};
    constexpr AnalogDeviceConf PHOTODIODE_3 {2, ADS1115_ADC::Channel::Channel3};
    constexpr AnalogDeviceConf PHOTODIODE_4 {3, ADS1115_ADC::Channel::Channel3};

    /********************************************
     * Configuration of the Absolute Barometers *
     ********************************************/

    constexpr size_t nOfBarometers = 2U;

    constexpr auto    ps1_i2cBusId   {I2CBusID::BUS3};
    constexpr uint8_t ps1_i2cAddress {MS5611_01BA03_Barometer::primaryI2CAddress};
    constexpr auto    ps1_OSR        {MS5611_01BA03_Barometer::OSR::OSR_256};
    constexpr bool    ps1_compensationEnabled {true};

    constexpr auto    ps2_i2cBusId   {I2CBusID::BUS3};
    constexpr uint8_t ps2_i2cAddress {MS5611_01BA03_Barometer::secondaryI2CAddress};
    constexpr auto    ps2_OSR        {MS5611_01BA03_Barometer::OSR::OSR_256};
    constexpr bool    ps2_compensationEnabled {true};

}

#endif //HAL_SDPU_CONF_H
