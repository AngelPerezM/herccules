#################################################################################
#                              H E R C C U L E S                                #
#            Copyright (C) 2023 Universidad Politécnica de Madrid               #
#                                                                               #
# The HERCCULES OBSW has been developed by the real time systems group from UPM #
#################################################################################

TEMPLATE = lib
CONFIG -= qt
CONFIG += generateC

# Load DataTypes definitions:
DISTFILES += \
    $(OBSW_DATA_TYPES)/DataTypes.acn \
    $(OBSW_DATA_TYPES)/DataTypes.asn \
    $(OBSW_DATA_TYPES)/DataTypes-ADSignals.acn \
    $(OBSW_DATA_TYPES)/DataTypes-ADSignals.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Events.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Events.asn \
    $(OBSW_DATA_TYPES)/DataTypes-OperatingModes.acn \
    $(OBSW_DATA_TYPES)/DataTypes-OperatingModes.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Subsystems.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Subsystems.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Telecommands.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Telecommands.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Telemetries.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Telemetries.asn


# Load Data Types from other componentes:
DISTFILES += NADS.acn \
             NADS.asn \
             Main.asn \
             Main.acn

# Load Deployment View models:
DISTFILES += rpi.dv.xml \
             rpi_home.dv.xml \
             rpi_home_mono.dv.xml \
             xpair_no_gs.dv.xml

DISTFILES += Main.msc
DISTFILES += interfaceview.xml
DISTFILES += work/binaries/*.msc
DISTFILES += work/binaries/coverage/index.html
DISTFILES += work/binaries/filters
DISTFILES += work/system.asn

# Load Paths from HAL:
INCLUDEPATH += \
    $(HAL_INC)/Bus_Handlers \
    $(HAL_INC)/Equipment_Handlers \
    $(HAL_INC)/Board_Support \
    $(HAL_INC)/Time_Management \
    $(HAL_INC)/Data_Storage

# Load Paths from Data View:
INCLUDEPATH += work/dataview/C

include(work/taste.pro)
message($$DISTFILES)
DISTFILES += work/doc/database/main.png
