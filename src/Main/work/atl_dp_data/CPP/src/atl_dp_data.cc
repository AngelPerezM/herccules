// C+++ body file for function ATL_DP_Data
#include "atl_dp_data.h"
#include "atl_dp_data_state.h"

// Define and use function state inside this context structure
// avoid defining global/static variable elsewhere
namespace {
    atl_dp_data_state ctxt_atl_dp_data;
}

void atl_dp_data_startup(void)
{
    asn1SccAtt_Lab_Data_Initialize(&ctxt_atl_dp_data.data);
}

void atl_dp_data_PI_Get
      (asn1SccAtt_Lab_Data *OUT_atl_data)
{
   *OUT_atl_data = ctxt_atl_dp_data.data;
}


void atl_dp_data_PI_Put
      (const asn1SccAtt_Lab_Data *IN_atl_data)
{
    ctxt_atl_dp_data.data = *IN_atl_data;
}


