// C+++ body file for function EL_DP_Data

#include "el_dp_data.h"
#include "el_dp_data_state.h"

// Define and use function state inside this context structure
// avoid defining global/static variable elsewhere
el_dp_data_state ctxt_el_dp_data;


void el_dp_data_startup(void)
{
   asn1SccEnvLab_Data_Initialize(&ctxt_el_dp_data.data);
}

void el_dp_data_PI_Get
      (asn1SccEnvLab_Data *OUT_el_data)
{
    *OUT_el_data = ctxt_el_dp_data.data;
}


void el_dp_data_PI_Put
      (const asn1SccEnvLab_Data *IN_el_data)
{
    ctxt_el_dp_data.data = *IN_el_data;
}


