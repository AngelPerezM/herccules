// C+++ body file for function NADS_DP_Data

#include "nads_dp_data.h"
#include "nads_dp_data_state.h"

namespace {
    nads_dp_data_state ctxt_nads_dp_data;
}

void nads_dp_data_startup(void)
{
    asn1SccNADS_Data_Initialize(&ctxt_nads_dp_data.data);
}

void nads_dp_data_PI_Get
      (asn1SccNADS_Data *OUT_nads_data)
{
   *OUT_nads_data = ctxt_nads_dp_data.data;
}


void nads_dp_data_PI_Put
      (const asn1SccNADS_Data *IN_nads_data)
{
   ctxt_nads_dp_data.data = *IN_nads_data;
}


