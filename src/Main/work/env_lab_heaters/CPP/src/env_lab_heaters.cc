/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                           Env_Lab_Heaters Source                           --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "env_lab_heaters.h"
#include "env_lab_heaters_state.h"

#include "PCU.h" // Hardware I/F for PCU

#include "debug_messages.h"

/*******************************************************************************
 * Private variables and constants:
 ******************************************************************************/

namespace {
    env_lab_heaters_state ctxt;
}

/*******************************************************************************
 * Private functions:
 ******************************************************************************/

namespace {
    void control_heater(const asn1SccEnvLab_Heater_ID  IN_heater_id,
                        const asn1SccSwitch_Status     IN_on_off)
    {
        auto heater_ID = IN_heater_id == EnvLab_Heater_ID_upwards_heater
                       ? board_support::pcu::UpwardsELHeater
                       : board_support::pcu::DownwardsELHeater;
        auto pwm {IN_on_off == asn1SccSwitch_Status_on ? 100.0F : 0.0F};
        auto success {board_support::pcu::setEnvLabPWMDutyCycle(heater_ID, pwm)};

        if (success) {
            debug_printf(LVL_INFO, "[EL_Heaters] set PWM successfully\n");
        } else {
            debug_printf(LVL_ERROR, "[EL_Heaters] Could not set PWM\n");
        }
    }
}

/*******************************************************************************
 * Public functions:
 ******************************************************************************/

void env_lab_heaters_startup(void)
{
    board_support::pcu::initialize_switches();
}

void env_lab_heaters_PI_Control_Heater_Auto
    (const asn1SccEnvLab_Heater_ID *IN_heater_id,
     const asn1SccSwitch_Status    *IN_on_off)
{
    if (ctxt.heaters_mode[*IN_heater_id] == asn1SccActuator_Control_Mode_autonomous_control) {
        control_heater(*IN_heater_id, *IN_on_off);
    }
}

void env_lab_heaters_PI_Control_Heater_Manual
    (const asn1SccEnvLab_Heater_ID  *IN_heater_id,
     const asn1SccSwitch_Status     *IN_on_off)
{
    if (ctxt.heaters_mode[*IN_heater_id] == asn1SccActuator_Control_Mode_manual_control) {
        control_heater(*IN_heater_id, *IN_on_off);
    }
}

void env_lab_heaters_PI_Get_Heaters_Status
    (asn1SccEnv_Lab_Heaters_Mode *OUT_heaters_mode,
     asn1SccSwitch_Status *OUT_heater_uel,
     asn1SccSwitch_Status *OUT_heater_del)
{
    float uel, del;
    board_support::pcu::getEnvLabPWMDutyCycle(uel, del);
    *OUT_heater_uel = (uel > 0.0F ? asn1SccSwitch_Status_on : asn1SccSwitch_Status_off);
    *OUT_heater_del = (del > 0.0F ? asn1SccSwitch_Status_on : asn1SccSwitch_Status_off);

    OUT_heaters_mode->upwards = ctxt.heaters_mode[asn1SccEnvLab_Heater_ID_upwards_heater];
    OUT_heaters_mode->downwards = ctxt.heaters_mode[asn1SccEnvLab_Heater_ID_downwards_heater];
}

void env_lab_heaters_PI_Set_Heaters_Mode
    (const asn1SccEnv_Lab_Heaters_Mode *IN_new_mode)
{
    ctxt.heaters_mode[asn1SccEnvLab_Heater_ID_upwards_heater] = IN_new_mode->upwards;
    ctxt.heaters_mode[asn1SccEnvLab_Heater_ID_downwards_heater] = IN_new_mode->downwards;
}

void env_lab_heaters_PI_Set_Single_EL_Heater_Mode
    (const asn1SccEnvLab_Heater_ID     * IN_heater_id,
     const asn1SccActuator_Control_Mode * IN_new_mode)
{
    ctxt.heaters_mode[*IN_heater_id] = *IN_new_mode;
}
