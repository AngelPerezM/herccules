// C+++ body file for function HTL_DP_Data

#include "htl_dp_data.h"
#include "htl_dp_data_state.h"

// Define and use function state inside this context structure
// avoid defining global/static variable elsewhere
namespace {
    htl_dp_data_state ctxt_htl_dp_data;
}

void htl_dp_data_startup(void) {
    asn1SccHTL_Data_Initialize(&ctxt_htl_dp_data.data);
}

void htl_dp_data_PI_Get
      (asn1SccHTL_Data *OUT_htl_data)
{
   *OUT_htl_data = ctxt_htl_dp_data.data;
}


void htl_dp_data_PI_Put
      (const asn1SccHTL_Data *IN_htl_data)
{
   ctxt_htl_dp_data.data = *IN_htl_data;
}


