/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                            Attitude_Lab  State                             --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "dataview-uniq.h"
#include "Context-attitude-lab.h"
#include "CsvWriter.h"

class attitude_lab_state {
public:
    /***************************************************************************
     * Context operations:
     **************************************************************************/

    static const char * image(const asn1SccAtt_Lab_Mode& x) {
        switch (x) {
            case asn1SccAtt_Lab_Mode_on:
                return "Mode-ON";
            case asn1SccAtt_Lab_Mode_off:
                return "Mode-OFF";
            default:
                return "Mode-Unknown";
        }
    }

    /***************************************************************************
     * Context data:
     **************************************************************************/

    /**
     * Flag set to true when the system shall be turned off.
     */
    bool end_of_mission {false};

    /**
     * @brief Acquired data from the ATL equipment.
     * It includes analogue sensors from thermistors and photodiodes.
     */
    asn1SccAtt_Lab_Data data;

    /**
     * Objects and types for the data logging
     * @{
     */
    using Logfile_type = data_storage::CsvWriter<10U>;
    const Logfile_type::Line header {
        // == Timestamps & modes (4 cols) ==
        "Snapshot time [secs]",
        "Snapshot time [msecs]",
        "Mission time [secs]",
        "ATL Mode",

        // == Photodiodes (4 cols) ==
        "Photo-1",
        "Photo-2",
        "Photo-3",
        "Photo-4",

        // == Thermistors (2 cols) ==
        "Thermistor-1",
        "Thermistor-2"
    };

    Logfile_type logfile {attitude_lab_ctxt.logfile_name, ';', header};
    /** @} */
};
