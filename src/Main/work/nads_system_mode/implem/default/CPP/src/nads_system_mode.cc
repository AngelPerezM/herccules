/* C++ Body file for function type NADS_System_Mode
 * Generated by TASTE/Kazoo
 * DO NOT EDIT THIS FILE, IT WILL BE OVERWRITTEN
 * Timers              : 
 */

#include "nads_system_mode.h"
#include "system_mode.h"


// Declaration of the function instance
system_mode NADS_System_Mode;

extern "C" void nads_system_mode_startup()
{
   NADS_System_Mode.startup();
}

extern "C" void nads_system_mode_PI_Get_Mode (asn1SccBalloon_Mode *current_mode)
{
   NADS_System_Mode.Get_Mode(current_mode);
}


extern "C" void nads_system_mode_PI_Set_Mode (const asn1SccBalloon_Mode *new_mode)
{
   NADS_System_Mode.Set_Mode(new_mode);
}
