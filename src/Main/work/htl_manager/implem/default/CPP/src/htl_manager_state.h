/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                             HTL_Manager State                              --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "dataview-uniq.h"

#include "CsvWriter.h"

/**
 * @brief This class contains the context data (internal state) of the HTL_Manager.
 */
class htl_manager_state {
public:

    int float_mode_start_time_secs {0};

    /***************************************************************************
     * Context operations:
     **************************************************************************/

    const char* image(const asn1SccHTL_Mode_mode &htl_mode) {
        switch (htl_mode) {
        case asn1SccHTL_Mode_mode_off:
            return "Mode-OFF";
        case asn1SccHTL_Mode_mode_pre_launch:
            return "Pre Launch";
        case asn1SccHTL_Mode_mode_ascent_mode1:
            return "Ascent-1";
        case asn1SccHTL_Mode_mode_ascent_mode2:
            return "Ascent-2";
        case asn1SccHTL_Mode_mode_float_mode1:
            return "Float-1";
        case asn1SccHTL_Mode_mode_float_mode2:
            return "Float-2";
        }
        return "HTL Mode ERROR";
    }

    const char* image(const asn1SccActuator_Control_Mode &heater_mode) {
        switch (heater_mode) {
        case asn1SccActuator_Control_Mode_autonomous_control:
            return "AUTO";
        case asn1SccActuator_Control_Mode_manual_control:
            return "MANUAL";
        }
        return "Heaters Mode ERROR";
    }

    /***************************************************************************
     * Context data:
     **************************************************************************/

    asn1SccBalloon_Mode balloon_mode {asn1SccBalloon_Mode_ground_await};

    bool finalize {false};

    /**
     * @brief Acquired data from the HTL equipment.
     * It includes analogue thermistors data and heaters status.
     */
    asn1SccHTL_Data data;

    /**
     * Objects and types for the data logging
     * @{
     */
    using Logfile_type = data_storage::CsvWriter<40U>;
    const Logfile_type::Line header {
        // == Timestamps & modes (8 cols) ==
        "Snapshot time [secs]",
        "Snapshot time [msecs]",
        "Mission time [secs]",
        "HTL Mode",
        "Heater 1 Mode",
        "Heater 2 Mode",
        "Heater 3 Mode",
        "Heater 4 Mode",

        // == HTL sensor Data (28 cols) ==
        "THERM 1 [Raw]",
        "THERM 2 [Raw]",
        "THERM 3 [Raw]",
        "THERM 4 [Raw]",
        "THERM 5 [Raw]",
        "THERM 6 [Raw]",
        "THERM 7 [Raw]",
        "THERM 8 [Raw]",
        "THERM 9 [Raw]",
        "THERM 10 [Raw]",
        "THERM 11 [Raw]",
        "THERM 12 [Raw]",
        "THERM 13 [Raw]",
        "THERM 14 [Raw]",
        "THERM 15 [Raw]",
        "THERM 16 [Raw]",
        "THERM 17 [Raw]",
        "THERM 18 [Raw]",
        "THERM 19 [Raw]",
        "THERM 20 [Raw]",
        "THERM 21 [Raw]",
        "THERM 22 [Raw]",
        "THERM 23 [Raw]",
        "THERM 24 [Raw]",
        "THERM 25 [Raw]",
        "THERM 26 [Raw]",
        "THERM 27 [Raw]",
        "THERM 28 [Raw]",

        // == HTL heaters Data (4 cols) ==
        "Heater 1 [Watts]",
        "Heater 2 [Watts]",
        "Heater 3 [Watts]",
        "Heater 4 [Watts]"
    };
    Logfile_type logfile {"htl_log.csv", ';', header};
    /** @} */
};
