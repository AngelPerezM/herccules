/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                              COMMUNICATION_IF                              --
--                                                                            --
--                             TC_Receiver Source                             --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/
#include "communication_if_tc_receiver.h"

#include "ft-tcp-svr-cwrapper.h"

// Tasking headers:
#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1 // See feature_test_macros(7)
#endif
#include <sched.h>    // CPU_ZERO, CPU_SET, affinity
#include <pthread.h>  // pthread_*
#include <stdio.h>    // perror
#include <sys/time.h> // timespec
#include <math.h>     // modff

// TASTE headers:

#include "dataview-uniq.h"
#include "debug_messages.h"
#include "PrintTypesAsASN1.h"
#include "Context-communication-if.h"

#include "partition_obsw_polyorb_interface.h"

/*******************************************************************************
 * Local constants: Connection configuration, etc.
 ******************************************************************************/

#define TC_PORT (communication_if_ctxt.my_tc_port)

#define TC_TASK_PRIO (1)
#define TC_TASK_CORE (2)
#define TC_TASK_MIAT (1.0F) // SECONDS

#define MAX_DISCONNECTION_COUNTS (6)

#define NAME "[COMM_IF - TC Receiver] "


/*******************************************************************************
 * Required interfaces
 ******************************************************************************/

/** ASN.1 Type and encoding of the parameters:
 *  IN_tmtc_mode type: TMTC-Mode ; encoding: NATIVE
 */
extern void vm_communication_if_set_internal_mode
    (asn1SccPID dest_pid,
     const char *IN_tmtc_mode, size_t IN_tmtc_mode_len);

/**
 * ASN.1 Type and encoding of the parameters:
 * tc type: TC-Type ; encoding: NATIVE
 *
 * We cannot call vm_communication_if_process_tc since
 * it checks the invoker thread to call the Manager_Facade.
 *
 * This function is based on vm_communication_if_process_tc
 * but removes the constraints so that the sporadic call is not discarded.
 */
static void process_tc(const asn1SccTC_Type * tc) {
    PrintASN1TC_Type(">>> [process_tc] Received TC: ", tc);
    debug_printf(LVL_INFO, "\n"NAME"Forwarding TC to Manager");

    const asn1SccTC_Type *IN_buf_tc = tc;
    const size_t size_IN_buf_tc = sizeof(*tc);

    __po_hi_request_t *request =
        __po_hi_get_request (tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc
    );

    __po_hi_copy_array(
        &(request->vars.tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc.tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc.buffer),
        (void *)IN_buf_tc, size_IN_buf_tc
    );

    request->vars.tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc.tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc.length =
    size_IN_buf_tc;

    request->port = tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc;

    __po_hi_gqueue_store_out(partition_obsw_tm_sender_send_tm_k, tm_sender_send_tm_local_outport_communication_if_process_tc_manager_facade_process_tc, request);
    __po_hi_send_output(partition_obsw_tm_sender_send_tm_k, tm_sender_send_tm_global_outport_communication_if_process_tc_manager_facade_process_tc);
}

/**
 * ASN.1 Type and encoding of the parameters:
 * event type: Balloon-Events ; encoding: NATIVE
 *
 * We cannot call vm_communication_if_process_tc since
 * it checks the invoker thread to call the Manager_Facade.
 *
 * This function is based on vm_communication_if_notify_event
 * but removes the constraints so that the sporadic call is not discarded.
 */
static void notify_event(const asn1SccBalloon_Events * event) {
    const asn1SccBalloon_Events *IN_buf_balloon_event = event;
    const size_t size_IN_buf_balloon_event = sizeof(*event);

    PrintASN1Balloon_Events(">>> [notify_event] Received Event: ", event);
    debug_printf(LVL_INFO, "\n"NAME"Forwarding Event to Manager\n");

    __po_hi_request_t *request = __po_hi_get_request (tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event);
    __po_hi_copy_array(&(request->vars.tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event.tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event.buffer),
    (void *)IN_buf_balloon_event, size_IN_buf_balloon_event);
    request->vars.tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event.tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event.length =
    size_IN_buf_balloon_event;
    request->port = tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event;
    __po_hi_gqueue_store_out(partition_obsw_tm_sender_send_tm_k, tm_sender_send_tm_local_outport_communication_if_notify_event_manager_facade_notify_event, request);
    __po_hi_send_output(partition_obsw_tm_sender_send_tm_k, tm_sender_send_tm_global_outport_communication_if_notify_event_manager_facade_notify_event);
}

/*******************************************************************************
 * Local operations
 ******************************************************************************/

void timespec_add(struct timespec *t1, struct timespec *t2, struct timespec *result) {
    const long BILLION = 1000000000;
    long sec  = t2->tv_sec + t1->tv_sec;
    long nsec = t2->tv_nsec + t1->tv_nsec;
    if (nsec >= BILLION) {
        nsec -= BILLION;
        sec++;
    }
    result->tv_sec = sec;
    result->tv_nsec = nsec;
}

struct timespec seconds_to_timespec(float seconds) {
    struct timespec ts;
    float intPart;
    ts.tv_nsec = (int) (modff(seconds, &intPart) * 1.0E09);
    ts.tv_sec = intPart;
    return ts;
}

enum Recv_Ret_Code {
    E_OK,
    E_RECV_ERROR,
    E_DECD_ERROR
};

static enum Recv_Ret_Code recv_tc(asn1SccTC_Type * tc) {
    static unsigned char buf [asn1SccTC_Type_REQUIRED_BYTES_FOR_ACN_ENCODING];
    bool ok = tcp_svr_recv(buf, asn1SccTC_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);
    if (!ok) {
        return E_RECV_ERROR;
    }

    static BitStream bitstream;
    BitStream_AttachBuffer(&bitstream, buf, asn1SccTC_Type_REQUIRED_BYTES_FOR_ACN_ENCODING);
    int err_code;
    if (!asn1SccTC_Type_ACN_Decode(tc, &bitstream, &err_code)) {
        return E_DECD_ERROR;
    }

    return E_OK;
}

static void notify_disconnection() {
    // 1. Notify disconnection to the TTC by updating the TMTC status:
    asn1SccTMTC_Mode mute = asn1SccTMTC_Mode_mute;
    vm_communication_if_set_internal_mode(PID_tmtc_internal_mode, (const char *) &mute, sizeof(mute));

    // 2. Notify disconnection to the manager:
    asn1SccBalloon_Events lost_comm = asn1SccBalloon_Events_lost_comm;
    notify_event(&lost_comm);
}

static void notify_connection() {
    asn1SccTMTC_Mode nominal = asn1SccTMTC_Mode_nominal;
    vm_communication_if_set_internal_mode(PID_tmtc_internal_mode, (const char *) &nominal, sizeof(nominal));
}

static void * tc_manager_task(void * arg) {
    (void) arg;

    debug_printf(LVL_INFO, NAME"Initializing & config connection...\n");

    bool was_connected = tcp_svr_initialize(TC_PORT);
    if (was_connected) {
        notify_connection();
    }

    // For timing:
    struct timespec miat = seconds_to_timespec(TC_TASK_MIAT);
    struct timespec last;

    // Sporadic activity:
    while (true) {
        // 1. If the socket could not be created nor configured, retry it!
        if (!tcp_svr_is_connected()) {
            debug_printf(LVL_INFO, NAME"Reconnecting\n");
            bool reconnected = tcp_svr_reconnect();
            if (was_connected) {
                if (!reconnected) { ///< Connection --> Disconnection
                    puts(NAME"Connection --> Disconnection: Notifying disconnection");
                    notify_disconnection();
                }
            } else {
                if (reconnected) {  ///< Disconnection --> Connection
                    puts(NAME"Disconnection --> Connection: Notifying connection");
                    notify_connection();
                }
            }
        }

        was_connected = tcp_svr_is_connected(); ///< For the next iteration

        // 2. BLOCKING CALL that waits for a TC reception
        //    This may modify the connection status!
        puts(NAME"Before recv---------------------------->");
        asn1SccTC_Type tc;
        enum Recv_Ret_Code recv_rc = recv_tc(&tc);
        puts(NAME"After recv<-----------------------------");


        /// Gets the time when this task was awaken
        if (clock_gettime(CLOCK_MONOTONIC, &last) < 0) {
            perror(NAME"clock_gettime");
        }

        // 3. Process possible errors while receiving a TC
        switch (recv_rc) {
        case E_RECV_ERROR:
            /// Connection --> Disconnection
            if (was_connected && !tcp_svr_is_connected()) {
                puts(NAME"E_RECV_ERROR: Connection --> Disconnection: notify_disconnection");
                notify_disconnection();
            }
            break;

        case E_DECD_ERROR:
            debug_printf(LVL_WARN, NAME"FAILED to DECODE TC\n");
            break;

        // 4. Send received TC to the Manager for its processing
        case E_OK:
            process_tc(&tc);
            break;
        }

        was_connected = tcp_svr_is_connected(); ///< For the next iteration

        /// @addtogroup Sleeps for remining time
        /// @{
        timespec_add(&last, &miat, &last);
        if (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &last, 0) < 0) {
            perror(NAME"clock_nanosleep");
        }
        /// }@
    }
}


/*******************************************************************************
 * Provided Interfaces (Public operations)
 ******************************************************************************/

bool tc_receiver_initialize() {
    pthread_attr_t attr;
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

    pthread_t tid;
    int err = pthread_create(&tid, NULL, (void *(*)(void *)) tc_manager_task, NULL);
    if (err != 0) {
        perror("pthread_create");
        return false;
    }

    struct sched_param param;
    param.sched_priority = TC_TASK_PRIO;
    if (pthread_setschedparam(tid, SCHED_FIFO, &param) != 0) {
        perror(NAME"pthread_setschedparam");
    }

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(TC_TASK_CORE, &cpuset);
    if (pthread_setaffinity_np(tid, sizeof(cpuset), &cpuset) != 0) {
        perror(NAME"pthread_setaffinity");
    }

    return true;
}
