/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                       FAULT TOLERANT COMMUNICATION_IF                      --
--                                                                            --
--                                  Source                                    --
--                                                                            --
--            Copyright (C) 2023 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "communication_if.h"
#include "debug_messages.h"

#include "communication_if_tm_sender.h"
#include "communication_if_tc_receiver.h"

#define NAME "[COMM-IF-FAULT-TOLERANT] "

/*******************************************************************************
 * Provided Interfaces (Public operations)
 ******************************************************************************/

/**
 * @brief Initializes the devices, ports, etc. used by the Communication I/F
 */
void communication_if_startup(void) {

    bool tc_receiver_initialized = tc_receiver_initialize();
    if (tc_receiver_initialized) {
        debug_printf(LVL_INFO, NAME"TC Receiver initialized\n");
    } else {
        debug_printf(LVL_ERROR, NAME"FAILED to initialize TC Receiver\n");
    }

    bool tm_sender_initialized = tm_sender_initialize();
    if (tm_sender_initialized) {
        debug_printf(LVL_INFO, NAME"TM Sender initialized\n");
    } else {
        debug_printf(LVL_ERROR, NAME"FAILED to initialized TM Sender!\n");
    }

    debug_printf(LVL_INFO, NAME"========== Initialized ===========\n");
}

void communication_if_Send_All_TM
    (const char *IN_hk_tm, size_t IN_hk_tm_len,
     const char *IN_sc_tm, size_t IN_sc_tm_len)
{
    tm_sender_send_hk_tm(IN_hk_tm);
    tm_sender_send_sc_tm(IN_sc_tm);
}

void communication_if_Send_HK_TM
    (const char *IN_hk_tm, size_t IN_hk_tm_len)
{
    tm_sender_send_hk_tm(IN_hk_tm);
}

void communication_if_Send_SC_TM
    (const char *IN_sc_tm, size_t IN_sc_tm_len)
{
    tm_sender_send_sc_tm(IN_sc_tm);
}
