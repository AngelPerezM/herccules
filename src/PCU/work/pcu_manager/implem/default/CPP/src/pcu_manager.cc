/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                                 SUBSYSTEMS                                 --
--                                                                            --
--                             PCU_Manager Source                             --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "pcu_manager.h"
#include "pcu_manager_state.h"

#include "PCU.h"

#include "CsvWriter.h"
#include "String.h"
#include "Images.h"

#include "Time_Management.h"

#include <iostream>

namespace bs = board_support;

// --  Local variables  --------------------------------------------------------

namespace {
    pcu_manager_state ctxt;
}

// --  Internal operations  ----------------------------------------------------

namespace {

    // --------------------
    // Process balloon mode
    // --------------------

    void pcu_process_balloon_mode()
    {
        asn1SccBalloon_Mode mode;
        pcu_manager_RI_Get_Mode(&mode);

        switch (mode) {
            case asn1SccBalloon_Mode_ground_await:
                ctxt.pcu_data.mode  = asn1SccPCU_Mode_off;
                break;
            case asn1SccBalloon_Mode_ground_pre_launch:
            case asn1SccBalloon_Mode_flight_ascent:
            case asn1SccBalloon_Mode_flight_float:
                ctxt.pcu_data.mode  = asn1SccPCU_Mode_on;
                break;
            case asn1SccBalloon_Mode_off:
                ctxt.pcu_data.mode  = asn1SccPCU_Mode_off;
                ctxt.end_of_mission = true;
        }
    }

    // --------------------------------------
    // Measure data + its auxiliary functions
    // --------------------------------------

    /**
     * -   On the 1st cycle, the TC74 is active consuming 200 uA.
     * -   On the 2nd cycle, the TC74 data is read and is deactivated.
     * -   On the 3rd, 4th, 5th cycles, the TC74 is in stand-by mode consuming 5 uA.
     */
    inline void read_tc74(uint8_t cycle)
    {        
        switch (cycle) {
        case 1U:
            bs::pcu::activateTC74();
            break;
        case 2U:
            std::cout << "[PCU-Manager] Reading TC74" << std::endl;
            int8_t temperature;
            if (bs::pcu::readTC74(temperature)) {
                std::cout << "[PCU-Manager] TC74 AVAILABLE" << std::endl;
                ctxt.pcu_data.payload.sensor_data.digital_temperature = temperature;
            }
            bs::pcu::deactivateTC74();
            break;
        default:
            break;
        }
    }

    /**
     * -   On the 2nd cycle, the INA data is read.
     * -   On the 1st, 3rd, 4th and 5th cycles, the INA sensor is measuring the data.
     *
     * NOTE: The reading is performed on the 2nd to update both INA and TC74 in the same cycle.
     */
    inline void read_ina(uint8_t cycle)
    {
        if (cycle == 2U) {
            std::cout << "[PCU-Manager] Reading INA" << std::endl;
            bs::pcu::PowerAndCurrentData ina_reading;
            if (bs::pcu::readPowerAndCurrent(ina_reading)) {
                std::cout << "[PCU-Manager] INA AVAILABLE" << std::endl;
                ctxt.pcu_data.payload.sensor_data.power_watts = ina_reading.power_W;
                ctxt.pcu_data.payload.sensor_data.current_amps = ina_reading.current_A;
                ctxt.pcu_data.payload.sensor_data.voltage_bus_volts = ina_reading.busVoltage_V;
                ctxt.pcu_data.payload.sensor_data.voltage_shunt_volts = ina_reading.shuntVoltage_V;
            }
        }
    }

    /**
     * The measuring behaviour depends on the PCU operating mode and the current cycle.
     * Therere is one hypercycle of 5 seconds, divided into 5 subcycles.
     */
    inline void read_digital_sensors_data()
    {
        static uint8_t cycle {0U};
        cycle = (cycle <= 5U)
              ? (cycle + 1U)
              : (1U);

        switch (ctxt.pcu_data.mode) {
            // --  ON  ---------------------------------------------------------
            case asn1SccPCU_Mode_on:
                if (bs::pcu::initialize_sensors()) {
                    read_tc74(cycle);
                    read_ina(cycle);
                }
                break;
            // --  OFF  --------------------------------------------------------
            case asn1SccPCU_Mode_off:
                bs::pcu::deactivateTC74();
                break;
            // --  Else  -------------------------------------------------------
            default:
                break;
        }
    }

    void pcu_measure_data()
    {        
        // Get time:
        auto && [secs, usecs] = time_management::absolute_time();
        ctxt.pcu_data.snapshot_time = {secs, usecs};
        ctxt.pcu_data.mission_time  = time_management::mission_time();

        // Read actuators status:
        pcu_manager_RI_Get_PS_Lines_Status
                (&ctxt.pcu_data.payload.switches);

        // Read digital data, dependant on the PCU mode:
        read_digital_sensors_data();
    }

    // ----------
    // Store data
    // ----------

    /**
     * Saves the logged data to disk, flushing internal buffers.
     * This is performed each 60 seconds.
     */
    void pcu_dump_data_to_disk() {
        static uint8_t cycle {1U};
        if (cycle == 60U) {
            ctxt.logfile.save_file();
            cycle = 1U;
        }
        cycle++;
    }

    void pcu_store_data() {
        using data_storage::basic_images::image;

        ctxt.logfile.add_line({
            image(static_cast<int32_t>(ctxt.pcu_data.snapshot_time.secs)),
            image(static_cast<int32_t>(ctxt.pcu_data.snapshot_time.usecs)),
            image(static_cast<float>  (ctxt.pcu_data.mission_time)),
            pcu_manager_state::
            image(ctxt.pcu_data.mode),

            image(static_cast<float>(ctxt.pcu_data.payload.sensor_data.power_watts)),
            image(static_cast<float>(ctxt.pcu_data.payload.sensor_data.current_amps)),
            image(static_cast<float>(ctxt.pcu_data.payload.sensor_data.voltage_bus_volts)),
            image(static_cast<float>(ctxt.pcu_data.payload.sensor_data.voltage_shunt_volts)),

            image(static_cast<std::int8_t>(ctxt.pcu_data.payload.sensor_data.digital_temperature)),

            pcu_manager_state::
            image(ctxt.pcu_data.payload.switches.al_line),
            pcu_manager_state::
            image(ctxt.pcu_data.payload.switches.tmu_line),
            pcu_manager_state::
            image(ctxt.pcu_data.payload.switches.sdpu_line)
        });

        pcu_dump_data_to_disk();
    }

    // ------------
    // Publish data
    // ------------

    void pcu_publish_data() {
        pcu_manager_RI_Update_PCU_Data(&ctxt.pcu_data);
    }

}

// --  Provided Interface  -----------------------------------------------------

// ------------------------
// -- Startup & Shutdown --
// ------------------------

/**
 * @note This startup routine is invoked by the TASTE runtime,
 * which guarantees that it's executed only once, even if it was invoked multiple times.
 */
void pcu_manager_startup(void)
{
    ctxt.pcu_data.mode = asn1SccPCU_Mode_off;
    (void) bs::pcu::initialize_switches();
}

/**
 * This is the shutdown routine that shall be invoked when "end of mission" is reached.
 */
void pcu_manager_shutdown(void)
{
    ctxt.logfile.save_file();

    // Notify the Manager that this subsystem has finished:
    static asn1SccBalloon_Events finish_event {asn1SccBalloon_Events_pcu_finished};
    pcu_manager_RI_Notify_Event(&finish_event);    
}

// ----------
// -- Tick --
// ----------

/**
 * Periodic activity that performs the core logic of the PCU subsystem:
 *    -   Monitor its sensors and switches during flight, and pre-launch modes.
 *    -   Record the monitored data.
 *    -   Notify the Manager when finished.
 *
 * Why do we check the `has_finished` condition twice?
 *    -   The condition is updated inside the `pcu_process_balloon_mode` procedure.
 *    -   This is a periodic activity.
 */
void pcu_manager_PI_Tick(void)
{
    if ( ! ctxt.end_of_mission ) {
        // 1.  Nominal activity:
        pcu_process_balloon_mode();
        pcu_measure_data();
        pcu_store_data();
        pcu_publish_data();

        // 2.  Shutdown if end of mission:
        if (ctxt.end_of_mission) {
            pcu_manager_shutdown();
        }
    }
}

// ----------------------
// -- Reset I2C Device --
// ----------------------

void pcu_manager_PI_Reset_PCU_I2C_Device
    (const asn1SccRestartable_Device_ID *device_id)
{
    switch (*device_id) {
        case asn1SccRestartable_Device_ID_pcu_power_current_sensor:
            bs::pcu::resetPowerAndCurrentSensor();
            break;
        case asn1SccRestartable_Device_ID_pcu_tc74:
            bs::pcu::deactivateTC74();
            bs::pcu::activateTC74();
            break;
        default:
            break;
    }
}
