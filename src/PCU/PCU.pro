TEMPLATE = lib
CONFIG -= qt
CONFIG += generateC

DISTFILES += \
    $(OBSW_DATA_TYPES)/DataTypes.acn \
    $(OBSW_DATA_TYPES)/DataTypes.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Events.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Events.asn \
    $(OBSW_DATA_TYPES)/DataTypes-OperatingModes.acn \
    $(OBSW_DATA_TYPES)/DataTypes-OperatingModes.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Subsystems.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Subsystems.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Telecommands.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Telecommands.asn \
    $(OBSW_DATA_TYPES)/DataTypes-Telemetries.acn \
    $(OBSW_DATA_TYPES)/DataTypes-Telemetries.asn \
    host.dv.xml \
    rpi.dv.xml

DISTFILES += PCU.msc
DISTFILES += interfaceview.xml
DISTFILES += work/binaries/*.msc
DISTFILES += work/binaries/coverage/index.html
DISTFILES += work/binaries/filters
DISTFILES += work/system.asn
DISTFILES +=
DISTFILES += PCU.asn
DISTFILES += PCU.acn

INCLUDEPATH += \
    ../build/rpi/debug/include/Bus_Handlers \
    ../build/rpi/debug/include/Equipment_Handlers \
    ../build/rpi/debug/include/Board_Support \
    ../build/rpi/debug/include/Time_Management \
    ../build/rpi/debug/include/Data_Storage \

include(work/taste.pro)
message($$DISTFILES)
