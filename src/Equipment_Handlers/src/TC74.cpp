/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                                TC74  Source                                --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "TC74.h"
#include "BusHandlers.h"

#include <iostream>
#include <chrono>

namespace bh    = bus_handlers;
namespace bh_dt = bus_handlers::data;

using std::cout;

namespace {
    constexpr uint8_t SBY_SWITCH_MASK = 0x80U;
    constexpr uint8_t DATA_READY_MASK = 0x40U;

    constexpr uint8_t TEMPERATURE_REG_ADDR = 0x00U;
    constexpr uint8_t CONFIG_REG_ADDR      = 0x01U;

    // -----------------
    // Private functions
    // -----------------

    /**
     * Class std::chrono::steady_clock represents a monotonic clock.
     * It can't decrease as physical time moves forward.
     * @return elapsed time in msecs
     */
    template <
            class result_t   = std::chrono::milliseconds,
            class clock_t    = std::chrono::steady_clock,
            class duration_t = std::chrono::milliseconds
    >
    auto elapsedTimeSince(std::chrono::time_point<clock_t, duration_t> const& start)
    {
        return (std::chrono::duration_cast<result_t>(clock_t::now() - start)).count();
    }
}

namespace equipment_handlers {

    /**
     * This function initializes the local functions and must be invoked before any operation
     */
    bool TC74::initialize(bh_dt::I2CBusID i2CBusId, uint8_t address, Mode mode) {
        m_i2cBusID   = i2CBusId;
        m_i2cAddress = address;

        bool i2cSuccess   = bus_handlers::initialize();
        bool setupSuccess = setMode(mode);

        return i2cSuccess && setupSuccess;
    }

    /**
     * @param mode Operational mode from the TC74 device, STANDBY or NORMAL.
     * @return true if successfully
     */
    bool TC74::setMode(Mode mode) {
        bool success = bh::writeByteRegister(m_i2cBusID, m_i2cAddress,
                                             CONFIG_REG_ADDR,
                                             mode);
        if (success) {
            m_currentMode = mode;
        }
        return success;
    }

    bool TC74::readTemperature(int8_t &temperature) {
        uint8_t raw {0U};
        bool success = bh::readByteRegister
                            (m_i2cBusID, m_i2cAddress,
                             TEMPERATURE_REG_ADDR,
                             raw);
        temperature = static_cast<int8_t>(raw);

        return success;
    }

    /**
     * This operation is dependant on the sensor operating mode.
     * If the current mode is Stand-By, the sensor must be set to
     * Normal to read the latest temperature.
     */
    bool TC74::readTemperatureBlocking(int8_t &temperature,
                                       int     max_msecs_timeout)
    {
        const Mode previousMode {m_currentMode};
        if (m_currentMode == Mode::Standby) {
            (void) setMode(TC74::Mode::Normal);
            if (!waitForTemperature(max_msecs_timeout)) { // could not wait for temperature
                return false;
            }
        }

        bool success = readTemperature(temperature);

        if (previousMode != m_currentMode) {
            (void) setMode(previousMode);
        }

        return success;
    }

    TC74::Mode TC74::getMode() {
        auto cfgReg = readConfigRegister();
        bool inStandby = cfgReg & SBY_SWITCH_MASK;
        m_currentMode = inStandby ? Mode::Standby : Mode::Normal;
        return m_currentMode;
    }

    bool TC74::isTheDataReady() const {
        auto cfgReg = readConfigRegister();
        bool isReady = cfgReg & DATA_READY_MASK;
        return isReady;
    }

    uint8_t TC74::readConfigRegister() const {
        uint8_t statusRegValue;
        (void) bus_handlers::readByteRegister(m_i2cBusID,
                                              m_i2cAddress,
                                              CONFIG_REG_ADDR,
                                              statusRegValue);
        return statusRegValue;
    }

    bool TC74::waitForTemperature(int max_msecs_timeout) const {
        bool dataReady;
        bool timeout;
        auto start = std::chrono::steady_clock::now();
        do {
            dataReady = isTheDataReady();
            timeout   = elapsedTimeSince(start) > max_msecs_timeout;
        } while (!dataReady && !timeout);

        return dataReady;
    }

}
