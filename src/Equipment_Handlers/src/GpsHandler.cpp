/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                            GPSHandler Source                               --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "GpsHandler.h"
#include "GpsHandler_hw.h"
#include "minmea.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1 // See feature_test_macros(7)
#endif
#include <sched.h>    // CPU_ZERO, CPU_SET, affinity
#include <pthread.h>  // pthread_*
#include <stdio.h>    // perror
#include <sys/time.h>

namespace eh = equipment_handlers;

/*******************************************************************************
 * Local macros
 ******************************************************************************/

#define GPS_HANDLER_CORE (1)
#define GPS_HANDLER_PRIO (2)
#define GPS_HANDLER_MIAT (1.0F/10.0F)

#define NAME "[GPS_HANDLER] "

/*******************************************************************************
 * Local variables, constants, and types
 ******************************************************************************/
namespace {
    std::string g_serial_port_name = "";
    eh::gps_handler::receiveNMEADataCallback callback;
    
    auto op_mode_pr {eh::gps_handler::OperatingMode::INACTIVE};
    pthread_mutex_t op_mode_mtx;

    bool gps_initialized = false;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/

namespace {
    bool initialize_gps() {
        if (!gps_initialized) {
            gps_initialized = eh::gps_handler::hw::initialize(g_serial_port_name);
        }
        return gps_initialized;
    }

    void finalize_gps() {
        if (gps_initialized) {
            eh::gps_handler::hw::finalize();
            gps_initialized = false;
        }
    }

    void timespec_add(timespec &t1, timespec &t2, timespec &result) {
        constexpr auto BILLION {1000000000};
        long sec  = t2.tv_sec + t1.tv_sec;
        long nsec = t2.tv_nsec + t1.tv_nsec;
        if (nsec >= BILLION) {
            nsec -= BILLION;
            sec++;
        }
        result.tv_sec = sec;
        result.tv_nsec = nsec;
    }

    struct timespec seconds_to_timespec(float seconds) {
        struct timespec ts;
        float intPart;
        ts.tv_nsec = int (modff(seconds, &intPart) * 1.0E09);
        ts.tv_sec = intPart;
        return ts;
    }

    bool gt_time(struct minmea_time * a, minmea_time * b) {
        return (a->hours > b->hours) ||
               (a->hours == b->hours && a->minutes > b->minutes) ||
               (a->hours == b->hours && a->minutes == b->minutes && a->seconds > b->seconds);
    }

    void parse_nmea(char const * nmeaMessage) {
        /// Data sent to user
        static struct equipment_handlers::gps_handler::gps_data_t gps_data;

        static struct minmea_time rmc_time {0,0,0,0};
        static struct minmea_time gga_time {0,0,0,0};

        switch (minmea_sentence_id(nmeaMessage, false)) {
            case MINMEA_SENTENCE_RMC: {
                struct minmea_sentence_rmc rmc;
                bool success {minmea_parse_rmc(&rmc, nmeaMessage)};
                if (success) {
                    /// Send parsed data so far if this RMC data
                    /// belongs to a greater timestamp
                    rmc_time = rmc.time;
                    if (gt_time(&rmc_time, &gga_time)) {
                        callback(gps_data);
                    }

                    minmea_gettime(&gps_data.unix_time, &rmc.date, &rmc.time);
                    gps_data.latitude_deg = minmea_tocoord(&rmc.latitude);
                    gps_data.longitude_deg = minmea_tocoord(&rmc.longitude);
                    gps_data.sog_knots = minmea_tofloat(&rmc.speed);
                    gps_data.cog_deg = minmea_tofloat(&rmc.course);
                }
            } break;

            case MINMEA_SENTENCE_GGA: {
                struct minmea_sentence_gga gga;
                bool success {minmea_parse_gga(&gga, nmeaMessage)};
                if (success) {
                    /// Send parsed data so far if this GGA data
                    /// belongs to a greater timestamp
                    gga_time = gga.time;
                    if (gt_time(&gga_time, &rmc_time)) {
                        callback(gps_data);
                    }

                    gps_data.altitude_m = minmea_tofloat(&gga.altitude);
                }
            } break;

            case MINMEA_INVALID:
                printf(NAME"ERROR: checksum\n");
                break;

            case MINMEA_UNKNOWN:
                printf(NAME"ERROR: unkown\n");
                break;

            default:
                break;
        }
    }

    /*
     * Sporadic thread that reads NMEA messages
     */
    void * dataReaderTh(void * threadId) {
        (void) threadId;

        // Setup local resources:
        static char nmeaMessage[eh::gps_handler::hw::MAX_MESSAGE_SIZE_BYTES];
        eh::gps_handler::hw::cleanRxBuffer();

        // For timing:
        struct timespec miat {seconds_to_timespec(GPS_HANDLER_MIAT)};
        struct timespec last;

        // Sporadic activity:
        while (true) {
            if (eh::gps_handler::getOperatingMode() == eh::gps_handler::ACTIVE) {
                puts(NAME"Mode-ACTIVE----------------------------------------");
                if (initialize_gps()) {
                    puts(NAME"GPS Initialized");

                    /// Blocking call that returns a NMEA message
                    bool success {
                        eh::gps_handler::hw::readNMEAMessage(
                            nmeaMessage,
                            eh::gps_handler::hw::MAX_MESSAGE_SIZE_BYTES
                        )
                    };

                    /// Gets the time when this task was awaken
                    if (clock_gettime(CLOCK_MONOTONIC, &last) < 0) {
                        perror(NAME"clock_gettime");
                    }

                    /// Parses NMEA message if successful:
                    if (success) {
                        parse_nmea(nmeaMessage);
                    } else {
                        puts(NAME"Error: Couldn't read NMEA");
                    }
                } else {
                    puts(NAME"*************** ERROR ****************\n"
                         NAME"FAILED to initialize GPS, retrying ...");
                }
            } else {
                /// Gets the time when this task was awaken
                if (clock_gettime(CLOCK_MONOTONIC, &last) < 0) {
                    perror(NAME"clock_gettime");
                }

                finalize_gps();

                /// @addtogroup Sleeps for remaining time
                /// @{
                timespec_add(last, miat, last);
                if (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &last, 0) < 0) {
                    perror(NAME"clock_nanosleep");
                }
                /// }@
            }            
        }
    }
}

// --------------------------------
// Public functions implementations
// --------------------------------

namespace equipment_handlers::gps_handler {

    bool initialize(std::string const & serial_port_name) {
        g_serial_port_name = serial_port_name;
        return true;
    }

    void startDataAcquisition(receiveNMEADataCallback cb) {
        /// save callback function in global variable
        callback = std::move(cb);

        /// Create MUTEX for Operating Mode
        pthread_mutex_init(&op_mode_mtx, NULL);

        /// create the data reader thread
        pthread_attr_t attr;
        pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

        pthread_t tid;
        int err = pthread_create(&tid, NULL, (void *(*)(void *)) dataReaderTh, NULL);
        if (err != 0) {
            perror(NAME"pthread_create");
        }

        struct sched_param param;
        param.sched_priority = GPS_HANDLER_PRIO;
        if (pthread_setschedparam(tid, SCHED_FIFO, &param) != 0) {
            perror(NAME"pthread_setschedparam");
        }

        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        CPU_SET(GPS_HANDLER_CORE, &cpuset);
        if (pthread_setaffinity_np(tid, sizeof(cpuset), &cpuset) != 0) {
            perror(NAME"pthread_setaffinity");
        }  
    }

    void setOperatingMode(OperatingMode opMode) {
        pthread_mutex_lock(&op_mode_mtx);
        op_mode_pr = opMode;
        pthread_mutex_unlock(&op_mode_mtx);
    }

    OperatingMode getOperatingMode() {
        OperatingMode opMode;
        pthread_mutex_lock(&op_mode_mtx);
        opMode = op_mode_pr;
        pthread_mutex_unlock(&op_mode_mtx);
        return opMode;
    }


} // equipment_handlers::gps_reader

/**
 * | NMEA messages | RMC | GGA | GSV | GLL | GSA | VTG |
 * |---------------+-----+-----+-----+-----+-----+-----|
 * |     unix_time |  X  |  T  |     |  T  |     |     |
 * |  latitude_deg |  X  |  X  |     |  X  |     |     |
 * | longitude_deg |  X  |  X  |     |  X  |     |     |
 * |    altitude_m |     |  X  |     |     |     |     |
 * |       sog_mps |  X  |     |     |     |     |  X  |
 * |       cog_deg |  X  |     |     |     |     |     |
 * 
 */

/* =======
 * = RMC =
 * =======
 * Recommended Minimum data
 *  - $GPRMC
 *  - Time of position fix [hhmmss.ss]
 *  - status: validity of data
 *  - latitude [ddmm.mmmm]
 *  - N/S
 *  - longitude [dddmm.mmmm]
 *  - E/W
 *  - Speed over gnd [knots]
 *  - course over gnd [degrees]
 *  - Date: [dd-mm-yy]
 *  - ... empty fields ...
 *  - Checksum
 *  - <CR><LF>
 */
 /*
 * =======
 * = GGA =
 * =======
 * Global positioning system fix data
 *  - $GPGGA
 *  - UTC Time [hhmmss.sss]
 *  - Latitude [ddmm.mmmm]
 *  - N/S
 *  - Longitude [ddmm.mmmm]
 *  - E/W
 *  - Fix validity
 *  - Satellites used [0 -> 12]
 *  - HDOP [hdop]
 *  - Mean Sea Level altitude [meters]
 *  - M
 *  - Geoid Separation [Altref]
 *  - M
 *  - Diff. reference station
 *  - Checksum
 *  - <CR><LF>
 */
 /*
 * =======
 * = GSV =
 * =======
 * GNSS Satellites in View
 * GSV messages can be divided in various NMEA sentences!
 *  - $GPGSV
 *  - ...
 */
 /* 
 * =======
 * = GLL =
 * =======
 * Latitude and longitude, with time of position fix and status
 *  - $GPGLL
 *  - Latitude [ddmm.mmmm]
 *  - N/S
 *  - Longitude [dddmm.mmmm]
 *  - E/W
 *  - UTC time [hhmmss.ss]
 *  - Validity
 *  - Position fix mode
 *  - Checksum
 *  - <CR><LF>
 */
 /* 
 * =======
 * = GSA =
 * =======
 * GNSS Dilution Of Precision and Active Satellites
 *  - $GPGSA
 *  - Mode
 *  - Fix status
 *  - ...
 */
 /* 
 * =======
 * = VTG =
 * =======
 * Course over ground and Ground speed
 *  - $GPVTG
 *  - Course over ground true [degrees]
 *  - Course over ground magnetic [degrees]
 *  - Speed over ground [knots]
 *  - Speed over ground [kph]
 *  - mode indicator
 *  - Checksum
 *  - <CR><LF>
 */
