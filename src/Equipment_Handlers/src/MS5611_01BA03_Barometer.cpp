/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                    MS5611_01BA03_Barometer  Source                         --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "MS5611_01BA03_Barometer.h"
#include "BusHandlers.h"
#include <ctime>  // nanosleep
#include <cstdio> // perror
#include <iostream>

namespace bh = bus_handlers;
namespace bh_dt = bus_handlers::data;

namespace {
    constexpr bool inRange(float min, float max, float val) {
        return ((val >= min) && (val <= max));
    }

    constexpr bool isPressureValid(float milliBar) {
        const float min_milliBar = 10.0F;
        const float max_milliBar = 1200.0F;
        return inRange(min_milliBar, max_milliBar, milliBar);
    }

    constexpr bool isTemperatureValid(float t_celsius) {
        const float min_celsius = -40.0F;
        const float max_celsius = 85.0F;
        return inRange(min_celsius, max_celsius, t_celsius);
    }

    /// Violates MISRA 18-0-4, but `clock_nanosleep` is recommended in the Burns & Wellings RTS&PL book.
    void waitNSecs(int transitionTimeNSecs) {
        struct timespec wait {
                .tv_sec = 0,
                .tv_nsec = transitionTimeNSecs
        };

        if(clock_nanosleep(CLOCK_MONOTONIC, 0, &wait, nullptr) < 0) {
            perror("clock_nanosleep");
        }
    }

    /// \brief returns [2 ^ exponent] which is equivalent to [2 << (exp - 1)]
    constexpr int32_t _2_pow(int exponent) {
        return 2 << (exponent-1);
    }
}

namespace equipment_handlers {

    MS5611_01BA03_Barometer::MS5611_01BA03_Barometer() = default;

    MS5611_01BA03_Barometer::~MS5611_01BA03_Barometer() = default;

    bool MS5611_01BA03_Barometer::initialize
        (bh_dt::I2CBusID i2cBusID,
         uint8_t i2cAddress,
         OSR osr,
         bool enable2ndOrderCompensation)
    {
        m_i2cBusID = i2cBusID;
        m_i2cAddress = i2cAddress;
        m_osr = osr;
        secondOrderCompensationEnabled = enable2ndOrderCompensation;

        if (!bh::initialize()) {
            return false;
        }

        bool success = reset();
        waitNSecs(3000000);
        success = success && setupCoefficients();
        return success;
    }

    /***********************
     * Provided interfaces *
     ***********************/

    bool MS5611_01BA03_Barometer::reset() {
        return bus_handlers::writeCommand(m_i2cBusID, m_i2cAddress, RESET_REG_ADDR);
    }

    bool MS5611_01BA03_Barometer::get(Reading &data) {
        int32_t D1 = 0;
        bool d1Available = readDigitalPressure(D1);

        int32_t D2 = 0;
        bool d2Available = readDigitalTemperature(D2);

        if (d2Available) { // continue with temperature calculation
            int32_t dT;
            int32_t TEMP;
            calculateTemperature(D2, dT, TEMP);

            data.temperature_raw = D2;
            data.temperature_celsius = static_cast<float>(TEMP) / 100.0F;

            if (d1Available) { // continue with temperature compensation
                int32_t P;
                calculateTemperatureCompensatedPressure(D1, dT, TEMP, P);

                data.pressure_raw = D1;
                data.pressure_milliBar = static_cast<float>(P) / 100.0F;
            }
        }

        return d1Available && d2Available;
    }

    /***********************
     * Auxiliary Functions *
     ***********************/

    bool MS5611_01BA03_Barometer::setupCoefficients() {
        bool success = false;
        try {
            C.at(_1) = static_cast<int32_t>(readPROM(C1_REG_ADDR));
            C.at(_2) = static_cast<int32_t>(readPROM(C2_REG_ADDR));
            C.at(_3) = static_cast<int32_t>(readPROM(C3_REG_ADDR));
            C.at(_4) = static_cast<int32_t>(readPROM(C4_REG_ADDR));
            C.at(_5) = static_cast<int32_t>(readPROM(C5_REG_ADDR));
            C.at(_6) = static_cast<int32_t>(readPROM(C6_REG_ADDR));

            success = true;
        } catch (PROM_Lecture_Error const &) {
            // TODO: Log error!
            std::cerr << "could not read PROM";
        }

        return success;
    }

    bool MS5611_01BA03_Barometer::readDigitalPressure(int32_t &D1) const {
        uint8_t cmd = CONVERT_D1_BASE_ADDR | static_cast<uint8_t>(m_osr);
        bool success = bus_handlers::writeCommand(m_i2cBusID, m_i2cAddress, cmd);

        if (success) {
            waitNSecs(8220000);
            success = readADC(D1);
        }

        return success;
    }

    bool MS5611_01BA03_Barometer::readDigitalTemperature(int32_t &D2) const {
        uint8_t cmd = CONVERT_D2_BASE_ADDR | static_cast<uint8_t>(m_osr);
        bool success = bus_handlers::writeCommand(m_i2cBusID, m_i2cAddress, cmd);

        if (success) {
            waitNSecs(8220000);
            success = readADC(D2);
        }

        return success;
    }

    void MS5611_01BA03_Barometer::calculateTemperature
            (int32_t D2,
             int32_t &dT, int32_t &TEMP)
    {
        dT = D2 - (C[_5] * _2_pow(8));
        TEMP = 2000 + ((dT * C[_6]) / _2_pow(23));
    }

    void MS5611_01BA03_Barometer::calculateTemperatureCompensatedPressure
        (int32_t D1, int32_t dT,
         int32_t &TEMP, int32_t &P)
    {
        int64_t OFF  = static_cast<int64_t>(C[_2]) * static_cast<int64_t >(_2_pow(16)) +
                       static_cast<int64_t>(C[_4]) * static_cast<int64_t >(dT) / static_cast<int64_t >(_2_pow(7));
        int64_t SENS = static_cast<int64_t>(C[_1]) * static_cast<int64_t >(_2_pow(15)) +
                       static_cast<int64_t>(C[_3]) * static_cast<int64_t >(dT) / static_cast<int64_t >(_2_pow(8));

        if (secondOrderCompensationEnabled) {
            apply2ndOrderTemperatureCompensation(dT, OFF, SENS, TEMP);
        }

        P = static_cast<int32_t>(((D1 * SENS / _2_pow(21)) - OFF) / _2_pow(15));
    }

    void MS5611_01BA03_Barometer::apply2ndOrderTemperatureCompensation
        (int32_t dT,
         int64_t &OFF, int64_t &SENS, int32_t &TEMP)
    {
        int32_t T2 = 0;
        int64_t OFF2 = 0;
        int64_t SENS2 = 0;

        if (TEMP < 2000) {     // Low temperature
            T2 = dT * dT / _2_pow(31);
            OFF2  = 5 * ((TEMP - 2000) * (TEMP - 2000)) / 2;
            SENS2 = OFF2 / 2;
            if (TEMP < 1500) { // Very low temperature
                OFF2 = OFF2 + (7 * (TEMP + 1500) * (TEMP + 1500));
                SENS2 = SENS2 + (11 * (TEMP + 1500) * (TEMP + 1500) / 2);
            }
        }

        // Calculate pressure and temperature:
        TEMP = TEMP - T2;
        OFF  = OFF - OFF2;
        SENS = SENS - SENS2;
    }

    uint16_t MS5611_01BA03_Barometer::readPROM(uint8_t address) const {
        uint16_t result = 0U;
        bool success = bus_handlers::readWordRegister(m_i2cBusID, m_i2cAddress, address, result, bus_handlers::data::ByteOrdering::Big);
        if (!success) {
            throw PROM_Lecture_Error();
        }
        return result;
    }

    bool MS5611_01BA03_Barometer::readADC(int32_t &raw_reading) const {
        uint32_t rx = 0U;
        bool success = bus_handlers::read3ByteRegister(m_i2cBusID,
                                                 m_i2cAddress,
                                                 ADC_READ_REG_ADDR,
                                                 rx,
                                                 bus_handlers::data::ByteOrdering::Big);
        if (success) { // save reading in out parameter
            raw_reading = static_cast<int32_t>(rx);
        }

        return success;
    }

}