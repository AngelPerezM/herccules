/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                           ADS1115_ADC  Source                              --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "ADS1115_ADC.h"
#include "BusHandlers.h"
#include <iostream>
#include <bitset>

namespace bh = bus_handlers;
namespace bh_dt = bus_handlers::data;

// -- Registers:

namespace {
    // All Registers addresses:
    static constexpr uint8_t CONVERSION_REG_ADDR = 0x00U;
    static constexpr uint8_t CONFIG_REG_ADDR     = 0x01U;
    static constexpr uint8_t LO_THRESH_REG_ADDR  = 0x02U;
    static constexpr uint8_t HI_THRESH_REG_ADDR  = 0x03U;
    static constexpr uint8_t RESET_REG_ADDR      = 0x06U;

    // Masks for CONFIG_REG fields:
    static constexpr uint16_t OS_Mask        = 0b1000'0000'0000'0000U;
    static constexpr uint16_t MUX_Mask       = 0b0111'0000'0000'0000U;
    static constexpr uint16_t PGA_Mask       = 0b0000'1110'0000'0000U;
    static constexpr uint16_t MODE_Mask      = 0b0000'0001'0000'0000U;
    static constexpr uint16_t DATA_RATE_Mask = 0b0000'0000'1110'0000U;
    static constexpr uint16_t COMP_MODE_Mask = 0b0000'0000'0001'0000U;
    static constexpr uint16_t COMP_POL_Mask  = 0b0000'0000'0000'1000U;
    static constexpr uint16_t COMP_LAT_Mask  = 0b0000'0000'0000'0100U;
    static constexpr uint16_t COMP_QUE_Mask  = 0b0000'0000'0000'0011U;
}

// -- Constants:
namespace {
    uint16_t MAX_RESET_ATTEMPTS = 10U;
}


namespace equipment_handlers {

    ADS1115_ADC::ADS1115_ADC() = default;

    bool ADS1115_ADC::initialize
        (bh_dt::I2CBusID i2cBusID,
         uint8_t address,
         ADS1115_ADC::Mode mode)
    {
        m_i2cBusID = i2cBusID;
        m_address  = address;

        bool bhInitialized = bh::initialize();
        if (!bhInitialized) {
            std::cout << "[ADS1115_ADC] Could not initialize Bus Handlers!" << std::endl;
            return false;
        }

        uint16_t reset_count {1U};
        bool     reset_success {false};
        // Resets the ADC at most MAX_RESET_ATTEMPTS times:
        while ((reset_count <= MAX_RESET_ATTEMPTS) && !reset_success) {            
            reset_success = reset();
            std::cout << "[ADS1115_ADC] Count for reset: " << reset_count << std::endl;
            reset_count++;
        }

        auto success = setMode(mode);
        (void) getConversionRegister(); // clear previous reading

        return success && reset_success;
    }

    int16_t ADS1115_ADC::readOneChannel(Channel channel, uint8_t n_retries) const {
        // Clear MUX channel bits:
        uint16_t config = 0U       // Clear all
                        | mode_    // Set safeguarded mode configuration
                        | OS_Mask  // Set OS bit to start one conversion
                        | static_cast<uint16_t>(channel); // Set channel

        (void) setConfigRegister(config);

        // Try to read ADC up to n_retries times:
        int retry_count {1U};
        bool success {false};
        do {
            waitForConversion(getMode().dataRate);

            success = isConversionAvailable();
            retry_count++;
        } while ((success == false) && (retry_count <= n_retries));

        // Return reading, or -1 in case of failure:
        if (!success) {
            std::cout << "[ADS1115_ADC] Conversion NOT available after " << retry_count << " retries\n";
            return -1;
        } else {
            return getConversionRegister();
        }
    }

    std::array<int16_t, 4U> ADS1115_ADC::readAllChannels(uint8_t n_retries) const {
        std::array<ADS1115_ADC::Channel, 4U> channels = {
            Channel::Channel0, Channel::Channel1, Channel::Channel2, Channel::Channel3
        };
        std::array<int16_t, 4U> readings {};
        for (size_t ch = 0U; ch < readings.size(); ++ch) {
            readings.at(ch) = readOneChannel(channels.at(ch), n_retries);
        }
        return readings;
    }

    bool ADS1115_ADC::setMode(ADS1115_ADC::Mode mode) {
        // Set new Mode
        // OS and MUX are set/cleared by other opertions, not this
        mode_ = 0U
              | static_cast<uint16_t>(mode.pga)
              | static_cast<uint16_t>(mode.opMode)
              | static_cast<uint16_t>(mode.dataRate)
              | static_cast<uint16_t>(mode.compMode)
              | static_cast<uint16_t>(mode.polarity)
              | static_cast<uint16_t>(mode.latch)
              | static_cast<uint16_t>(mode.queue);

        // Read MUX bits and DO NOT change them
        auto mux = getConfigRegister() & (MUX_Mask);

        return setConfigRegister(mode_ | mux);
    }

    uint16_t ADS1115_ADC::getConfigRegister() const {
        uint16_t config = 0U;
        bool success = bh::readWordRegister(m_i2cBusID, m_address, CONFIG_REG_ADDR, config);
        if (!success) {
            std::cerr << "[ADS1115_ADC] Could not read config register" << std::endl;
        }
        return config;
    }

    bool ADS1115_ADC::setConfigRegister(uint16_t config) const {
        bool success = bh::writeWordRegister(m_i2cBusID, m_address, CONFIG_REG_ADDR, config);
        if (!success) {
            std::cerr << "[ADS1115_ADC] could not set the config register" << std::endl;
        }
        return success;
    }

    ADS1115_ADC::Mode ADS1115_ADC::getMode() const {
        uint16_t config = getConfigRegister();
        ADS1115_ADC::Mode mode;
        mode.pga      = static_cast<PGAMode>(config & PGA_Mask);
        mode.opMode   = static_cast<OperatingMode>(config & MODE_Mask);
        mode.dataRate = static_cast<DataRate>(config & DATA_RATE_Mask);
        mode.compMode = static_cast<ComparatorMode>(config & COMP_MODE_Mask);
        mode.polarity = static_cast<ComparatorPolarity>(config & COMP_POL_Mask);
        mode.latch    = static_cast<LatchingComparator>(config & COMP_LAT_Mask);
        mode.queue    = static_cast<ComparatorQueueConfig>(config & COMP_QUE_Mask);
        return mode;
    }

    bool ADS1115_ADC::reset() {
        return bus_handlers::writeCommand(m_i2cBusID, m_address, RESET_REG_ADDR);
    }

    /**
     * @brief Waits for 1 / Data Rate seconds. Conversions in the ADS111x settle within a single cycle,
     *        thus, the conversion time is equal to 1 / DR
     * @returns true if conversion is available, false otherwise
     */
    void ADS1115_ADC::waitForConversion(ADS1115_ADC::DataRate dr) const {
        // Look Up Table (LUT) for the transition times (nanoseconds)
        static constexpr std::array transitionTimeNSecs_LUT = {
            125000000, 62500000, 31250000, 15625000, 7812500, 4000000, 2105264, 1162791
        };

        static constexpr int startupNSecs = 25000;
        // From datasheet pg 21:
        //   When the OS bit is asserted the device powers up
        //   in approximately 25 usecs, resets the OS bit, and starts the conversion.

        static constexpr int muxTransitionNSecs = 20000;
        // From Stackoverflow:
        //   10 usecs for channel transition

        struct timespec wait {
            .tv_sec = 0,
            .tv_nsec = muxTransitionNSecs
                     + startupNSecs
                     + transitionTimeNSecs_LUT.at(static_cast<size_t>(dr) >> 5U)
        };

        if (clock_nanosleep(CLOCK_MONOTONIC, 0, &wait, nullptr) < 0) {
            perror("clock_nanosleep");
        }
    }

    bool ADS1115_ADC::isConversionAvailable() const {
        uint16_t config = getConfigRegister();
        bool conversionAvailable = (config & OS_Mask) != 0U;
        return conversionAvailable;
    }

    /// @returns 2C raw reading
    int16_t ADS1115_ADC::getConversionRegister() const {
        uint16_t conversion = 0U;
        bool success = bh::readWordRegister(m_i2cBusID, m_address, CONVERSION_REG_ADDR, conversion);
        if (!success) {
            std::cerr << "[ADS1115_ADC] Could not read conversion register" << std::endl;
        }
        return static_cast<int16_t>(conversion);
    }

}
