/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                              Switch  Source                                --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "Switch.h"
#include <pigpio.h>
#include <iostream>
#include "pigpio_initialization.h"

namespace equipment_handlers {

    Switch::Switch() = default;

    Switch::Switch(unsigned int pin) {
        bool success = initialize(pin);
        if (!success) {
            std::cerr << "pigpio initialisation failed" << std::endl;
        }
    }

    bool Switch::initialize(unsigned int pin) {
        this->m_pin = pin;
        bool success = false;
        if (pigpio_initialization::initialize()) {
            int res = gpioSetMode(pin, static_cast<unsigned>(PI_OUTPUT));
            if (res != 0) {
                std::cerr << "Bad " << ((res == PI_BAD_GPIO) ? "GPIO" : "level") << std::endl;
            } else {
                success = (gpioWrite(pin, 0U) == 0);
            }
        }

        return success;
    }

    void Switch::finalize() {
        bool success = switchOff();
        if (!success) {
            std::cerr << "Could not turn off the switch" << std::endl;
        }
    }

    bool Switch::switchOn() const {
        int res = gpioWrite(this->m_pin, 1U);
        return res == 0;
    }

    bool Switch::switchOff() const {
        int res = gpioWrite(this->m_pin, 0U);
        return res == 0;
    }
}
