/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                                MUX Source                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#include "MUX.h"
#include "pigpio_initialization.h"

#include <pigpio.h>
#include <iostream>

namespace equipment_handlers {

    MUX::MUX() = default;

    bool MUX::initialize(unsigned int pin0, unsigned int pin1, unsigned int pin2) {
        this->selectorPins = {pin0, pin1, pin2};
        pigpio_initialization::initialize();
        return initializeSelectorPins();
    }

    bool MUX::initializeSelectorPins() const {
        bool success = true;

        for (unsigned pin : selectorPins) {
            int res = gpioSetMode(pin, static_cast<unsigned>(PI_OUTPUT));
            if (res != 0) {
                success = false;
                std::cerr << "Bad" << ((res == PI_BAD_GPIO) ? "GPIO" : "level") << std::endl;
            } else {
                (void) gpioWrite(pin, 0U);
            }
        }

        return success;
    }

    void MUX::finalize() {
        for (unsigned pin : selectorPins) {
            int res = gpioWrite(pin, 0U);
            if (res != 0) {
                std::cerr << "Bad" << ((res == PI_BAD_GPIO) ? "GPIO" : "level") << std::endl;
            }
        }
    }

    void MUX::selectInput(int input) {
        if (currentInput == input) {
            return;
        }

        for (size_t i = 0U; i < selectorPins.size(); ++i) {
            bool isSet = input & (1 << i);
            unsigned value = isSet ? 1U : 0U;
            int res = gpioWrite(selectorPins.at(i), value);
            if (res != 0) {
                std::cerr << "Bad" << ((res == PI_BAD_GPIO) ? "GPIO" : "level") << std::endl;
                return;
            }
        }

        waitTransition();
        currentInput = input;
    }

    /// Violates MISRA 18-0-4, but `clock_nanosleep` is recommended in the Burns & Wellings RTS&PL book.
    void MUX::waitTransition() {
        struct timespec wait {
            .tv_sec = 0,
            .tv_nsec = transitionTimeNSecs
        };

        if(clock_nanosleep(CLOCK_MONOTONIC, 0, &wait, nullptr) < 0) {
            perror("clock_nanosleep");
        }
    }

}
