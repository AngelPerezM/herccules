/*****************************************************************************
**                                                                          **
**                    HERCCULES Software Components                         **
**                                                                          **
**                         Equipment_Handlers                               **
**                                                                          **
**                            ADC  Tests                                    **
**                                                                          **
**          Copyright (C) 2022 Universidad Politécnica de Madrid            **
**                                                                          **
** This is free software;  you can redistribute it  and/or modify it  under **
** terms of the  GNU General Public License as published  by the Free Soft- **
** ware  Foundation;  either version 3,  or (at your option) any later ver- **
** sion.  This software is distributed in the hope  that it will be useful, **
** but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- **
** TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public **
** License for  more details.  You should have  received  a copy of the GNU **
** General  Public  License  distributed  with  this  software;   see  file **
** COPYING3.  If not, go to http://www.gnu.org/licenses for a complete copy **
** of the license.                                                          **
**                                                                          **
*****************************************************************************/

#include "BusHandlers_Data.h"
#include "ADS1115_ADC.h"

#include <gtest/gtest.h>

namespace eh      = equipment_handlers;
namespace bh_data = bus_handlers::data;

// ==============
// ADCTests class
// ==============

class ADCTests : public ::testing::Test {
protected:

    static constexpr float maximumDeviationRaw { 0.01 * 1E6 / 125 }; // Means 0.01 volts

    static constexpr auto i2cbus  {bh_data::I2CBusID::BUS0};
    static constexpr auto address {eh::ADS1115_ADC::primaryI2CAddress};
    static constexpr eh::ADS1115_ADC::Mode mode_default {
        .pga      = eh::ADS1115_ADC::PGAMode::FSR_4V096,
        .opMode   = eh::ADS1115_ADC::OperatingMode::Single_Shot_Or_Power_Down,
        .dataRate = eh::ADS1115_ADC::DataRate::_8SPS,
        .compMode = eh::ADS1115_ADC::ComparatorMode::Traditional,
        .polarity = eh::ADS1115_ADC::ComparatorPolarity::ActiveLow,
        .latch    = eh::ADS1115_ADC::LatchingComparator::NonLatching,
        .queue    = eh::ADS1115_ADC::ComparatorQueueConfig::Disabled
    };

    eh::ADS1115_ADC adc;

    static void SetUpTestCase() {
        // empty
    }

    void update_data_rate(eh::ADS1115_ADC::DataRate dr) {
        auto new_mode {adc.getMode()};
        new_mode.dataRate = dr;
        adc.setMode(new_mode);
    }

    /// Violates MISRA 18-0-4, but `clock_nanosleep` is recommended in the Burns & Wellings RTS&PL book.
    static void waitNSecs(int transitionTimeNSecs) {
        struct timespec wait {
                .tv_sec = 0,
                .tv_nsec = transitionTimeNSecs
        };

        if(clock_nanosleep(CLOCK_MONOTONIC, 0, &wait, nullptr) < 0) {
            perror("clock_nanosleep");
        }
    }
};

// --------------
// All unit tests
// --------------

TEST_F(ADCTests, InitializationSuccess) {
    bool ok {adc.initialize(i2cbus, address, mode_default)};
    ASSERT_TRUE(ok) <<
        "\t-----------ooOOoo-----------\n"
        "\tCould NOT initialize the ADC\n"
        "\t-----------ooOOoo-----------\n";
}

TEST_F(ADCTests, MultipleInitializations) {
    bool first  {adc.initialize(i2cbus, address, mode_default)};
    bool second {adc.initialize(i2cbus, address, mode_default)};

    ASSERT_TRUE(first && second);
}

TEST_F(ADCTests, UpdateOperatingMode) {
    eh::ADS1115_ADC::Mode mode_random {
        .pga      = eh::ADS1115_ADC::PGAMode::FSR_4V096,
        .opMode   = eh::ADS1115_ADC::OperatingMode::Single_Shot_Or_Power_Down,
        .dataRate = eh::ADS1115_ADC::DataRate::_128SPS,
        .compMode = eh::ADS1115_ADC::ComparatorMode::WindowComparator,
        .polarity = eh::ADS1115_ADC::ComparatorPolarity::ActiveHigh,
        .latch    = eh::ADS1115_ADC::LatchingComparator::Latching,
        .queue    = eh::ADS1115_ADC::ComparatorQueueConfig::Disabled
    };

    adc.setMode(mode_random);
    auto mode = adc.getMode();

    ASSERT_EQ (mode_random.pga,      mode.pga);
    ASSERT_EQ (mode_random.opMode,   mode.opMode);
    ASSERT_EQ (mode_random.dataRate, mode.dataRate);
    ASSERT_EQ (mode_random.compMode, mode.compMode);
    ASSERT_EQ (mode_random.polarity, mode.polarity);
    ASSERT_EQ (mode_random.latch,    mode.latch);
    ASSERT_EQ (mode_random.queue,    mode.queue);
}

TEST_F(ADCTests, UpdateDataRate) {
    update_data_rate(eh::ADS1115_ADC::DataRate::_8SPS);
    ASSERT_EQ (eh::ADS1115_ADC::DataRate::_8SPS, adc.getMode().dataRate);

    update_data_rate(eh::ADS1115_ADC::DataRate::_16SPS);
    ASSERT_EQ (eh::ADS1115_ADC::DataRate::_16SPS, adc.getMode().dataRate);

    update_data_rate(eh::ADS1115_ADC::DataRate::_32SPS);
    ASSERT_EQ (eh::ADS1115_ADC::DataRate::_32SPS, adc.getMode().dataRate);

    update_data_rate(eh::ADS1115_ADC::DataRate::_64SPS);
    ASSERT_EQ (eh::ADS1115_ADC::DataRate::_64SPS, adc.getMode().dataRate);

    update_data_rate(eh::ADS1115_ADC::DataRate::_128SPS);
    ASSERT_EQ (eh::ADS1115_ADC::DataRate::_128SPS, adc.getMode().dataRate);

    update_data_rate(eh::ADS1115_ADC::DataRate::_250SPS);
    ASSERT_EQ (eh::ADS1115_ADC::DataRate::_250SPS, adc.getMode().dataRate);

    update_data_rate(eh::ADS1115_ADC::DataRate::_475SPS);
    ASSERT_EQ (eh::ADS1115_ADC::DataRate::_475SPS, adc.getMode().dataRate);

    update_data_rate(eh::ADS1115_ADC::DataRate::_860SPS);
    ASSERT_EQ (eh::ADS1115_ADC::DataRate::_860SPS, adc.getMode().dataRate);
}

TEST_F(ADCTests, IndividualVSCompleteReadingsAll) {
    adc.setMode(mode_default);

    EXPECT_NEAR (adc.readAllChannels()[0], adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel0), maximumDeviationRaw);
    EXPECT_NEAR (adc.readAllChannels()[1], adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel1), maximumDeviationRaw);
    EXPECT_NEAR (adc.readAllChannels()[2], adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel2), maximumDeviationRaw);
    EXPECT_NEAR (adc.readAllChannels()[3], adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel3), maximumDeviationRaw);
}

TEST_F(ADCTests, IndividualReadings) {
    adc.setMode(mode_default);

    EXPECT_NEAR (adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel0),
                 adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel0),
                 maximumDeviationRaw);
    EXPECT_NEAR (adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel1),
                 adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel1),
                 maximumDeviationRaw);
    EXPECT_NEAR (adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel2),
                 adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel2),
                 maximumDeviationRaw);
    EXPECT_NEAR (adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel3),
                 adc.readOneChannel(eh::ADS1115_ADC::Channel::Channel3),
                 maximumDeviationRaw);
}

TEST_F(ADCTests, CompleteReadings) {
    adc.setMode(mode_default);

    EXPECT_NEAR (adc.readAllChannels()[0], adc.readAllChannels()[0], maximumDeviationRaw);
    EXPECT_NEAR (adc.readAllChannels()[1], adc.readAllChannels()[1], maximumDeviationRaw);
    EXPECT_NEAR (adc.readAllChannels()[2], adc.readAllChannels()[2], maximumDeviationRaw);
    EXPECT_NEAR (adc.readAllChannels()[3], adc.readAllChannels()[3], maximumDeviationRaw);
}