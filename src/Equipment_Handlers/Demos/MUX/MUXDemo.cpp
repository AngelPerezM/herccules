//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//          Copyright (C) 2022 Universidad Politécnica de Madrid            //
//                                                                          //
// This is free software;  you can redistribute it  and/or modify it  under //
// terms of the  GNU General Public License as published  by the Free Soft- //
// ware  Foundation;  either version 3,  or (at your option) any later ver- //
// sion.  This software is distributed in the hope  that it will be useful, //
// but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- //
// TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public //
// License for  more details.  You should have  received  a copy of the GNU //
// General  Public  License  distributed  with  this  software;   see  file //
// COPYING3.  If not, go to http://www.gnu.org/licenses for a complete copy //
// of the license.                                                          //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#include "MUX.h"
#include "unistd.h"
#include <pigpio.h>
#include <iostream>

namespace eh = equipment_handlers;

int main () {
    eh::MUX mux;
    bool success = mux.initialize(27U, 22U, 10U);
    if(!success) {
        return 1;
    }

    for (int i = 0; i < 8; ++i) {
        mux.selectInput(i);
        std::cout << "GPIO 27, 22, 10 := " << gpioRead(27) << gpioRead(22) << gpioRead(10) << std::endl;
        (void) sleep(3U);
    }

    return 0;
}