//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//          Copyright (C) 2022 Universidad Politécnica de Madrid            //
//                                                                          //
// This is free software;  you can redistribute it  and/or modify it  under //
// terms of the  GNU General Public License as published  by the Free Soft- //
// ware  Foundation;  either version 3,  or (at your option) any later ver- //
// sion.  This software is distributed in the hope  that it will be useful, //
// but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- //
// TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public //
// License for  more details.  You should have  received  a copy of the GNU //
// General  Public  License  distributed  with  this  software;   see  file //
// COPYING3.  If not, go to http://www.gnu.org/licenses for a complete copy //
// of the license.                                                          //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#include "ADS1115_ADC.h"
#include "iostream"
#include "unistd.h"

namespace bh = bus_handlers;
namespace bh_dt = bus_handlers::data;
namespace eh = equipment_handlers;

void printReadings(eh::ADS1115_ADC &adc) {
    for (int i = 0; i < 5; ++i) {
        auto readings = adc.readAllChannels();
        int channel = 0;
        for (auto r : readings) {
            std::cout << "Channel @ " << channel << " = " << r << std::endl;
            channel++;
        }
        (void)sleep(1U);
    }
}

int main() {
    struct eh::ADS1115_ADC::Mode prototypeTMUADCMode; // Use default config for the other fields
    prototypeTMUADCMode.pga      = eh::ADS1115_ADC::PGAMode::FSR_4V096;
    prototypeTMUADCMode.opMode   = eh::ADS1115_ADC::OperatingMode::Single_Shot_Or_Power_Down;
    prototypeTMUADCMode.dataRate = eh::ADS1115_ADC::DataRate::_8SPS;

    eh::ADS1115_ADC adc0;
    bool success = adc0.initialize(bh_dt::I2CBusID::BUS1, 0x48U, prototypeTMUADCMode);
    if (!success) {
        std::cout << "[ADCDemo] Could not initialize adc0." << std::endl;
        return 1;
    }

    std::cout << "[ADCDemo] reading at 8SPS" << std::endl;
    prototypeTMUADCMode.dataRate = eh::ADS1115_ADC::DataRate::_8SPS;
    (void) adc0.setMode(prototypeTMUADCMode);
    printReadings(adc0);
    std::cout << std::endl;

    std::cout << "[ADCDemo] reading at 64SPS" << std::endl;
    prototypeTMUADCMode.dataRate = eh::ADS1115_ADC::DataRate::_64SPS;
    (void) adc0.setMode(prototypeTMUADCMode);
    printReadings(adc0);
    std::cout << std::endl;

    std::cout << "[ADCDemo] reading at 860SPS" << std::endl;
    prototypeTMUADCMode.dataRate = eh::ADS1115_ADC::DataRate::_860SPS;
    (void) adc0.setMode(prototypeTMUADCMode);
    printReadings(adc0);
    std::cout << std::endl;

    return 0;
}