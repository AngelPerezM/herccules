set(BINARY_NAME ADCDemo)
# 1st param: executable
# 2nd param: sources need to generate the executable
add_executable(${BINARY_NAME} "ADCDemo.cpp")

target_link_libraries(${BINARY_NAME} PRIVATE Equipment_Handlers)
target_link_libraries(${BINARY_NAME} PRIVATE Bus_Handlers)

install(TARGETS ${BINARY_NAME} DESTINATION ${CMAKE_BINARY_DIR}/bin/${PROJECT_NAME})
