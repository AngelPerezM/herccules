//
// Created by javilete on 10/3/22.
//

#include "GpsHandler.h"
#include <iostream>

int main(int argc, char ** argv) {

    if (argc < 1) {
        return 1;
    }

    char * serial_port = argv[0];
    bool success = equipment_handlers::gps_handler::initialize(serial_port);
    if (!success) {
        std::cerr << "ERROR!!! could not initialize gps handler\n";
    } else {
        std::cout << "initialized\n";
    }

    equipment_handlers::gps_handler::startDataAcquisition(
        [](equipment_handlers::gps_handler::gps_data_t gps_data) {
            std::cout << "\n"
                     "-------------------------\n" <<
                     " - secs:  " << gps_data.unix_time.tv_sec <<
                     "   nsecs: " << gps_data.unix_time.tv_nsec <<
                     " - lat: " << gps_data.latitude_deg <<
                     " - lon: " << gps_data.longitude_deg <<
                     " - alt: " << gps_data.altitude_m <<
                     " - sog: " << gps_data.sog_knots <<
                     " - cog: " << gps_data.cog_deg <<
                     "-------------------------\n\n";
        }
    );

    while (true) {
    }

    return 0;
}