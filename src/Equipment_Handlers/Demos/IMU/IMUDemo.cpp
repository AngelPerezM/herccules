//
// Created by grover on 24/04/22.
//

#include "BNO055_IMU.h"
#include <iostream>
#include <unistd.h>

namespace eh = equipment_handlers;
namespace bh = bus_handlers;
namespace bh_dt = bus_handlers::data;

using equipment_handlers::BNO055_IMU;

void printSensorData(BNO055_IMU::SensorData s) {
    std::cout << "X: " << s.X << "  Y: " << s.Y << "  Z: " << s.Z << std::endl;
}

void printSensorData(BNO055_IMU::Quaternion s) {
    std::cout << "W: " << s.W << "X: " << s.X << "  Y: " << s.Y << "  Z: " << s.Z << std::endl;
}

int main() {

    equipment_handlers::BNO055_IMU imu;
    bool success = imu.initialize(eh::BNO055_IMU::BNO055_Primary_Address, bh_dt::I2CBusID::BUS1);
    if (!success) {
        std::cout << "Could not initialize IMU!" << std::endl;
        return 1;
    }

    std::cout << "Calibrating IMU" << std::endl;
    bool calib = false;
    while (!calib) {
        auto status = imu.getSensorsCalibration();
        if (status.Accelerometer == equipment_handlers::BNO055_IMU::FULLY_CALIBRATED) {
            std::cout << "Accelerometer calibrated" << std::endl;
        }
        if (status.Gyroscope == equipment_handlers::BNO055_IMU::FULLY_CALIBRATED) {
            std::cout << "Gyroscope calibrated" << std::endl;
        }
        if (status.Magnetometer == equipment_handlers::BNO055_IMU::FULLY_CALIBRATED) {
            std::cout << "Magnetometer calibrated" << std::endl;
        }

        calib = status.Accelerometer == equipment_handlers::BNO055_IMU::FULLY_CALIBRATED
             && status.Gyroscope == equipment_handlers::BNO055_IMU::FULLY_CALIBRATED
             && status.Magnetometer == equipment_handlers::BNO055_IMU::FULLY_CALIBRATED;
    }
    std::cout << "IMU calibrated" << std::endl;

    if (imu.testWhoAmI()) {
        std::cout << "Who am i passed, all IDs are CORRECT!" << std::endl;
    } else {
        std::cout << "Did not pass who am i test" << std::endl;
        return 1;
    }

    // imu.setMode(equipment_handlers::BNO055_IMU::AMG);
    imu.setAccelerationUnits(equipment_handlers::BNO055_IMU::METERS_SECOND_SQUARED);

    auto mode = imu.getCurrentMode();
    std::cout << "My current mode is: " << mode << std::endl;

    while (true) {
        BNO055_IMU::Acceleration accel;
        BNO055_IMU::AngularVelocity gyro;
        BNO055_IMU::MagField mgt;
        BNO055_IMU::Gravity gra;
        BNO055_IMU::LinearAcceleration lia;
        BNO055_IMU::EulerOrientation eu;
        BNO055_IMU::Quaternion quat;

        imu.readSensorData(accel);
        imu.readSensorData(gyro);
        imu.readSensorData(mgt);
    
        imu.readFusedData(gra);
        imu.readFusedData(lia);
        imu.readFusedData(eu);
        imu.readFusedData(quat);

        BNO055_IMU::Temperatures ts;
        imu.readTemperatures(ts);

        BNO055_IMU::FusedData fused;
        imu.readAllFusedData(fused);

        std::cout << "Accel:     "; printSensorData(accel);
        std::cout << "Gyro:      "; printSensorData(gyro);
        std::cout << "MGT:       "; printSensorData(mgt);
        std::cout << "Gravity    "; printSensorData(gra);
        std::cout << "LIA:       "; printSensorData(lia);
        std::cout << "EU orient: "; printSensorData(eu);
        std::cout << "Quat:      "; printSensorData(quat);
        std::cout << "Acc Temp:  " << static_cast<int16_t>(ts.from_accelerometer) << std::endl;
        std::cout << "Gyro Temp: " << static_cast<int16_t>(ts.from_gyroscope) << std::endl
                                                                     << std::endl;

        std::cout << "Fused data burst:\n";
        std::cout << "\t EU orient: "; printSensorData(fused.eulerOrientation);
        std::cout << "\t Gravity:   "; printSensorData(fused.gravity);
        std::cout << "\t LIA:       "; printSensorData(fused.linearAcceleration);
        std::cout << "\t Quat:      "; printSensorData(fused.quat);


        std::cout << "------------------------------------\n";


        (void)sleep(1U);
    }

    return 0;
}