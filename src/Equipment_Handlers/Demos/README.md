Equipment Handlers Demo
=======================

This directory contains all the demos for the *Equipment Handlers* component.
Each demo is located in its own directory, and corresponds to an executable target.

The demo executes the basic functionality of a sensor, actuator, or a subsystem.
For instance, the `ADCDemo` instances one object from the `ADS1115_ADC` class and reads periodically its raw data.

CMake configuration
-------------------

Since these demos use classes that are not exported from the *Equipment Handlers* (i.e. classes inside the `src` directory), 
each demo's `CMakeLists.txt` file includes these source directory as follows:

```
target_include_directories(<DemoName> PRIVATE ${EquipmentHandlers_SOURCE_DIR}/src)
```