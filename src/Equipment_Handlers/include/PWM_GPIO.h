/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                             PWM_GPIO Header                                --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/


#ifndef OBSW_PWM_GPIO_H
#define OBSW_PWM_GPIO_H

#include "Switch.h"

namespace equipment_handlers {

    class PWM_GPIO : public Switch {
    public:
        // --------------------------
        // Constructors & destructors
        // --------------------------

        PWM_GPIO();

        PWM_GPIO(unsigned int pin);

        bool initialize(unsigned int pin);

        ~PWM_GPIO();

        // ------------
        // Manipulators
        // ------------

        bool setPWMFrequency(unsigned int frequency);

        /**
         * The Duty Cycle range is the number of steps between
         * fully on and off
         */
        bool setDutyCycleRange(unsigned int range);

        /**
         * @param dutyCycle from 0 to 100 %
         */
        bool setDutyCycle(float dutyCycle);

        float dutyCycle();

    private:
        int m_pwmFreq               = 0;
        unsigned int m_pwmRange     = 255U;
        unsigned int m_pwmRealRange = 255U;
    };

} // equipment_handlers

#endif //OBSW_PWM_GPIO_H
