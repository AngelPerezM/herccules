/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                           ADS1115_ADC  Header                              --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef HAL_ADS1115_ADC_H
#define HAL_ADS1115_ADC_H

#include "BusHandlers_Data.h"
#include <array>
#include <cstdint>

namespace equipment_handlers {

    /// @note This is a BIG-Endian slave, i.e.: the MSB is send first.
    class ADS1115_ADC {

    public:

        // --  Constants  ------------------------------------------------------

        static const uint8_t primaryI2CAddress   = 0x048U;
        static const uint8_t secondaryI2CAddress = 0x049U;

        // --  Data Types  -----------------------------------------------------

        enum class Channel : uint16_t {
            Channel0 = 0x0004U << 12,
            Channel1 = 0x0005U << 12,
            Channel2 = 0x0006U << 12,
            Channel3 = 0x0007U << 12
        };

        /// PGA (Programmable Gain Amplifier) configuration values
        enum class PGAMode : uint16_t {
            FSR_6V144 = 0x0000U << 9U,
            FSR_4V096 = 0x0001U << 9U,
            FSR_2V048 = 0x0002U << 9U,
            FSR_1V024 = 0x0003U << 9U,
            FSR_0V512 = 0x0004U << 9U,
            FSR_0V256 = 0x0005U << 9U
        };

        /// Device operating mode
        enum class OperatingMode : uint16_t {
            Continuous_Conversion     = 0x0000U << 8U,
            Single_Shot_Or_Power_Down = 0x0001U << 8U
        };

        enum class DataRate : uint16_t {
            _8SPS   = 0x0000U << 5U,
            _16SPS  = 0x0001U << 5U,
            _32SPS  = 0x0002U << 5U,
            _64SPS  = 0x0003U << 5U,
            _128SPS = 0x0004U << 5U,
            _250SPS = 0x0005U << 5U,
            _475SPS = 0x0006U << 5U,
            _860SPS = 0x0007U << 5U
        };

        enum class ComparatorMode : uint16_t {
            Traditional      = 0x0000U << 4U,
            WindowComparator = 0x0001U << 4U
        };

        enum class ComparatorPolarity : uint16_t {
            ActiveLow  = 0x0000U << 3U,
            ActiveHigh = 0x0001U << 3U
        };

        enum class LatchingComparator : uint16_t {
            NonLatching = 0x0000U << 2U,
            Latching    = 0x0001U << 2U
        };

        enum class ComparatorQueueConfig :uint16_t {
            AssertAfter1Conversion  = 0x0000U,
            AssertAfter2Conversions = 0x0001U,
            AssertAfter4Conversions = 0x0002U,
            Disabled                = 0x0003U
        };

        struct Mode {
            PGAMode pga                 = PGAMode::FSR_2V048;
            OperatingMode opMode        = OperatingMode::Single_Shot_Or_Power_Down;
            DataRate dataRate           = DataRate::_128SPS;
            ComparatorMode compMode     = ComparatorMode::Traditional;
            ComparatorPolarity polarity = ComparatorPolarity::ActiveLow;
            LatchingComparator latch    = LatchingComparator::NonLatching;
            ComparatorQueueConfig queue = ComparatorQueueConfig::Disabled;
        };

        // --  Provided IF  ----------------------------------------------------

        ADS1115_ADC();

        bool initialize
            (bus_handlers::data::I2CBusID i2cBusID,
             uint8_t address,
             Mode mode);

        /// @return -1 in case of failure. Results from 0 up to 0x7FFF are expected
        int16_t readOneChannel(Channel channel, uint8_t n_retries = 0U) const;

        /// @return -1s in case of failure. Results from 0 up to 0x7FFF are expected
        std::array<int16_t, 4U> readAllChannels(uint8_t n_retries_per_channel = 0U) const;

        bool setMode(Mode mode);

        Mode getMode() const;

        bool reset();

    private:

        bus_handlers::data::I2CBusID m_i2cBusID {};
        uint8_t m_address {primaryI2CAddress};
        uint16_t mode_ {};

        uint16_t getConfigRegister() const;

        bool setConfigRegister(uint16_t config) const;

        void waitForConversion(DataRate dr) const;

        int16_t getConversionRegister() const;

        bool isConversionAvailable() const;

    };

}

#endif //HAL_ADS1115_ADC_H
