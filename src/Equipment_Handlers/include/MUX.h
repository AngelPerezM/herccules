/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                                MUX Header                                  --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef HAL_MUX_H
#define HAL_MUX_H

#include <array>

namespace equipment_handlers {

    class MUX {
    public:

        MUX();

        bool initialize(unsigned int pin0, unsigned int pin1, unsigned int pin2);

        void finalize();

        /**
         * @param input used to select the MUX input, represents the select bits.
         * Since there are 8 inputs, input must be in the [0; 7] range
         */
        void selectInput(int input);

    private:

        bool initializeSelectorPins() const;

        static void waitTransition();

        /// @brief Address lines pins configuration, selectorPins[i] contains
        /// the GPIO pin for the address line i-th bit
        std::array <unsigned, 3U> selectorPins;
        int currentInput = 0;
        static const int transitionTimeNSecs = 360;
    };

}

#endif //HAL_MUX_H
