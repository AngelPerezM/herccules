/*------------------------------------------------------------------------------
--                   HERCCULES On-Board Software Components                   --
--                                                                            --
--                           EQUIPMENT  HANDLERS                              --
--                                                                            --
--                              INA226  Header                                --
--                                                                            --
--            Copyright (C) 2022 Universidad Politécnica de Madrid            --
--                                                                            --
-- HERCCULES was developed by the Real-Time Systems Group at  the Universidad --
-- Politécnica de Madrid.                                                     --
--                                                                            --
-- HERCCULES is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU General Public License as published by the Free Soft- --
-- ware Foundation, either version 3 of the License,  or (at your option) any --
-- later version.                                                             --
--                                                                            --
-- HERCCULES is distributed  in the hope that  it will be useful  but WITHOUT --
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FIT- --
-- NESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more --
-- details. You should have received a copy of the GNU General Public License --
-- along with HERCCULES. If not, see <https://www.gnu.org/licenses/>.         --
--                                                                            --
------------------------------------------------------------------------------*/

#ifndef OBSW_INA226_H
#define OBSW_INA226_H

#include "BusHandlers_Data.h"
#include <cstdint>

namespace equipment_handlers {

    /// @note This is a BIG-Endian slave, i.e.: the MSB is send/received first
    class INA226 {

    public:

        static constexpr uint8_t defaultAddress = 0x40U;

        INA226();

        enum OperatingMode : uint16_t {
            Off = 0U,

            ShuntTrigger    = 1U,
            BusTrigger      = 2U,
            ShuntBusTrigger = 3U,

            ShuntContinuous    = 5U,
            BusContinuous      = 6U,
            ShuntBusContinuous = 7U
        };

        /// Initializes the power and current sensor with the passed configuration as input parameters
        /// \return True in success.
        bool initialize(bus_handlers::data::I2CBusID i2cBusID,
                        uint8_t i2cAddress = defaultAddress,
                        OperatingMode mode = Off);

        float getBusVoltage();

        float getShuntVoltage();

        float getCurrent();

        float getPower();

        /**
         * @note
         * If the sensor is in activated mode,
         * the client MUST request the new readings explicitly
         * and check their availability.
         *
         * This function clears the "conversion ready" register,
         * so, if you call requestData() before conversionReady(),
         * and the elapsed time between these calls is lower than the conversion time
         * conversionReady() will return false.
         */
        void requestData();

        // Configuration:

        void reset();

        void enableConversionReadyAlert();

        bool conversionsReady();

        enum Average_Samples : uint16_t {
            AVERAGE_1    = 0x0000U,
            AVERAGE_4    = 0x0200U,
            AVERAGE_16   = 0x0400U,
            AVERAGE_64   = 0x0600U,
            AVERAGE_128  = 0x0800U,
            AVERAGE_256  = 0x0A00U,
            AVERAGE_512  = 0x0C00U,
            AVERAGE_1024 = 0x0E00U
        };

        void setAverage(Average_Samples avg = AVERAGE_1);
        Average_Samples getAverage();

        enum Conversion_Time_ms : uint16_t {
            CONV_TIME_140   = 0b000U,
            CONV_TIME_204   = 0b001U,
            CONV_TIME_332   = 0b0010U,
            CONV_TIME_588   = 0b011U,
            CONV_TIME_1100  = 0b100U,
            CONV_TIME_2116  = 0b101U,
            CONV_TIME_4156  = 0b110U,
            CONV_TIME_8244  = 0b111U
        };

        void setBusVoltageConversionTime(Conversion_Time_ms bvct);
        Conversion_Time_ms getBusVoltageConversionTime();

        void setShuntVoltageConversionTime(Conversion_Time_ms svct);
        Conversion_Time_ms getShuntVoltageConversionTime();


        void setMaxCurrentShunt(float maxCurrent, // Amps
                                float shunt);     // OHMs

        float getShunt();
        float getMaxCurrent();
        bool  isCalibrated();

        void setMode(OperatingMode mode = ShuntBusTrigger);
        OperatingMode getMode();

    private:

        bus_handlers::data::I2CBusID m_i2cBusID;
        uint8_t m_i2cAddress;

        OperatingMode m_mode = Off;

        float m_shunt      = 0.0F;
        float m_maxCurrent = 0.0F;

        float current_LSB = 0.0F;
        float power_LSB   = 0.0F;

        uint16_t  readRegister(uint8_t reg) const;
        void     writeRegister(uint8_t reg, uint16_t value) const;

    };

} // equipment_handlers

#endif //OBSW_INA226_H
